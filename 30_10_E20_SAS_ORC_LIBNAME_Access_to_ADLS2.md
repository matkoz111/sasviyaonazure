![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# SAS ORC LIBNAME access to Azure ADLS2 data file
- [Login to SAS Viya Application](#login-to-sas-viya-application)
- [Reading and writing single .orc file as table to ADLS2](#reading-and-writing-single-orc-file-as-table-to-adls2)
  - [Authenticate Viya Server device for ADLS2 access](#authenticate-viya-server-device-for-adls2-access)
    - [Steps to login to Viya01 Unix server](#steps-to-login-to-viya01-unix-server)
    - [You are on Viya01 server](#you-are-on-viya01-server)
    - [Authenticate using microsoft site](#authenticate-using-microsoft-site)
  - [Back to SAS Studio Application](#back-to-sas-studio-application)
- [Reading writing multiple .orc file as single table to ADLS2](#reading-writing-multiple-orc-file-as-single-table-to-adls2)
  - [Save a SAS table to ADLS2 with multiple orc file](#save-a-sas-table-to-adls2-with-multiple-orc-file)
  - [Verify the data files at ADLS2 sub-folder](#verify-the-data-files-at-adls2-sub-folder)
  - [Read multiple .orc file into a single table](#read-multiple-orc-file-into-a-single-table)

Purpose of this hands-on is to demonstrate the ORC data file access from SAS 9.4 to Azure Data Lake Storage 2 (ADLS2) using SAS ORC LIBNAME statement. The ORC SAS engine let you access ORC files located at ADLS2.

**Assuming you have ADLS2 connection information.**

## Login to SAS Viya Application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Reading and writing single .orc file as table to ADLS2

On SAS Studio editor execute the following code to save a SAS data sets table to ADLS2 location as ORC file.

**Before running the code, set the parameters for storage account name and file system with information collected in previous section.**

Code:

```sas
/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;


/*
Example:
%let MYSTRGACC="utkuma5adls2strg";
%let MYSTRGFS="fsutkuma5adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-a430-xxxxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-9eda-xxxxxxxxxxxxxx";
*/

libname orclib ORC "/sample_data"
      storage_account_name =&MYSTRGACC
      storage_file_system =&MYSTRGFS
      storage_dns_suffix = "dfs.core.windows.net"
      storage_application_id=&MYAPPID
      storage_tenant_id=&MYTNTID
      DIRECTORIES_AS_DATA=YES
      FILE_NAME_EXTENSION=(orc ORC)
;

data orclib.fish_orc;
   set sashelp.fish;
run;

Proc SQL ;
select * from orclib.fish_orc ;
run;
quit;

```

Note: **Very first time when you run the LIBNAME statement, you will see following error msg.** You need to login to viya01 Unix server and view the /home/viyademo01/*.json file. **The .json file has the instruction for how to register and authenticate the device.** The json file is generated for user who is running the SAS Studio application session.

Results:

```log
88         DIRECTORIES_AS_DATA=YES
89         FILE_NAME_EXTENSION=(orc ORC)
90   ;
ERROR: Invalid physical name for library ORCLIB.
ERROR: Error in the LIBNAME statement.
91
```

### Authenticate Viya Server device for ADLS2 access

#### Steps to login to Viya01 Unix server

Login to cldlgn.fyi.sas.com server and cd to ..../Scripts folder and concatenate the resource_info file. This file contains the information about how to jump onto Viya01 server.

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

more resource_info
```

Look for “To Jump to Viya01 cloud server:” section

```log

##### This is just example code, you get the right code by doing "more resource_info" file in your environment.
………………………..
……
=============
To Jump to Viya01 cloud server :
=============
# my_private_key=/r/ge.unx.sas.com/vol/vol310/u31/utkuma/.ssh/utkuma-viyadep-key
# my_user=viyadep
# vm_public_ip=52.254.10.86

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
```

Execute the listed 4 lines from this section at cldlgn.fyi.sas.com server and it will take you to Viya01 server.

Code:

```bash

##### This is just example code, you get the right code by doing "more resource_info" file in your environment.

# my_private_key=/r/ge.unx.sas.com/vol/vol310/u31/utkuma/.ssh/utkuma-viyadep-key
# my_user=viyadep
# vm_public_ip=52.254.10.86

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
```

#### You are on Viya01 server

On Viya01 Unix server execute the following statement to get the device registration instructions.

Code:

```bash
sudo ls -la /home/viyademo01/
sudo more /home/viyademo01/.sasadls_100001.json
```

Results:

```log
[viyadep@viya01 ~]$ sudo ls -la /home/viyademo01/
total 32
…..
….
-rw-------.  1 viyademo01 marketing  408 Apr 22 16:10 .sasadls_100001.json
....
...
[viyadep@viya01 ~]$


[viyadep@viya01 ~]$ sudo more /home/viyademo01/.sasadls_100001.json
{"refresh_token":"","device_code":"AAQABAAEAAAAm-06blBE1TpVMil8KPQ41GgI-YkVkIf2nhpSkRd9jM9j2N5GfLEPotyL7Bibs0gFHjjvfATdJKe22oKxRjVIIv-djHs_-0JAdgTTMEu70qS
cODRuYDpQigEI1Mx2RJf-fQj_vgpL5p_nD2T4PWrxhxZx7J6e4BXDdJmslfece7O5r_diYESDIV-V5WuX14CsgAA","message":"To sign in, use a web browser to open the page https:
//microsoft.com/devicelogin and enter the code AAM8LARQT to authenticate.","oauth":"","resource":""}
[viyadep@viya01 ~]$
```

#### Authenticate using microsoft site

On a web browser open the site //microsoft.com/devicelogin and use the code listed in the .json file to authenticate the device.

Note: If you take too long to authenticate, the code might expire, and you need to regenerate the code. To re-generate the device code remove the /home/viyademo01/.....json file and rerun the sas code. You can  use “sudo rm  /home/viyademo01/.sasadls_100001.json “ statement at Viya01 server to remove the file.

Exit out of Viya01 Unix server

Code:

```bash
exit
```

### Back to SAS Studio Application

Come back to SAS Studio application and re-run the LIBNAME statement and data step code.

Results:

```log
…………….
….
82   data orclib.fish_orc ;
83   set sashelp.fish ;
84   run;
NOTE: There were 159 observations read from the data set SASHELP.FISH.
NOTE: The data set ORCLIB.fish_orc has 159 observations and 7 variables.
NOTE: DATA statement used (Total process time):
      real time           0.90 seconds
      cpu time            0.11 seconds

85
……………
…………………..
```

Result from PROC SQL:
![1](img/utkuma_2020-05-26-15-07-42.png)

Verify data file saved at ADLS2 storage folder (sample_data). At Azure Storage account navigate to Storage Explorer and refresh the sample_data folder to see the new files.

![1](img/utkuma_2020-05-26-15-08-23.png)

## Reading writing multiple .orc file as single table to ADLS2

On Azure Storage Account location, navigate to /sample_data folder  and use “New Folder” option to create a new sub-folder.

![1](img/utkuma_2020-05-26-15-09-23.png)

Create a new sub-folder named “fish_n”.

![1](img/utkuma_2020-05-26-15-10-44.png)

### Save a SAS table to ADLS2 with multiple orc file

On SAS Studio editor execute the following code to save multiple .orc file to ADLS2 sub-folder (fish_n) from a SAS data table.

**Before running the code, set the parameters for Storage account name and file system with information collected in previous section.**

**Note : The LIBNAME path is to "/sample_data/fish_n"**

Code:

```sas

/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;

/*
Example:
%let MYSTRGACC="utkuma5adls2strg";
%let MYSTRGFS="fsutkuma5adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-xxxxxxxxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-xxxxxxxxxxxxxxxxxx";
*/


libname orclib1 ORC "/sample_data/fish_n"
      storage_account_name = &MYSTRGACC
      storage_file_system = &MYSTRGFS
      storage_dns_suffix = "dfs.core.windows.net"
      storage_application_id=&MYAPPID
      storage_tenant_id=&MYTNTID
      DIRECTORIES_AS_DATA=YES
      FILE_NAME_EXTENSION=(orc ORC)
;



data orclib1.fish_0;
   set sashelp.fish;
run;

data orclib1.fish_1;
   set sashelp.fish;
run;

data orclib1.fish_2;
   set sashelp.fish;
run;
```

Results:

```log
…………….
….
82   data orclib1.fish_1;
83      set sashelp.fish;
84   run;
NOTE: There were 159 observations read from the data set SASHELP.FISH.
NOTE: The data set ORCLIB1.fish_1 has 159 observations and 7 variables.
NOTE: DATA statement used (Total process time):
      real time           0.55 seconds
      cpu time            0.11 seconds

85
86   data orclib1.fish_2;
87      set sashelp.fish;
88   run;
NOTE: There were 159 observations read from the data set SASHELP.FISH.
NOTE: The data set ORCLIB1.fish_2 has 159 observations and 7 variables.
NOTE: DATA statement used (Total process time):
      real time           0.53 seconds
      cpu time            0.10 seconds

89
……………
…………………..
```

### Verify the data files at ADLS2 sub-folder

Navigate to ADLS2 Storage Account and then to sub-folder and view the files.

![1](img/utkuma_2020-05-26-15-11-21.png)

### Read multiple .orc file into a single table

Now, lets read that multiple .orc file under a sub-folder as one table. Execute the following code on SAS Studio application.

Note: LIBNAME orclib has path to **/sample_data** with DIRECTORIES_AS_DATA=YES.

Code:

```sas
proc SQL ;
select * from orclib.fish_n ;
run;
quit;
```

Now, create a new table from n-number of .orc file to a single file .orc at ADLS2.

Note: LIBNAME orclib has path to **/sample_data** with DIRECTORIES_AS_DATA=YES.

Execute the following data step code on SAS Studio application.

Code:

```sas
data orclib.fish_new;
   set orclib.fish_n;
run;
```

Results:

Notice the Number of rows read from fish_n table to create a new table.

```log
………
…………….
82   data orclib.fish_new;
83      set orclib.fish_n;
84   run;
NOTE: There were 477 observations read from the data set ORCLIB.fish_n.
NOTE: The data set ORCLIB.fish_new has 477 observations and 7 variables.
………
…………
```

Verify the data files at ADLS2 sub-folder.

![1](img/utkuma_2020-05-26-15-12-12.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)**<-- you are here**
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
