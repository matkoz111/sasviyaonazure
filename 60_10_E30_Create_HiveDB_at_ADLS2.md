![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Create a Hive Database with location to ADLS2
- [Login to Viya application](#login-to-viya-application)
- [Create a Hive Database with storage location to ADLS2](#create-a-hive-database-with-storage-location-to-adls2)

Purpose of this hands on is to create a Hive Database with data file located to ADLS2. With file location to ADLS2, the tables and other objects created under this database reside on ADLS2 and not on HDFS. This database will help to save SAS and CAS table to ADLS2 with different file type (CSV, Avro,Parquet, Jason etc..) supported by Hive.

## Login to Viya application

Login to SAS Viya application using following informations.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Create a Hive Database with storage location to ADLS2

On SAS Studio execute the following code to create a Hive Database with external data file.

**Before executing the code set the HDINM= , and  MYSTRGACC= parameter with your HDI name and Storage account name. The MYSTRGFS=fsdata is valid value and keep it unchanged**

Code:

```sas

/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=fsdata;

/*
Example:
%let HDINM=hdiutkuma8;
%let MYSTRGACC=utkuma8adls2strg;
%let MYSTRGFS=fsdata;
*/


option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
options sastrace = ',,,sd' sastraceloc = saslog ;

PROC SQL ;
connect to hadoop as hvlib (uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
server="&HDINM..azurehdinsight.net" user="admin" pw="SASV3i5ya@2020" schema="default");
execute (CREATE database geltest LOCATION "abfs://&MYSTRGFS@&MYSTRGACC..dfs.core.windows.net/hive_db"
) by hvlib;
quit;
```

Results:

```log
...
......
86   PROC SQL ;
87   connect to hadoop as hvlib (uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;
87 ! ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
88   server="&HDINM..azurehdinsight.net" user="admin" pw=XXXXXXXXXXXXXXXX schema="default");
89   execute (CREATE database geltest LOCATION "abfs://&MYSTRGFS@&MYSTRGACC..dfs.core.windows.net/hive_db"
90   ) by hvlib;
  0 1589477088 no_name 0 SQL
HADOOP_1: Executed: on connection 0 1 1589477088 no_name 0 SQL
CREATE database geltest LOCATION "abfs://fsdata@utkuma8adls2strg.dfs.core.windows.net/hive_db" 2 1589477088 no_name 0 SQL
  3 1589477088 no_name 0 SQL
  4 1589477089 no_name 0 SQL
Summary Statistics for HADOOP are: 5 1589477089 no_name 0 SQL
Total SQL execution seconds were:                   0.724960 6 1589477089 no_name 0 SQL
Total seconds used by the HADOOP ACCESS engine were     0.728869 7 1589477089 no_name 0 SQL
  8 1589477089 no_name 0 SQL
91   quit;
...
..........
```

You can notice a new sub-folder on ADLS2 Blob Storage filesystem.

![1](img/utkuma_2020-05-14-13-30-08.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)**<-- you are here**
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
