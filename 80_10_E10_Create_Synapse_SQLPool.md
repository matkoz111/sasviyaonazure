![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Create an Azure Synapse Analytics SQL Pool Service
- [Create Azure Synapse SQL Pool Service](#create-azure-synapse-sql-pool-service)
  - [Login to Azure Dashboard](#login-to-azure-dashboard)
  - [Navigate to Resource group](#navigate-to-resource-group)
  - [Add Azure Synapse Analytics SQL Pool Service](#add-azure-synapse-analytics-sql-pool-service)
    - [Basics Tab](#basics-tab)
    - [Additional settings Tab](#additional-settings-tab)
    - [Tags Tab](#tags-tab)
    - [Review and Create Tab](#review-and-create-tab)
- [Collect Synapse SQL Pool Connection information](#collect-synapse-sql-pool-connection-information)
- [Distribute customised ODBC.ini to Viya and CAS Servers](#distribute-customised-odbcini-to-viya-and-cas-servers)

Purpose of this hands-on is to start an Azure Synapse SQL Pool Service with MS-SQL Server. Collect connection information to connect Synapse SQL Pool database.

## Create Azure Synapse SQL Pool Service

### Login to Azure Dashboard

Login to Azure Dashboard and navigate to Azure Menu.
[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

### Navigate to Resource group

Navigate to list of Azure Resource Groups from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group from the list of resource group.

![1](img/utkuma_2020-05-26-16-11-10.png)

On Resource group click on +Add menu option to add a new object.

![1](img/utkuma_2020-05-26-16-09-45.png)

### Add Azure Synapse Analytics SQL Pool Service

On New object window, select **Analytics** from Azure marketplace list and then select **Azure Synapse Analytics** to start a new **SQL Pool**.

![1](img/utkuma_2020-07-02-11-10-12.png)

#### Basics Tab

On Basics tab provide required values as follows:


```text
Subscription   : GELDM

Resources group :  e.g. utkuma8

SQL pool name   : <resource_group_name>pool  e.g. utkuma8pool

Server          : Select the MS-SQL server name you have from startup script e.g. utkuma8sqlsrv

Performance level : Gen 2 DW100c (use Select performance level to change the existing value DW1000c )

```

![1](img/utkuma_2020-07-02-11-35-40.png)

On configuration Performance tab, stay in **Gen2** sub-tab. Under **Scale your system** section slide the slide-bar to all the way to left and make it **100c**. Select **Apply** to save the changes.

![1](img/utkuma_2020-07-02-11-42-09.png)

![1](img/utkuma_2020-07-02-11-45-19.png)

Press **Next** to move on to **Additional settings** tab.

#### Additional settings Tab

Keep the default value for all the parameters. select next to move on to **Tags**  tab.

![1](img/utkuma_2020-07-02-11-49-00.png)

#### Tags Tab

On Tags tab use 'name' as tag and your NTid as value.  Select **Review and Create** to review the parameters details for Synapse SQL Pool.

![1](img/utkuma_2020-07-02-11-50-24.png)

#### Review and Create Tab

On Review tab, review the service details and select Create to deploy the service.  Wait for 5-10 min for Azure Synapse SQL pool service to be available.

![1](img/utkuma_2020-07-02-11-52-48.png)

While deployment is running you will see the following screen.

![1](img/utkuma_2020-07-02-11-55-17.png)

Once deployment completed you will see following screen.

![1](img/utkuma_2020-07-02-11-56-40.png)

## Collect Synapse SQL Pool Connection information

Navigate to your Azure Resource Group and look for **Synapse SQL Pool** Service from the list of objects.

![1](img/utkuma_2020-07-02-13-51-08.png)

Click on hyperlink of **Synapse SQL Pool** service and  will take you to Synapse SQL Pool Service overview page.

![1](img/utkuma_2020-07-02-13-53-48.png)

On overview page, under **Settings** section, select **Connection string**. On Connection strings navigate to **ODBC** tab. Copy paste the ODBC connection string in a notepad, will use these information to connect SQL pool from SAS and CAS.

![1](img/utkuma_2020-07-02-13-59-16.png)

The password is set in the startup scripts to lnxsas@2020.

You will need following similar information to connect to SQl Pool database.

```text
Server    : utkuma8sqlsrv.database.windows.net
Database  : utkuma8pool
Uid       : viyadep
Pwd       : lnxsas@2020
Port      : 1433
```

## Distribute customised ODBC.ini to Viya and CAS Servers

To access SQL pool database from SAS and CAS using SAS/ACCESS  and MS-SQL connector, you need a new DSN name in odbc.ini with SQL pool server name, database name, user id and password. A customized odbc.ini is placed in .../Scripts folder with **sqls_gelws** DNS name . The following script will update the value for above listed variable and deploy across the Viya servers.
The updated odbc.ini gets deployed to SPRE and CAS …./accessclients/ sub-folder.

The customized odbc DSN have following parameter updated from the standard template [SQL Server Wire Protocol].

```text
CryptoProtocolVersion=TLSv1.2, TLSv1.1,TLSv1, SSLv3, SSLv2
AuthenticationMethod=1
EncryptionMethod=1
ValidateServerCertificate=0
```

The script/process also copy following file to .../accessclients/lib/ folder for SSL.

```bash
cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so  /opt/sas/spre/home/lib64/accessclients/lib/

cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so /opt/sas/viya/home/lib64/accessclients/lib/
```

For more detail, take a look at spre_odbc.ini and viya_odbc.ini under Scripts folder at cldlgn.fyi.sas.com server.

Login to cldlgn.fyi.sas.com Unix server and navigate to ..../Scripts folder, from where you started Azure environment. Execute the "To_Distribute_ODBC_ini.sh" script to distribute customized odbc.ini file to Viya environment.

Code:

```bash

cd ~/PSGEL265-sas-viya-3.5....../Scripts

./To_Distribute_ODBC_ini.sh <NT ID> <Resource Group Name> <SQL Pool Database Name> < env mode >

Example:
./To_Distribute_ODBC_ini.sh utkuma utkuma8 utkuma8pool smp

```

Results:

```log

cldlgn01.unx.sas.com> ./To_Distribute_ODBC_ini.sh utkuma utkuma8 utkuma8pool smp
Checking for Azure Resource Group
Azure Resource Group Exists : utkuma8 : true
 Resource Group  : utkuma8
 SQL Pool Server Name  : utkuma8sqlsrv
 SQL Admin User Name  : viyadep
 SQL Admin User Pwd   : lnxsas@2020
 Getting Viya01 VM Public IP
Public Ip of Viya01 VM : 40.79.28.98
 Copying updated ODBC.ini file to Viya server
spre_odbc.ini                                                                                                           100% 6247   472.4KB/s   00:00
viya_odbc.ini                                                                                                           100% 6249   463.1KB/s   00:00
plugin_MsSQL_to_viya.sh                                                                                                 100% 2331   182.8KB/s   00:00
SMP Environment.
My Private Key File : ~/.ssh/private_key.pem
 Copy customised odbc.ini file to spre and CAS fodler
viya01 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "checksum": "ce0e8f1c7ab3b5902ff7f8a6db9fdb1e5cf39cd7",
    "dest": "/opt/sas/spre/home/lib64/accessclients/odbc.ini",
    "gid": 0,
    "group": "root",
    "mode": "0755",
    "owner": "root",
    "path": "/opt/sas/spre/home/lib64/accessclients/odbc.ini",
    "secontext": "system_u:object_r:usr_t:s0",
    "size": 6247,
    "state": "file",
    "uid": 0
}
....
.................
......................

cldlgn01.unx.sas.com>
```




<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)**<-- you are here**
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
