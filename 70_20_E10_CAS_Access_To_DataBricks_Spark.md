![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# CAS access to Databricks Spark table
- [Login to Viya application](#login-to-viya-application)
- [CAS load from Databricks Spark table](#cas-load-from-databricks-spark-table)

Purpose of this hands-on is to demonstrate the data access from CAS to Azure Databricks Spark Cluster table. The serial method can be used to load CAS from Azure Databricks Spark cluster using JDBC driver.

To connect Azure Databricks, it requires JDBC driver. The JDBC Jar file is downloaded, staged and distributed across the Viya environment by JDBC Plugin script.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## CAS load from Databricks Spark table

On SAS Studio execute the following code to load Databricks Spark table to CAS.

**Before executing the code change/update the URL from your Databricks Spark cluster url information. Also update the PWD=< Databricks WS token > for user token**

Notice: The URL with user name 'token' and PWD=< Databricks WS token >

Code:

```sas

CAS mySession  SESSOPTS=( CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib jdcaslib dataSource=(srctype='jdbc',
           url="jdbc:spark://adb-3776906867067670.10.azuredatabricks.net:443/default;transportMode=http;ssl=1;httpPath=sql/protocolv1/o/3776906867067670/0612-172425-nixed137;AuthMech=3;UID=token;PWD=dapicb3e9c6548bdb1ba601c1ac31989c21e"
		   class="com.simba.spark.jdbc.Driver",
           classpath="/opt/sas/viya/config/data/JDBCDriver",
           schema="default" );

proc casutil incaslib="jdcaslib" outcaslib="jdcaslib";
   list files ;
quit;

proc casutil outcaslib="jdcaslib" incaslib="jdcaslib" ;
	load casdata="baseball_prqt" casout="baseball_prqt" replace;
    list tables;
quit;

CAS mySession  TERMINATE;
```

Results:

```log
.........
.....
84
85   caslib jdcaslib dataSource=(srctype='jdbc',
86   url="jdbc:spark://adb-1734021646764232.12.azuredatabricks.net:443/default;transportMode=http;ssl=1;
86 ! httpPath=sql/protocolv1/o/1734021646764232/0629-192258-icon139;AuthMech=3;UID=token;PWD=dapi8186bc93e141056fb6a4c900d4652b44"
87              class="com.simba.spark.jdbc.Driver",
88              classpath="/opt/sas/viya/config/data/JDBCDriver",
89              schema="default" );
NOTE: Executing action 'table.addCaslib'.
NOTE: 'JDCASLIB' is now the active caslib.
NOTE: Cloud Analytic Services added the caslib 'JDCASLIB'.
NOTE: Action 'table.addCaslib' used (Total process time):
....
...........
82
83   proc casutil outcaslib="jdcaslib" incaslib="jdcaslib" ;
NOTE: The UUID 'ad9c3896-d023-0346-a87e-a33b79ade9fc' is connected using session MYSESSION.
84
84 !  load casdata="baseball_prqt" casout="baseball_prqt" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to JDBC.
NOTE: Cloud Analytic Services made the external data from baseball_prqt available as table BASEBALL_PRQT in caslib jdcaslib.
NOTE: Action 'table.loadTable' used (Total process time):
NOTE:       real time               1.988507 seconds
....
..............

```

Results:

CAS Table loaded from Databricks Spark table.

![1](img/utkuma_2020-06-29-17-16-31.png)


<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)**<-- you are here**
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
