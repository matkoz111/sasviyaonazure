![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Collect Azure MS-SQL Database connection information
- [Navigate to Azure MS-SQL server database](#navigate-to-azure-ms-sql-server-database)
- [Collect Database connection informations](#collect-database-connection-informations)
- [Customised ODBC.ini](#customised-odbcini)

Purpose of this hands-on is to collect he Azure MS-SQL connection information to connect and access data from SAS and CAS.

## Navigate to Azure MS-SQL server database

As part of environment startup process, an Azure MS-SQL server database started under Azure Resource group. You can navigate to the Azure MS-SQL Server database dashboard and view the database connection properties.

Login to Azure Dashboard and navigate to Azure Menu.

[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

Navigate to list of Azure Resource Groups from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group and view the list of objects. Click on Resource Group name hyper link to go to next window.

Navigate to the MS SQL Server Database from the list of objects.

![1](img/utkuma_2020-05-26-15-41-23.png)

## Collect Database connection informations

Click on your database to view the details. Most of the information is available on Connection strings pages. Use ODBC tab to get the connection string.

The password is set in the startup scripts to lnxsas@2020.

You will need following information to connect to MS-SQL database.

```text
Server    : <utkuma8sqlsrv>.database.windows.net
Database  : geldb
Uid       : viyadep
Pwd       : lnxsas@2020
Port      : 1433
```

![1](img/utkuma_2020-05-26-15-42-34.png)

## Customised ODBC.ini

As part of environment setup, a customized odbc.ini gets deployed to SPRE and CAS …./accessclients/ folder. The updated odbc.ini contains a new DNS entry (sqls_gel) with newly created Azure MS-SQL server name, user id and password.

The customized odbc DSN have following parameter updated from the standard template [SQL Server Wire Protocol].

```text
CryptoProtocolVersion=TLSv1.2, TLSv1.1,TLSv1, SSLv3, SSLv2
AuthenticationMethod=1
EncryptionMethod=1
ValidateServerCertificate=0
```

The environment setup script/process also copy following file to .../accessclients/lib/ folder for SSL.

```bash
cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so  /opt/sas/spre/home/lib64/accessclients/lib/

cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so /opt/sas/viya/home/lib64/accessclients/lib/
```

For more detail, take a look at spre_odbc.ini and viya_odbc.ini scripts folder at cldlgn.fyi.sas.com server.

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)**<-- you are here**
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
