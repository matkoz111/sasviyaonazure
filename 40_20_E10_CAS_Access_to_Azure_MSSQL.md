![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Data table access from CAS to Azure MS-SQL database
- [Login to Viya application](#login-to-viya-application)
- [Serial data load from Azure MS-SQL database to CAS](#serial-data-load-from-azure-ms-sql-database-to-cas)
- [Multi-Node CAS load from Azure MS-SQL database](#multi-node-cas-load-from-azure-ms-sql-database)
  - [Verify trace file at CAS Nodes](#verify-trace-file-at-cas-nodes)
    - [Now you are on CSA02 server](#now-you-are-on-csa02-server)

Purpose of this section is to demonstrate the data table access from CAS to Azure MS-SQL Database. You can use serial and multi-node mechanism to load Azure Ms-SQL database table to CAS.

To access data table from SAS Viya/CAS to Azure MS-SQL database, you need ms-sql/ODBC configuration at SPRE and CAS servers. The configuration is part of the environment setup. The vars.yml features the environment variable to deploy the ODBC configuration Parameters on Viya and CAS servers. The customized odbc.ini containing MS-SQL server is also deployed as part of environment setup.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Serial data load from Azure MS-SQL database to CAS

On SAS Studio execute the following code to serial load CAS from Azure MS-SQL database.

Code:

```sas
CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib azsqldb desc='Microsoft SQL Server Caslib'
dataSource=(srctype='sqlserver'
username='viyadep'
password='lnxsas@2020'
sqlserver_dsn="sqls_gel" );

proc casutil;
load data=sashelp.cars outcaslib="azsqldb" casout="cars" replace;
save casdata="cars" casout="cars" incaslib="azsqldb" outcaslib="azsqldb"  replace;
quit;


proc casutil incaslib="azsqldb" outcaslib="azsqldb";
load casdata="cars" casout="cars_new" replace;
load casdata="fish_sas" casout="fish_sas" replace;
list tables;
quit;

CAS mySession  TERMINATE;
```

Results:

```log
……………….
….
96
97   proc casutil incaslib="azsqldb" outcaslib="azsqldb";
NOTE: The UUID '835901a3-aec7-eb4b-9549-cd84da9f5951' is connected using session MYSESSION.
98   load casdata="cars" casout="cars_new" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to SQL Server.
NOTE: Cloud Analytic Services made the external data from cars available as table CARS_NEW in caslib azsqldb.
99   load casdata="fish_sas" casout="fish_sas" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to SQL Server.
NOTE: Cloud Analytic Services made the external data from fish_sas available as table FISH_SAS in caslib azsqldb.
…..
…………..
```

Result Output:

![1](img/utkuma_2020-05-26-16-02-32.png)

## Multi-Node CAS load from Azure MS-SQL database

**Note: This section is valid, only If you have started MPP Environment**

On SAS Studio execute the following code to load CAS from  Azure MS-SQL database using Multi-Node data load mechanism.

Code:

```sas
CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib azsqldb desc='Microsoft SQL Server Caslib'
dataSource=(srctype='sqlserver',
username='viyadep',
password='lnxsas@2020',
sqlserver_dsn="sqls_gel",
numreadnodes=10,numwritenodes=10,
DRIVER_TRACE="SQL",
DRIVER_TRACEFILE="/tmp/sasdcpg.log",
DRIVER_TRACEOPTIONS="TIMESTAMP|APPEND"
);


proc casutil incaslib="azsqldb" outcaslib="azsqldb";
load casdata="fish_sas" casout="fish_sas" options=(sliceColumn="weight")  replace;
list tables;
quit;


CAS mySession  TERMINATE;
```

Results:

```log
……….
……………………….
2   proc casutil incaslib="azsqldb" outcaslib="azsqldb";
NOTE: The UUID '0e29a7e3-d26a-8f4f-96b7-83968eea7107' is connected using session MYSESSION.
83   load casdata="fish_sas" casout="fish_sas" options=(sliceColumn="weight")  replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to SQL Server.
WARNING: The value of numReadNodes(10) exceeds the number of available worker nodes(2). The load will proceed with numReadNodes=2.
NOTE: Cloud Analytic Services made the external data from fish_sas available as table FISH_SAS in caslib azsqldb.
…………
……………………….
```

### Verify trace file at CAS Nodes

**Note: This section is valid, only If you have started MPP Environment**

Login to “cldlgn.fyi.sas.com” Unix server using moba-x-term application. Change to the .../Scripts sub-folder from where you have started the environment.

SSH to your CAS02 server using following statement. Change the IP address as you see in your "user_host_file". The server name and IP address are listed in "user_host_file", located under .../Scripts sub-folder at cldlgn.fyi.sas.com server.

Note: If there is warning about “fingerprint for the ECDSA key", ignore it.

**CAS servers IP in 'user_host_file' file under .../Scripts sub-folder**

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

more resource_info

my_private_key=~/.ssh/<NTID>-viyadep-key
my_user=viyadep
vm_public_ip=<CAS02 Public IP>

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
```

Results:

```log
cldlgn03.unx.sas.com> ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
Last login: Thu Apr 30 14:03:58 2020 from 100.17.37.188
[viyadep@cas02~]$



```

#### Now you are on CSA02 server

Now you are on CAS02 server, execute the following statement to view the trace file.

Code:

```bash
more /tmp/sasdcpg.log | grep 'DRIVER SQL'
```

Results:
You will notice the following statement in the trace log file.

```log
[viyadep@cas02 ~]$ more /tmp/sasdcpg.log | grep 'DRIVER SQL'
19.14.16.38:          DRIVER SQL: "select * from geldb.dbo.fish_sas where 1=0 " on connection 0x0000000040034b80
19.14.16.50:          DRIVER SQL: "select SLICE_SQL.Species, SLICE_SQL.Weight, SLICE_SQL.Length1, SLICE_SQL.Length2, SLICE_SQL.Length3, SLICE_SQL.Height, SLICE_SQL.Width from  (select geldb.dbo.fish_sas.Species, geldb.dbo.fish_sas.Weight, geldb.dbo.fish_sas.Length1, geldb.dbo.fish_sas.Length2, geldb.dbo.fish_sas.Length3, geldb.dbo.fish_sas.Height, geldb.dbo.fish_sas.Width from geldb.dbo.fish_sas)  SLICE_SQL where  ( ( ( CAST(FLOOR (ABS (LOG10 (ABS (SLICE_SQL.Weight) ) ) )  AS BIGINT)  % 2=0)  or SLICE_SQL.Weight IS NULL) ) " on connection 0x0000000040042e20
[viyadep@cas02 ~]$
````

Exit out of CAS02 server.
Code:

```bash
exit
```
<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)**<-- you are here**
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
