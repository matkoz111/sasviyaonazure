![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Collect HDI Connection Info and validate the access
- [Collect HDInsight cluster Connection Information](#collect-hdinsight-cluster-connection-information)
  - [Login to Azure Dashboard](#login-to-azure-dashboard)
  - [Navigate to Resource Group](#navigate-to-resource-group)
  - [Navigate to the HDInsight Cluster](#navigate-to-the-hdinsight-cluster)
  - [HDInsight URL](#hdinsight-url)
  - [Hive connection string](#hive-connection-string)
  - [Optional Steps](#optional-steps)
- [Validate access to Azure HDInsight](#validate-access-to-azure-hdinsight)
  - [Login to Viya Application](#login-to-viya-application)
  - [Save a SAS table to HDInsight Hive database](#save-a-sas-table-to-hdinsight-hive-database)

Purpose of this hands-on is to review and collect connection information of HDInsight cluster to access from SAS and CAS. Execute a sample sas code to validate the access.

## Collect HDInsight cluster Connection Information

### Login to Azure Dashboard

Login to Azure Dashboard and navigate to Azure Menu.
[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

### Navigate to Resource Group

Navigate to list of Azure Resource Groups from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group and view the list of objects. Click on Resource Group name hyper link to go to next window.

### Navigate to the HDInsight Cluster

CLink on HDI Cluster hyperlink from the list of the objects.

![1](img/utkuma_2020-05-26-16-28-31.png)

### HDInsight URL

On HDInsight Cluster navigate to overview section and look for URL. This is the server name you will use to connect to Hive database service.

![1](img/utkuma_2020-05-26-16-29-32.png)

### Hive connection string

The following information is required to connect to hive database. User id and password is obtained from creating the HDI cluster process. Replace the server name with your HDI cluster name.

```sas
uri='jdbc:hive2://hdiutkuma8.azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2'
server='hdiutkuma8.azurehdinsight.net'
user="admin"
pw="SASV3i5ya@2020"
schema="default";
```

### Optional Steps

You can login to Ambari UI by using same URL, user id, and password to view the Hadoop cluster and it's components. This will take you to Ambari dashboard.

![1](img/utkuma_2020-06-22-15-37-44.png)

![1](img/utkuma_2020-06-22-15-39-20.png)


## Validate access to Azure HDInsight

The purpose of this section is to validate the Azure HDI Hive database access from SAS 9.4 environment using SAS/ACCESS.

As part of HDI Plugin script, it distributes the Jar and Config files to Viya and CAS servers.

### Login to Viya Application

Login to SAS Viya application using following information.

```text
link    : http://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

### Save a SAS table to HDInsight Hive database

On SAS Studio execute the following code to save a data table to Azure HDInsight hive/hadoop database.

Before executing the code set the HDINM= parameter with your HDI name.

Code:

```sas

/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;

/*
Example:
%let HDINM=hdiutkuma8;
*/

option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
options sastrace = ',,,sd' sastraceloc = saslog ;

libname hdilib HADOOP
uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
server="&HDINM..azurehdinsight.net"
user="admin" pw="SASV3i5ya@2020" schema="default";


data hdilib.cars9;
   set sashelp.cars;
run ;

proc sql outobs=20;
select * from hdilib.cars9;
quit;

```

Results:

```log
81 %let HDINM=hdiutkuma8;

82   option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
83   option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
84   options sastrace = ',,,sd' sastraceloc = saslog ;
85
86   libname hdilib HADOOP
87   uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;
87 ! hive.server2.thrift.http.path=hive2"
88   server="&HDINM..azurehdinsight.net"
89   user="admin" pw=XXXXXXXXXXXXXXXX schema="default";
NOTE: Libref HDILIB was successfully assigned as follows:
      Engine:        HADOOP
      Physical Name:
      jdbc:hive2://hdiutkuma8.azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path
      =hive2
90
91

82
83   data hdilib.cars9;
84      set sashelp.cars;
85   run;
  109 1589157229 no_name 0 DATASTEP
HADOOP_1: Prepared: on connection 1 110 1589157229 no_name 0 DATASTEP
SHOW TABLES IN `default` 'CARS9' 111 1589157229 no_name 0 DATASTEP
  112 1589157229 no_name 0 DATASTEP
  113 1589157230 no_name 0 DATASTEP
Summary Statistics for HADOOP are: 114 1589157230 no_name 0 DATASTEP
Total SQL prepare seconds were:                     0.618338 115 1589157230 no_name 0 DATASTEP
Total seconds used by the HADOOP ACCESS engine were     0.696233 116 1589157230 no_name 0 DATASTEP
  117 1589157230 no_name 0 DATASTEP
NOTE: SAS variable labels, formats, and lengths are not written to DBMS tables.
  118 1589157230 no_name 0 DATASTEP
HADOOP_2: Executed: on connection 2 119 1589157230 no_name 0 DATASTEP
CREATE TABLE `default`.`CARS9` (`Make` VARCHAR(13),`Model` VARCHAR(40),`Type` VARCHAR(8),`Origin` VARCHAR(6),`DriveTrain`
VARCHAR(5),`MSRP` DOUBLE,`Invoice` DOUBLE,`EngineSize` DOUBLE,`Cylinders` DOUBLE,`Horsepower` DOUBLE,`MPG_City`
DOUBLE,`MPG_Highway` DOUBLE,`Weight` DOUBLE,`Wheelbase` DOUBLE,`Length` DOUBLE) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001'
LINES TERMINATED BY '\012' STORED AS TEXTFILE  TBLPROPERTIES ('SAS OS Name'='Linux','SAS Version'='V.03.05M0P11112019')  120
1589157230 no_name 0 DATASTEP
  121 1589157230 no_name 0 DATASTEP
NOTE: There were 428 observations read from the data set SASHELP.CARS.
NOTE: The data set HDILIB.CARS9 has 428 observations and 15 variables.
  122 1589157232 no_name 0 DATASTEP
HADOOP_3: Executed: on connection 2 123 1589157232 no_name 0 DATASTEP
LOAD DATA INPATH '/tmp/sasdata-e1-2020-05-11-00-33-51-0001.dlv' OVERWRITE INTO TABLE `default`.`CARS9` 124 1589157232 no_name 0
DATASTEP
….
…..
```

Results from PROC SQL statement:

![1](img/utkuma_2020-05-14-12-01-49.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)**<-- you are here**
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
