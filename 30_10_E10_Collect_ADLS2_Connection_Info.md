![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Collect ADLS2 connection information
- [Navigate to ADLS2 Storage account](#navigate-to-adls2-storage-account)
- [Collect ADLS2 information](#collect-adls2-information)
- [Azure Application Authentication Permission](#azure-application-authentication-permission)

The purpose of this hands-on is to collect ADLS2 connection information to access data files from SAS and CAS.

## Navigate to ADLS2 Storage account

As part of environment startup process, an Azure Data Lake Storage 2 (ADLS2) Storage Account is created under specified Resource Group. You can navigate to the Azure Storage Account and view the properties and data location container/filesystem.

Login to Azure Dashboard and navigate to Azure Menu.

[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

Navigate to list of Azure Resource Group from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group and view the list of objects. Click on Resource Group hyper link to move on to next window.

![1](img/utkuma_2020-05-26-14-43-10.png)

Navigate to the ADLS2 Storage Account from list of the objects.

![1](img/utkuma_2020-05-26-14-47-19.png)

Navigate to the ADLS2 Storage Account container.

![1](img/utkuma_2020-05-26-14-48-33.png)

Navigate to container and view the information about data folders and files.

![1](img/utkuma_2020-06-16-14-53-17.png)

## Collect ADLS2 information

You will need following information to connect to ADLS2 storage account data folder. You can collect and save it in a notepad/text editor. The first 3 parameter information is available on Storage Account -> Storage Explorer Preview window.

```text
example:
accountname= "utkuma5adls2strg"
filesystem= "fsutkuma5adls2strg"
path= "/sample_data"

tenantid="b1c14d5c-3625-45b3-a430-9552373a0c2f"
applicationId="a2e7cfdc-93f8-4f12-9eda-81cf09axxxxx"
```

Application Id and Tenant Id can be found at Azure App registration page. Navigate to Azure Active Directory and then to App Registrations page.

![1](img/utkuma_2020-05-26-14-50-26.png)

On Azure Active Directory page, select App Registration.

![1](img/utkuma_2020-05-26-14-51-28.png)

Select "All Applications" tab. In the search box type 'sasgel' to filter the application from the list to view the properties. Make sure to select the application which has status as 'current' for Certificate & secret.

![1](img/utkuma_2020-10-09-10-13-38.png)

Collect Tenant Id and Application Id from this page.

![1](img/utkuma_2020-05-26-14-52-56.png)

```text
example:
tenantid="b1c14d5c-3625-45b3-a430-9552373a0c2f"
applicationId="a2e7cfdc-93f8-4f12-9eda-81cf09axxxxx"
```

To access ADLS2 storage from Viya application, Azure application must have following API/Permission set on Azure Application Id.

![1](img/utkuma_2020-05-28-15-54-47.png)

## Azure Application Authentication Permission

**Note : This section is meant for information purpose only, no need to update/change permission for 'sasgel' application.**

To access ADLS2 storage from Viya application, Azure application must have following Authentication set on Azure Application Id.

![1](img/utkuma_2020-05-28-15-59-32.png)

![1](img/utkuma_2020-05-28-16-02-15.png)

![1](img/utkuma_2020-05-28-16-03-32.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)**<-- you are here**
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
