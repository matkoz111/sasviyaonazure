![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# CAS table load/save from ADLS2 via HDInsight Hive
- [Login to Viya application](#login-to-viya-application)
- [CAS Serial save/load from ADLS2 via Azure HDI Hive](#cas-serial-saveload-from-adls2-via-azure-hdi-hive)
  - [Data file at ADLS2 Location](#data-file-at-adls2-location)

Purpose of this hands-on is to demonstrate the data access from CAS to Azure HDI Hive database table. You can use serial and multi-node data load method to load CAS from Azure HDI Hive database table. The Hive database can have data files to local HDFS or external Blob Storage ADLS2 location. In this hands on you will use external Hive database to load and save CAS table.

To connect Azure HDI Hive, it requires Hadoop Jar and Config files from HDI hadoop cluster. The Jar and config files were collected by HDI Plugin script and distributed across Viya environment.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## CAS Serial save/load from ADLS2 via Azure HDI Hive

On SAS Studio execute the following code to save and load a CAS table to Hive Database with external location (ADLS2).

**Before executing the code reset the HDINM= parameter with your HDI cluster name.**

Notice: The URI link with geltest and schema="geltest".

Code:

```sas
/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;

/*
Example:
%let HDINM=hdiutkuma8;
*/

CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib cashive datasource=(srctype="hadoop",
server="&HDINM..azurehdinsight.net",
username="admin", pwd="SASV3i5ya@2020", schema=geltest,
uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/geltest;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2",
hadoopconfigdir="/opt/sas/viya/config/data/HDIHadoopData/conf",
hadoopjarpath="/opt/sas/viya/config/data/HDIHadoopData/lib");

proc casutil incaslib="cashive" outcaslib="cashive"  ;
load data=sashelp.prdsal3 casout="prdsal3" replace;
save casdata="prdsal3" casout="prdsal3"  replace;
quit;

proc casutil incaslib="cashive" outcaslib="cashive";
load casdata="prdsal3" casout="prdsal3_new" replace;
list tables;
quit;

CAS mySession  TERMINATE;
```

Results:

```log
.........
.....
84   caslib cashive datasource=(srctype="hadoop",
85   server="&HDINM..azurehdinsight.net",
86   username="admin", pwd=XXXXXXXXXXXXXXXX, schema=geltest,
87   uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/geltest;ssl=true?hive.server2.transport.mode=http;
87 ! hive.server2.thrift.http.path=hive2",
88   hadoopconfigdir="/opt/sas/viya/config/data/HDIHadoopData/conf",
89   hadoopjarpath="/opt/sas/viya/config/data/HDIHadoopData/lib");
NOTE: Executing action 'table.addCaslib'.
NOTE: 'CASHIVE' is now the active caslib.
...
.........
NOTE: SASHELP.PRDSAL3 was successfully added to the "CASHIVE" caslib as "PRDSAL3".
93   save casdata="prdsal3" casout="prdsal3"  replace;
NOTE: Executing action 'table.save'.
NOTE: Performing serial SaveTable action using SAS Data Connector to Hadoop.
NOTE: Cloud Analytic Services saved the file prdsal3 in caslib CASHIVE.
....
...........
97   load casdata="prdsal3" casout="prdsal3_new" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to Hadoop.
NOTE: Cloud Analytic Services made the external data from prdsal3 available as table PRDSAL3_NEW in caslib cashive.
.....
..........

```

Results:

CAS Table loaded from Blob storage data file.

![1](img/utkuma_2020-05-14-14-36-56.png)

### Data file at ADLS2 Location

Navigate to Blob Storage Account Container 'fsdata'
to find the new sub-folder and data files. For each table there will a new sub-folder under hive_db folder.

Data folder created by CAS table save operation.

![1](img/utkuma_2020-05-14-14-38-46.png)

Data file created by CAS table save operation.

![1](img/utkuma_2020-05-14-14-39-54.png)


<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)**<-- you are here**
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
