![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Start Azure Data Management Environment
- [Login to cldlgn.fyi.sas.com server](#login-to-cldlgnfyisascom-server)
- [Download/clone GITLAB project](#downloadclone-gitlab-project)
- [Start Azure Viya environment](#start-azure-viya-environment)
- [Validate SAS Viya Environment](#validate-sas-viya-environment)
  - [Check Status of Viya Services](#check-status-of-viya-services)
  - [Open SAS Viya application](#open-sas-viya-application)
  - [Troubleshoot - Viya01 Server not found issue](#troubleshoot-viya01-server-not-found-issue)

Purpose of this hands-on is to start Azure Data Management environment under SAS Managed Azure Account.The Azure Data Management environment starts using set of Unix shell scripts having Azure CLI.

The environment consists of Azure VM Servers, Storage Account (ADLS2), MS-SQL Server, HDInsight cluster, Databricks Spark cluster, and Synapse SQL Pool. User needs to login to cldlgn.fyi.sas.com server and clone/download the giltlab project. The downloaded project contains a script folder. Execute the startup script and it  will create Azure VM machine/s and deploy the SAS Viya software.

The Azure Data Management  environment:

![1](img/utkuma_2020-07-09-11-02-51.png)

## Login to cldlgn.fyi.sas.com server

Login to cldlgn.fyi.sas.com Unix server using following information.

```text
Remote host : cldlgn.fyi.sas.com
User Name   : <SAS Windows NT user id>
Password    : <SAS Windows NT password>
```

## Download/clone GITLAB project

Download/clone the Azure Data Management gitlab project to get all scripts by running following statement.

Code:

```bash
cd ~

git clone https://gitlab.sas.com/GEL/workshops/PSGEL265-sas-viya-3.5-data-management-on-azure-cloud.git

ls -lrt
```

Results:

```log
cldlgn05.unx.sas.com> git clone https://gitlab.sas.com/GEL/workshops/PSGEL265-sas-viya-3.5-data-management-on-azure-cloud.git
Cloning into 'PSGEL265-sas-viya-3.5-data-management-on-azure-cloud'...
remote: Enumerating objects: 287, done.
remote: Counting objects: 100% (287/287), done.
remote: Compressing objects: 100% (215/215), done.
remote: Total 287 (delta 79), reused 274 (delta 66), pack-reused 0
Receiving objects: 100% (287/287), 8.18 MiB | 36.73 MiB/s, done.
Resolving deltas: 100% (79/79), done.
cldlgn05.unx.sas.com>

cldlgn05.unx.sas.com> ls -lrt
total 76
....
.....

drwxr-xr-x 6 utkuma users 4096 May 28 13:14  PSGEL265-sas-viya-3.5-data-management-on-azure-cloud
```

## Start Azure Viya environment

There are set of Azure CLI script to start the Viya environment.The script creates new Azure VM machines using Azure data disk snapshots. The Azure disk snapshots  contains Viya software mirror.The environment starts with Viya35, ADLS2 storage, and SQL Server. There are separate steps to start HDInsight cluster, Databricks cluster, and Synapse SQL Pool.

Start environment by executing **To_Start_AZ_Environment.sh** script from ~/PSGEL265-sas-viya-3.5....../Scripts sub-folder.The Script takes three parameters:

```text
<Win NT ID>
<New Resource Group name>
<Environment Mode>.
```

The new Resource Group could be your NT ID and Environment mode could be either ‘smp’ or ‘mpp’.
The SMP mode will start Viya Environment on single machine.

Please avoid using any underscore or hyphen or any special character in Resource group name.

The software deployment takes at least 2:00 Hrs, so be patient and it will come out.In case Unix session times out while software deployment is running, it is a good idea to use tmux to avoid a disconnection. Open a tmux session before executing the startup script.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

chmod 755 *

tmux

./To_Start_AZ_Environment.sh <WIN NT ID> <WIN NT IDn> <smp / mpp >

Note: Provide  all parameters in lower-case. For second parameter, just use Win NT id with number 1-9.

Example :
./To_Start_AZ_Environment.sh utkuma utkuma5 smp
```

[Tmux cheat sheet](https://tmuxcheatsheet.com/)
- To detach from tmux session  "Ctrl + b" d
- To attach to last tmux session "$ tmux a"
- To moved to next tmux session "Ctrl + b" )
- To move to previous tmux session "Ctrl + b" (
- To list tmux sessions "$ tmux ls"

Note : In case you are using environment for multiple days, please stop the environment at the end of your working day to minimize the Azure cost charged to your account/cost-center. Follow the steps from **"Stop Terminate Environment"** hands-on document to stop the environment. This document features at the end of document index.

Note : The startup script will create all required objects under provided resource group name.

As a result of above script execution, on Azure Resource Group Dashboard you will see the list of objects.  Notice the Resource group with your NTIDn.

![1](img/utkuma_2020-05-26-14-28-17.png)

## Validate SAS Viya Environment

On to “cldlgn.fyi.sas.com” server, there will be a text file created with name “user_host_file” under ~/PSGEL265-sas-viya-3.5......../Scripts sub-folder. Use the information from this file and add an entry into your local desktop under C:\windows\System32\drivers\etc\hosts file for Viya and CAS servers. This will help to access the Viya environment from local machine using URL.

Login to “cldlgn.fyi.sas.com” server and cd to .../Scripts sub-folder. Concatenate the “user_host_file” file and highlight the contents to copy into copy-buffer.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

cat user_host_file
```

![1](img/utkuma_2020-05-28-13-39-12.png)

![1](img/utkuma_2020-05-26-14-29-53.png)

Paste the copy-buffer contents into C:\windows\System32\drivers\etc\hosts file. Make sure you have opened the ....\etc\hosts file as administrator to update.

![1](img/utkuma_2020-05-26-14-32-21.png)

### Check Status of Viya Services

Once Viya software deployment or environment re-start process completed, verify the status of services before accessing Viya environment.

Check the status of Viya services by executing **To_Status_AZ_ViyaServer_Services.sh** script from ~/PSGEL265-sas-viya-3.5....../sub-folder.The Script takes two parameters:

```text
<Win NT ID>
<New Resource Group name>
```

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

./To_Status_AZ_ViyaServer_Services.sh <WIN NT ID> <WIN NT IDn>


Example :
./To_Status_AZ_ViyaServer_Services.sh utkuma utkuma5
```

### Open SAS Viya application

Once Viya software deployment completed use following link and credential to login to Viya environment.
<mark>If you receive a "server not found" message when trying to connect to https://viya01/SASDrive, then perform the steps in the [Troubleshoot - Viya01 Server not found issue](#troubleshoot-viya01-server-not-found-issue) section.</mark>

```text
Link    : https://viya01/SASDrive
User-id : viyademo01
Password: lnxsas
```



### Troubleshoot Viya01 Server not found issue

**Issue:**

As part of environment startup process. , you get an Azure Network security group created and configured for inbound network traffic on specific port. The Azure Virtual machine are using same Network Security group. To let SAS network traffic in, 149.173.0.0/16 is configured for port 22,443 and 143.

If you have logged into SAS network using "Cisco AnyConnect" VPN application, the external Ip Address for your local desktop/laptop may not start with 149.173.[ ].[ ] and not able to open the Viya application. The external Ip address of your local desktop/laptop depends on VPN server you choose to connect.

You can check the external IP address of local desktop/laptop using link at same machine:
http://ip4.me/

![1](img/utkuma_2020-06-10-16-41-33.png)

**Resolution:**
Depending on your location, Use either one of the option to resolve the issue.

**Option-1. Direct Connection to Cary VPN server**

While connecting to SAS VPN Network, choose **"Direct to Cary - SAS Device"** option and your desktop/laptop will be assigned with 149.173.[ ].[ ] external Ip address.

![1](img/utkuma_2020-06-10-16-10-31.png)

External Ip assigned to local desktop/laptop after using **Direct to Cary - SAS Device** VPN connection.

![1](img/utkuma_2020-06-10-17-11-13.png)

**Option-2. Update Azure Network Security Group**

Login to Azure Dashboard and navigate to Azure Menu.

[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

Navigate to list of Azure Resource Group from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Your Azure Resource Group and view the list of objects. Click on **Resource Group** hyper link to move on to next window.

![1](img/utkuma_2020-06-11-13-21-39.png)

Navigate to the **Network Security Group** from list of the objects. Click on **Viya35v01-nsg** hyper link to move on to next window.

![1](img/utkuma_2020-06-11-13-23-43.png)

Click on 'ssh' Inbound Security rule to open and update the source.

![1](img/utkuma_2020-06-11-13-28-31.png)

Add external (using http://ip4.me/) Ip address of your local desktop/laptop at the end of the existing entry. Use ',' to separator new additional entry. Save the changes.

![1](img/utkuma_2020-06-11-13-40-16.png)

To get your external Ip of Local Desktop/laptop.

http://ip4.me/
![1](img/utkuma_2020-06-10-16-41-33.png)

After applying changes to **Network Security Group** , go back to  **Open SAS Viya Application** step and validate the access to Viya Application.

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)**<-- you are here**
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
