![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Save SAS datasets to ADLS2 Blob Storage with parquet file
- [Login to Viya application](#login-to-viya-application)
- [Save a SAS Datastes to Hive Database with storage location to ADLS2](#save-a-sas-datastes-to-hive-database-with-storage-location-to-adls2)
- [Data file at ADLS2 Location](#data-file-at-adls2-location)

Purpose of this hands-on is to save a sas datasets table to ADLS2 via Hive. The LIBNAME uses the schema name of new database name, created in previous steps. The new database data file path is located at ADLS2 rather than HDFS. This means any hive table saved under this database will have data file at ADLS2.

**"schema=geltest" used in connection string**

## Login to Viya application

Login to SAS Viya application using following informations.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Save a SAS Datastes to Hive Database with storage location to ADLS2

On SAS Studio execute the following code to save a SAS datasets to Hive Database with external location (ADLS2) and customized data file type.

Before executing the code change/update the HDI Server name in URI= and Server= parameters.

Notice the URI link with "geltest" and schema="geltest". The data file type to save as used DBCREATE_TABLE_OPTS="stored as parquet".

Make sure you have **"geltest" database name in LIBNAME statement.** Update/change the **Schema="geltest"** and **URI=.....net/:443/geltest;.....**

Code:

```sas
/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;

/*
Example:
%let HDINM=hdiutkuma8;
*/


option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
options sastrace = ',,,sd' sastraceloc = saslog ;


libname hdilib HADOOP
uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/geltest;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
server="&HDINM..azurehdinsight.net"
user="admin" pw="SASV3i5ya@2020" schema="geltest"
DBCREATE_TABLE_OPTS="stored as parquet" ;

data hdilib.prdsal2;
   set sashelp.prdsal2;
run;
```

Results:

```log

....
...........
87   libname hdilib HADOOP
88   uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/geltest;ssl=true?hive.server2.transport.mode=http;
88 ! hive.server2.thrift.http.path=hive2"
89   server="&HDINM..azurehdinsight.net"
90   user="admin" pw=XXXXXXXXXXXXXXXX schema="geltest"
91   DBCREATE_TABLE_OPTS="stored as parquet" ;
NOTE: Libref HDILIB was successfully assigned as follows:
      Engine:        HADOOP
      Physical Name:
      jdbc:hive2://hdiutkuma8.azurehdinsight.net:443/geltest;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path
      =hive2
92
93   data hdilib.prdsal2;
94      set sashelp.prdsal2;
95   run;
  9 1589477719 no_name 0 DATASTEP
HADOOP_1: Prepared: on connection 1 10 1589477719 no_name 0 DATASTEP
SHOW TABLES IN `geltest` 'PRDSAL2' 11 1589477719 no_name 0 DATASTEP
  12 1589477719 no_name 0 DATASTEP
  13 1589477719 no_name 0 DATASTEP
Summary Statistics for HADOOP are: 14 1589477719 no_name 0 DATASTEP
Total SQL prepare seconds were:                     0.505650 15 1589477719 no_name 0 DATASTEP
Total seconds used by the HADOOP ACCESS engine were     0.596009 16 1589477719 no_name 0 DATASTEP
  17 1589477719 no_name 0 DATASTEP
NOTE: SAS variable labels, formats, and lengths are not written to DBMS tables.
  18 1589477720 no_name 0 DATASTEP
HADOOP_2: Executed: on connection 2 19 1589477720 no_name 0 DATASTEP
CREATE TABLE `geltest`.`PRDSAL2` (`COUNTRY` VARCHAR(10),`STATE` VARCHAR(22),`COUNTY` VARCHAR(20),`ACTUAL` DOUBLE,`PREDICT`
DOUBLE,`PRODTYPE` VARCHAR(10),`PRODUCT` VARCHAR(10),`YEAR` SMALLINT,`QUARTER` INT,`MONTH` DATE,`MONYR` DATE) stored as parquet
TBLPROPERTIES ('SAS OS Name'='Linux','SAS Version'='V.03.05M0P11112019')  20 1589477720 no_name 0 DATASTEP
  21 1589477720 no_name 0 DATASTEP
NOTE: There were 23040 observations read from the data set SASHELP.PRDSAL2.
NOTE: The data set HDILIB.PRDSAL2 has 23040 observations and 11 variables.
  22 1589477723 no_name 0 DATASTEP
HADOOP_3: Executed: on connection 2 23 1589477723 no_name 0 DATASTEP
CREATE TEMPORARY TABLE `geltest`.`sastmp_05_14_17_35_23_614_00001`( `COUNTRY` VARCHAR(10),`STATE` VARCHAR(22),`COUNTY`
VARCHAR(20),`ACTUAL` DOUBLE,`PREDICT` DOUBLE,`PRODTYPE` VARCHAR(10),`PRODUCT` VARCHAR(10),`YEAR` SMALLINT,`QUARTER` INT,`MONTH`
DATE,`MONYR` DATE )  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\012' STORED AS TEXTFILE 24 1589477723
no_name 0 DATASTEP
  25 1589477723 no_name 0 DATASTEP
  26 1589477723 no_name 0 DATASTEP
HADOOP_4: Executed: on connection 2 27 1589477723 no_name 0 DATASTEP
LOAD DATA INPATH '/tmp/sasdata-e1-2020-05-14-17-35-20-0000.dlv' OVERWRITE INTO TABLE `geltest`.`sastmp_05_14_17_35_23_614_00001` 28
.......
...........
```

## Data file at ADLS2 Location

Navigate to Blob Storage Account Container 'fsdata'
to find the new sub-folder and data files. For each table there will a new sub-folder under hive_db folder.

![1](img/utkuma_2020-05-14-13-41-39.png)

![1](img/utkuma_2020-05-14-13-42-30.png)

Note: The data file saved at ADLS2 is a parquet file ( due to LIBNAME contains DBCREATE_TABLE_OPTS="stored as parquet" )

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)**<-- you are here**
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
