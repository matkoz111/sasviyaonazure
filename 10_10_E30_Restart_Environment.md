![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Restart Stopped environment

Purpose of this hands-on is to Re-start Azure Data Management environment, if you have stopped the Azure Data Management environment for multiple day usage. You can restart it from cldlgn.fyi.sas.com server by running following script.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

tmux

./To_ReStart_AZ_Environment.sh <WinNTID> <WinNTIDn> < smp / mpp>

Example:
./To_ReStart_AZ_Environment.sh utkuma utkuma5 smp
```

[Tmux cheat sheet](https://tmuxcheatsheet.com/)
- To detach from tmux session  "Ctrl + b" d
- To attach to last tmux session "$ tmux a"
- To moved to next tmux session "Ctrl + b" )
- To move to previous tmux session "Ctrl + b" (
- To list tmux sessions "$ tmux ls"



**The restart of SAS Viya services will take ~15-20 Min, check the status of services by running ./To_Status_AZ_ViyaServer_Services.sh < WIN NT ID> < WIN NT IDn>**

## Validate SAS Viya Environment

When you re-start Azure Data Management from an stopped collection **the Virtual machine will have new Public-Ip address assigned**. The Viya and CAS servers will starts with new Public Ips. The Private Ip remains same and does not effect the Viya services. To access the Viya application using link https://viya01/SASDrive , you need to update the C:\windows\System32\drivers\etc\hosts again.

On to “cldlgn.fyi.sas.com” server, the restart process will re-create “user_host_file” with new Ip address under ~/PSGEL265-sas-viya-3.5......../Scripts sub-folder. Use the information from this file and add an entry into your local desktop under C:\windows\System32\drivers\etc\hosts file for Viya and CAS servers. This will help to access the Viya environment from local machine using URL.

Login to “cldlgn.fyi.sas.com” server and cd to .../Scripts sub-folder. Concatenate the “user_host_file” file and highlight the contents to copy into copy-buffer.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

cat user_host_file
```

![1](img/utkuma_2020-05-28-13-39-12.png)

![1](img/utkuma_2020-05-26-14-29-53.png)

Paste the copy-buffer contents into C:\windows\System32\drivers\etc\hosts file. Make sure you have opened the ....\etc\hosts file as administrator to update.

![1](img/utkuma_2020-05-26-14-32-21.png)

### Check Status of Viya Services

Once Viya software deployment or environment re-start process completed, verify the status of services before accessing Viya environment.

Check the status of Viya services by executing **To_Status_AZ_ViyaServer_Services.sh** script from ~/PSGEL265-sas-viya-3.5....../sub-folder.The Script takes two parameters:

```text
<Win NT ID>
<New Resource Group name>
```

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

./To_Status_AZ_ViyaServer_Services.sh <WIN NT ID> <WIN NT IDn>


Example :
./To_Status_AZ_ViyaServer_Services.sh utkuma utkuma5
```

### Open SAS Viya application

Once Viya software deployment completed use following link and credential to login to Viya environment.

```text
Link    : https://viya01/SASDrive
User-id : viyademo01
Password: lnxsas
```

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)**<-- you are here**
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
