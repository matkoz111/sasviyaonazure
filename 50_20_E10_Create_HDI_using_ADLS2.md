![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Create an Azure HDInsight cluster using Azure Data Lake Storage 2 (ADLS2)
- [Create HDInsight Cluster using ADLS2 storage](#create-hdinsight-cluster-using-adls2-storage)
  - [Login to Azure Dashboard](#login-to-azure-dashboard)
  - [Navigate to Resource group](#navigate-to-resource-group)
  - [Add HDInsight cluster](#add-hdinsight-cluster)
    - [Basic Tab](#basic-tab)
      - [Basic Part - 1](#basic-part---1)
      - [Basic Part - 2](#basic-part---2)
      - [Basic Part - 3](#basic-part---3)
      - [Basic Part - 4](#basic-part---4)
    - [Storage Tab](#storage-tab)
    - [Security and Networking Tab](#security-and-networking-tab)
    - [Config and Pricing Tab](#config-and-pricing-tab)
    - [Tag Tab](#tag-tab)
    - [Review and Create Tab](#review-and-create-tab)
- [Collect Hadoop Jar and Config files](#collect-hadoop-jar-and-config-files)

Purpose of this hands-on is to start an Azure HDInsight cluster using ADLS2 storage location for HDFS. Collect Hadoop Jar and Config files from HDI master node. The Hadoop Jar and Config files are being used to connect HDI hive data tables from SAS 9.4 and CAS.

## Create HDInsight Cluster using ADLS2 storage

Simple Azure CLI does not support creation of Azure HDInsight Cluster on ADLS2. To start HDI Cluster at ADLS2 either you use CLI with resource and parameter file or use Azure UI. When using CLI user needs to update resource and parameter file before executing it. For user convenience, we are using UI to create the cluster.

### Login to Azure Dashboard

Login to Azure Dashboard and navigate to Azure Menu.
[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

### Navigate to Resource group

Navigate to list of Azure Resource Groups from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group from the list of resource group.

![1](img/utkuma_2020-05-26-16-11-10.png)

On Resource group click on +Add menu option to add a new object.

![1](img/utkuma_2020-05-26-16-09-45.png)

### Add HDInsight cluster

On New object window, select Analytics from Azure marketplace list and then select Azure HDInsight to start a new HDInsight cluster.

![1](img/utkuma_2020-05-26-16-11-59.png)

#### Basic Tab

On Basics tab provide required values as follows:

##### Basic Part - 1

```text
Subscription   : GELDM

Resources group:  e.g. utkuma8

Cluster name   : hdi<resource_group_name> e.g. hdiutkuma8

Region   : (US) EAST US 2
```

![1](img/utkuma_2020-05-26-16-12-34.png)

##### Basic Part - 2

```text
Cluster type  : Hadoop  ( click on “select cluster type” hyperlink to get list of available type). Keep the Hadoop version 2.7

Cluster login user  : admin

Cluster login pwd : SASV3i5ya@2020

Secure shell login : viyadep
- Uncheck the use cluster log pws for SSH.

SSH Auth type  : PUBLIC KEY
```

Please keep the admin password safe as this will required to login to Hadoop Cluster Ambari UI and to access hive tables.

![1](img/utkuma_2020-05-26-16-13-28.png)

##### Basic Part - 3

Select Public key for user 'viyadep'. Make sure you have copied the public key from cldlgn.fyi.sas.com unix server to local machine. The public key file is located at ~/.ssh folder at cldlgn.fyi.sas.com unix server.

![1](img/utkuma_2020-05-26-16-15-18.png)

##### Basic Part - 4

With SSH key:
![1](img/utkuma_2020-05-26-16-16-06.png)

Press Next to move on to Storage tab.

#### Storage Tab

On Storage tab provide required values as follows:

```text
Primary Storage type : Azure Data Lake Storage Gen2 - use drop down list to select it.

Primary Storage account : e.g. utkuma8adls2strg (available from startup script)

Filesystem : keep the default value or make it little short name.

User-assigned managed identity : uidgelhdi
– use this uid as it has “blob storage data owner role on Storage account”
```

A generic User Managed Identity account has been created for HDInsight cluster access to ADLS2 Blob objects.

keep the default values for rest of parameters except for above mentioned parameters. Select Next to move on to Networking tab.

![1](img/utkuma_2020-05-26-16-17-32.png)

#### Security and Networking Tab

Select Virtual network and subnet from drop down list. Choose the VNET name which is used for starting Viya VM servers.

Keep the default value for rest of the parameters and select next to move on to Config and Pricing tab.

![1](img/utkuma_2020-05-26-16-18-19.png)

#### Config and Pricing Tab

Select D12 V2 as node size for both Head node and worker node. Keep Number of nodes 2 for both head and worker node.

Select next to move on to Tag tab.

![1](img/utkuma_2020-05-26-16-18-55.png)

#### Tag Tab

On Tag tab use 'name' as tag and your NTid as value.  Select Next to review and create the HDI cluster.

![1](img/utkuma_2020-05-26-16-19-25.png)

#### Review and Create Tab

On Review tab, if the review process is ok and have green check. Select Create option to launch the HDI cluster.  Wait for 5-10 min for HDI cluster to be available.

![1](img/utkuma_2020-05-26-16-20-42.png)

While waiting you will see the following screen. The creation on HDICLuster takes max ~10 Min, be patient and check the status of the cluster at HDI dashboard.

![1](img/utkuma_2020-05-26-16-21-09.png)

Navigate to HDI dashboard, by using three-bar menu option.

![1](img/utkuma_2020-09-10-14-28-53.png)

Select "Analytics" and "HDInsight cluster" to navigate to HDI dashboard.

![1](img/utkuma_2020-09-10-14-30-34.png)


Filter your HDI Cluster by typing name in filter area. If you see the cluster status as "Running", then and then ony execute next step to collect Hadoop Jar and Configuration files. Otherwise, wait for the Cluster status to turn "Running".

![1](img/utkuma_2020-09-10-14-34-42.png)



## Collect Hadoop Jar and Config files

**Wait for HDICluster deployment process to complete before executing steps to collect Hadoop Jars and Config files.The following script will take around ~25 Min.**

Login to cldlgn.fyi.sas.com Unix server and navigate to ..../Scripts folder, from where you started Azure environment. Execute the "To_Plugin_ADLS2_HDIcluster.sh" script to collect hadoop Jar and config files from HDI Master node. The Jar and config files are collected at Viya01 server. The same script also distribute the Jar and config files to Viya environment. If you see any error displayed for HBASE, PIGCHAT, HBASECATALOG, please ignore it, the script ran alright.

Code:

```bash

cd ~/PSGEL265-sas-viya-3.5....../Scripts

tmux

./To_Plugin_ADLS2_HDIcluster.sh <NT ID> <Resource Group Name> <HDI Cluster Name>

Example:
./To_Plugin_ADLS2_HDIcluster.sh utkuma utkuma8 hdiutkuma8

```

**Note: While script is running you may see scrolling screen with error messages about "hbase, hbasecat client, etc. . It's normal for 'SAS hadooptracer'; ignore these error messages.**

Results:

```log
cldlgn02.unx.sas.com> ./To_Plugin_ADLS2_HDIcluster.sh utkuma utkuma8 hdiutkuma8

Checking for Azure Resource Group
Azure Resource Group Exists : utkuma8 : true
 Resource Group  : utkuma8
 Storage Account : utkuma8adls2strg
HDInsight SSH Server name : hdiutkuma8-ssh.azurehdinsight.net
 Collecting Hadoop Jar and Config files from  HDI Hadoop cluster
 Getting Viya01 VM Public IP
Public Ip of Viya01 VM : 52.179.132.227
plugin_HDInsight_to_viya.sh                                                           100% 1756   129.2KB/s   00:00
My Private Key File : ~/.ssh/private_key.pem
Warning: Permanently added 'hdiutkuma8-ssh.azurehdinsight.net,104.209.245.136' (ECDSA) to the list of known hosts.
Authorized uses only. All activity may be monitored and reported.
Authorized uses only. All activity may be monitored and reported.
sudo: yum: command not found
2020-05-14 15:03:27,819 hadooptracer [INFO]
2020-05-14 15:03:27,819 hadooptracer [INFO] HadoopTracer started
2020-05-14 15:03:27,819 hadooptracer [INFO] Current version of the hadooptracer script: 19w47.01
2020-05-14 15:03:29,155 hadooptracer [INFO] Running Scenario **********************************************************************************************************************************
cldlgn02.unx.sas.com>
```
<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)**<-- you are here**
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
