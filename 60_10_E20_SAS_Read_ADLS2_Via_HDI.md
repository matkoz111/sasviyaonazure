![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# SAS Access to ADLS2 Blob Storage datafile via HDI Hive Table
- [Login to Viya application](#login-to-viya-application)
- [Create hive tables with storage location to ADLS2](#create-hive-tables-with-storage-location-to-adls2)
- [Access the hive table having data files located at ADLS2](#access-the-hive-table-having-data-files-located-at-adls2)

Purpose of this hands on is to demonstrate SAS 9.4 access to data files located ADLS2. The data files could be csv or Parquet. These files can be read into SAS via HDInsight Hive. We are going to use the data file uploaded in “upload sample data” section.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Create hive tables with storage location to ADLS2

On SAS Studio execute the following code to create a hive table with external data file.

**Before executing the code set the HDINM= , and  MYSTRGACC= parameter with your HDI name and Storage account name. The MYSTRGFS=fsdata is valid value and keep it unchanged**

Code:

```sas

/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=fsdata;

/*
Example:
%let HDINM=hdiutkuma8;
%let MYSTRGACC=utkuma8adls2strg;
%let MYSTRGFS=fsdata;
*/

option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
options sastrace = ',,,sd' sastraceloc = saslog ;

PROC SQL ;
connect to hadoop as hvlib (uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
server="&HDINM..azurehdinsight.net" user="admin" pw="SASV3i5ya@2020" schema="default");
execute (drop table flight_reporting ) by hvlib;
execute (CREATE EXTERNAL TABLE flight_reporting (
    YEAR varchar(4) ,
    MONTH varchar(4),
    ORIGIN_AIRPORT_ID varchar(10),
    ORIGIN_AIRPORT_SEQ_ID varchar(10),
    ORIGIN_CITY_MARKET_ID varchar(10),
    ORIGIN_CITY_NAME varchar(20),
    DEST_AIRPORT_ID varchar(10),
    DEST_AIRPORT_SEQ_ID varchar(10),
    DEST_CITY_MARKET_ID varchar(10),
    DEST_CITY_NAME varchar(20)
    )
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION "abfs://&MYSTRGFS@&MYSTRGACC..dfs.core.windows.net/flight_data"
) by hvlib;
execute (drop table baseball_adls2) by hvlib;
execute (CREATE EXTERNAL TABLE baseball_adls2(
  name varchar(18),
  team varchar(14),
  natbat double,
  nhits double,
  nhome double,
  nruns double,
  nrbi double,
  nbb double,
  yrmajor double,
  cratbat double,
  crhits double,
  crhome double,
  crruns double,
  crrbi double,
  crbb double,
  league varchar(8),
  division varchar(8),
  position varchar(8),
  nouts double,
  nassts double,
  nerror double,
  salary double,
  div_1 varchar(16),
  logsalary double
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\u0001'
  LINES TERMINATED BY '\n'
  STORED AS parquet
LOCATION "abfs://&MYSTRGFS@&MYSTRGACC..dfs.core.windows.net/baseball"
) by hvlib;
quit;
```

Results:

```log
...
........
connect to hadoop as hvlib (uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;
87 ! ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
88   server="&HDINM..azurehdinsight.net" user="admin" pw=XXXXXXXXXXXXXXXX schema="default");
89   execute (drop table flight_reporting ) by hvlib;
  32 1589472298 no_name 0 SQL
HADOOP_2: Executed: on connection 2 33 1589472298 no_name 0 SQL
drop table flight_reporting 34 1589472298 no_name 0 SQL
  35 1589472298 no_name 0 SQL
  36 1589472299 no_name 0 SQL
Summary Statistics for HADOOP are: 37 1589472299 no_name 0 SQL
Total SQL execution seconds were:                   0.510127 38 1589472299 no_name 0 SQL
Total seconds used by the HADOOP ACCESS engine were     0.510883 39 1589472299 no_name 0 SQL
  40 1589472299 no_name 0 SQL
90   execute (CREATE EXTERNAL TABLE flight_reporting (
91       YEAR varchar(4) ,
92       MONTH varchar(4),
93       ORIGIN_AIRPORT_ID varchar(10),
94       ORIGIN_AIRPORT_SEQ_ID varchar(10),
95       ORIGIN_CITY_MARKET_ID varchar(10),
96       ORIGIN_CITY_NAME varchar(20),
97       DEST_AIRPORT_ID varchar(10),
98       DEST_AIRPORT_SEQ_ID varchar(10),
99       DEST_CITY_MARKET_ID varchar(10),
100      DEST_CITY_NAME varchar(20)
101      )
102  ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
103  LINES TERMINATED BY '\n'
104  STORED AS TEXTFILE
105  LOCATION "abfs://fsdata@utkuma8adls2strg.dfs.core.windows.net/flight_data"
106  ) by hvlib;
41 1589472299 no_name 0 SQL

..........
...................

```

## Access the hive table having data files located at ADLS2

On SAS Studio execute the following code to access the hive table with external data file.

**Before executing the code set the HDINM= parameter with your HDI Cluster name.**

Code:

```sas

/* Set parameters */
%let HDINM=<Your HDInsight Cluster Name> ;

/*
Example:
%let HDINM=hdiutkuma8;
*/

option set = SAS_HADOOP_CONFIG_PATH = "/opt/sas/viya/config/data/HDIHadoopData/conf";
option set = SAS_HADOOP_JAR_PATH = "/opt/sas/viya/config/data/HDIHadoopData/lib";
options sastrace = ',,,sd' sastraceloc = saslog ;

libname hdilib HADOOP
uri="jdbc:hive2://&HDINM..azurehdinsight.net:443/default;ssl=true?hive.server2.transport.mode=http;hive.server2.thrift.http.path=hive2"
server="&HDINM..azurehdinsight.net"
user="admin" pw="SASV3i5ya@2020" schema="default";


proc sql outobs=20;
select * from hdilib.flight_reporting ;
select * from hdilib.baseball_adls2 ;
quit;

```

Results:

```log
......
............

82
83   proc sql outobs=20;
84   select * from hdilib.flight_reporting ;
  74 1589472533 no_name 0 SQL
HADOOP_1: Prepared: on connection 0 75 1589472533 no_name 0 SQL
DESCRIBE FORMATTED `default`.`FLIGHT_REPORTING` 76 1589472533 no_name 0 SQL
  77 1589472533 no_name 0 SQL
  78 1589472533 no_name 0 SQL
HADOOP_2: Executed: on connection 0 79 1589472533 no_name 0 SQL
SELECT * FROM `default`.`FLIGHT_REPORTING` 80 1589472533 no_name 0 SQL
  81 1589472533 no_name 0 SQL
WARNING: Statement terminated early due to OUTOBS=20 option.
  82 1589472535 no_name 0 SQL
Summary Statistics for HADOOP are: 83 1589472535 no_name 0 SQL
Total SQL execution seconds were:                   1.512895 84 1589472535 no_name 0 SQL
Total SQL prepare seconds were:                     0.672871 85 1589472535 no_name 0 SQL
Total SQL describe seconds were:                    0.000006 86 1589472535 no_name 0 SQL
Total seconds used by the HADOOP ACCESS engine were     2.658667 87 1589472535 no_name 0 SQL
  88 1589472535 no_name 0 SQL
85   select * from hdilib.baseball_adls2 ;
  89 1589472535 no_name 0 SQL
HADOOP_3: Prepared: on connection 0 90 1589472535 no_name 0 SQL
DESCRIBE FORMATTED `default`.`BASEBALL_ADLS2` 91 1589472535 no_name 0 SQL
  92 1589472535 no_name 0 SQL
  93 1589472536 no_name 0 SQL
HADOOP_4: Executed: on connection 0 94 1589472536 no_name 0 SQL
SELECT * FROM `default`.`BASEBALL_ADLS2` 95 1589472536 no_name 0 SQL
  96 1589472536 no_name 0 SQL
WARNING: Statement terminated early due to OUTOBS=20 option.
  97 1589472539 no_name 0

.....
..............

```

Results from PROC SQL statement:

Note: In this case, the data read from ADLS2 Blob Storage location to SAS via HDInsight Hive.The Flight data file is in CSV format and baseball data file is in Parquet format.

![1](img/utkuma_2020-05-14-12-13-59.png)

![1](img/utkuma_2020-05-14-12-14-41.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)**<-- you are here**
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
