![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Upload Sample Data to 'fsdata' Blob Container (ADLS2)
- [Review the ADLS2 'fsdata' blob storage](#review-the-adls2-fsdata-blob-storage)
- [Upload data files to 'fsdata' filesystem/container](#upload-data-files-to-fsdata-filesystemcontainer)
  - [Upload CSV Files](#upload-csv-files)
  - [Upload Parquet Files](#upload-parquet-files)
- [Validate files at Azure Storage Account](#validate-files-at-azure-storage-account)

Purpose of this hands-on is to upload sample data to Blob Storage Container (ADLS2) 'fsdata' which will later be accessed from HDInsight cluster using hive. These data files will not be on HDFS of HDInsight cluster rather located on ADLS2 storage. The ADLS2 'fsdata' blob container created as part of environment startup process.

In this example, script upload CSV and Parquet files to ADLS2 and. The HDI cluster can access various data file (csv,avro,json...) stored at ADLS2 provided supported by native hive.

## Review the ADLS2 'fsdata' blob storage

Login to Azure Dashboard and navigate to Azure Menu.

[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

Navigate to list of Azure Resource Group from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group and view the list of objects. Click on Resource Group hyper link to move on to next window.

![1](img/utkuma_2020-05-26-16-11-10.png)

Navigate to the ADLS2 Storage Account from list of the objects.

![1](img/utkuma_2020-05-26-16-56-31.png)

Navigate to the ADLS2 Storage Account container and view the 'fsdata' container.
![1](img/utkuma_2020-06-16-15-45-39.png)

We are going to upload data files under same container.

## Upload data files to 'fsdata' filesystem/container

Login to cldlgn.fyi.sas.com Unix server and cd to ..../Scripts folder; the Azure environment scripts folder.

Use Storage account name and filesystem name from the above screen as parameter for the script.

### Upload CSV Files

Execute following statement to upload CSV sample data to 'fsdata' ADLS2 location.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

./Upload_CSV_Data_to_ADLS2.sh < Storage Account Name> <Filesystem Name >

example:
./Upload_CSV_Data_to_ADLS2.sh utkuma8adls2strg fsdata

```

Results:

```log
cldlgn03.unx.sas.com> ./Upload_CSV_Data_to_ADLS2.sh utkuma8adls2strg fsdata
Finished[#############################################################]  100.0000%
{
  "etag": "\"0x8D7F77E62ACB070\"",
  "lastModified": "2020-05-13T20:44:08+00:00"
}
Name                              Blob Type    Blob Tier    Length    Content Type    Last Modified              Snapshot
--------------------------------  -----------  -----------  --------  --------------  -------------------------  ----------
flight_data                       BlockBlob    Hot                                    2020-05-13T20:44:08+00:00
flight_data/flight_reporting.csv  BlockBlob    Hot          31645     text/csv        2020-05-13T20:44:08+00:00
cldlgn03.unx.sas.com>

```

### Upload Parquet Files

Execute following statement to upload Parquet sample data to 'fsdata' ADLS2 location.

Code:

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

./Upload_Parquet_LargeData_to_ADLS2.sh <Storage Account Name > <File system Name>  < No of copies>

Example:
./Upload_Parquet_LargeData_to_ADLS2.sh utkuma8adls2strg fsdata 5

```

Results:

```log
cldlgn03.unx.sas.com> ./Upload_Parquet_LargeData_to_ADLS2.sh utkuma8adls2strg fsdata 5

Number of Files to copy :5
Finished[#############################################################]  100.0000%
{
  "etag": "\"0x8D7F77EE91E7D70\"",
  "lastModified": "2020-05-13T20:47:54+00:00"
}
Finished[#############################################################]  100.0000%
.........
...........

Name                              Blob Type    Blob Tier    Length    Content Type              Last Modified              Snapshot
--------------------------------  -----------  -----------  --------  ------------------------  -------------------------  ----------
baseball                          BlockBlob    Hot                                              2020-05-13T20:47:54+00:00
baseball/baseball_prqt_1          BlockBlob    Hot          38910     application/octet-stream  2020-05-13T20:47:54+00:00
baseball/baseball_prqt_2          BlockBlob    Hot          38910     application/octet-stream  2020-05-13T20:47:56+00:00
baseball/baseball_prqt_3          BlockBlob    Hot          38910     application/octet-stream  2020-05-13T20:47:57+00:00
baseball/baseball_prqt_4          BlockBlob    Hot          38910     application/octet-stream  2020-05-13T20:47:59+00:00
baseball/baseball_prqt_5          BlockBlob    Hot          38910     application/octet-stream  2020-05-13T20:48:00+00:00
flight_data                       BlockBlob    Hot                                              2020-05-13T20:44:08+00:00
flight_data/flight_reporting.csv  BlockBlob    Hot          31645     text/csv                  2020-05-13T20:44:08+00:00
cldlgn03.unx.sas.com>
```

## Validate files at Azure Storage Account

Switch to Azure Storage Account Dashboard UI.

On Storage Explorer Preview navigate to 'fsdata' Blob Container / filesystem and view the uploaded folders and files.

If folders and files are not visible, refresh the container by using …More  menu option.

CSV File:
![1](img/utkuma_2020-05-13-17-04-20.png)

Parquet Files:

![1](img/utkuma_2020-05-13-17-08-05.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)**<-- you are here**
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
