## Install Open LDAP at Viya server 
echo " Installing Open LDAP at Viya01 server " 
cd ~
ansible-playbook /opt/sas/playbook/OpenLDAP/gel.openldapremove.yml -i ~/inventory.ini
ansible-playbook /opt/sas/playbook/OpenLDAP/gel.openldapsetup.yml -i ~/inventory.ini -e "OLCROOTPW=lnxsas" -e "DC=iaas"


## Copy sitedefault.yml to playbook folder 
echo " Copy sitedefault.yml to playbook folder  " 
cp /opt/sas/playbook/OpenLDAP/sitedefault.yml /opt/sas/playbook/sas_viya_playbook/roles/consul/files/
