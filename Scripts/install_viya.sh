## Start Viya Repository 
cd /opt/sas/mirror/yummirror
nohup python -m ComplexHTTPServer 8123 > /dev/null 2>/dev/null  &

## Perform Viya Software Deployment  
echo " Performing Viya Software Deployment " 

cd /opt/sas/playbook/sas_viya_playbook
ansible all -m ping

time ansible-playbook site.yml

ansible sas_all -m service -a "name=sas-viya-all-services.service enabled=no" -b > ./service.disable.log ; 

ansible sas_all -b -m shell -a "/etc/init.d/sas-viya-all-services status" ;
