#############################################################################
### This is the master script will calls subsequest script to 
### Re-start an AZ VM for SMP or MMP Viya enviroment
##############################################################################

if [ $# -lt 3 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource group name] [ Environment mode ]  "
    echo
    echo "Script requires Three parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group name to create VM and related objects  e.g. utkuma5 / WinNTIDn "
    echo "     3. Environemnt Starting mode  e.g. smp / mpp "
    echo
    #echo "This script allows you to Start Azure Environment with VM machine, and Storage Accound "
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi


user_nt_id=$1
user_rs_group=$2
env_mode=$3

#user_nt_id=utkuma
#user_rs_group=utkuma3

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 
my_location=eastus2


function error_exit {
    echo "${1}"
    exit 1
}

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

# Test for Valid Environment mode SMP or MMP 
echo " Environment mode :"$env_mode
if [[ $env_mode != 'smp'  ]] && [[ $env_mode != 'mpp'  ]]; then
    error_exit "Not a valid Environment mode:$env_mode  ; use 'smp' or 'mmp' as value .  " 
fi

## Set Azure default Subscripton to GELDM
rs_sub_id=`az account set -s GELDM`


## checking for valid  Azure Resource Group
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

rs_group=`az group show -n $user_rs_group --query name -o tsv` 
echo " Resource group : "  $rs_group 
echo " Private key    : "  $my_private_key
echo " Public  key    : "  $my_public_key

cldlgn_host_name=`uname -a `

echo " Informations about the Environment  "  $rs_group > resource_info
echo " =================================== "  $rs_group >> resource_info
echo " Resource group : "  $rs_group >> resource_info
echo " Private key    : "  $my_private_key >> resource_info
echo " Public  key    : "  $my_public_key >> resource_info
echo " Cloud Login Host Name   : "  $cldlgn_host_name  >> resource_info


if [[ $env_mode == 'smp'  ]] ; then
  echo "Restart SMP Azure VM Servers/Environment.  "
  ./To_ReStart_AZ_ViyaServer_SMP.sh $user_nt_id $rs_group
else 
  echo "Restart MPP Azure VM Servers/Environment.  "
  ./To_ReStart_AZ_ViyaServer_MPP.sh $user_nt_id $rs_group
fi
###### Call Scripts to start related objects
./To_Create_SQLServer_Database.sh $user_nt_id $rs_group $env_mode
