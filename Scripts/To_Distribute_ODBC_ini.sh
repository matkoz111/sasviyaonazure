#################################################################################
### This script udpate customised OBDC ini file and distribute to acros the viya environment. 
#################################################################################

if [ $# -lt 3 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User NT ID ] [Resource group name] [Environment Mode] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo "     3. SQL Pool Name  e.g. : utkuma5pool / <resourcegroup+pool>  "
    echo "     4. Environment Mode e.g. : smp / mpp  "
    echo
    #echo "This script allows you to create a MS SQL Server in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2
sql_pool_db_name=$3 
env_mode=$4 

my_user=viyadep
my_location=eastus2
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
vm_name=$user_rs_group-viya35v01

function error_exit {
    echo "${1}"
    exit 1
}

## Verify private keys file for the WiN NT ID.
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Invalid Win NT ID  "
fi


## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

# Test for Valid Environment mode SMP or MMP
echo " Environment mode :"$env_mode
if [[ $env_mode != 'smp'  ]] && [[ $env_mode != 'mpp'  ]]; then
    error_exit "Not a valid Environment mode:$env_mode  ; use 'smp' or 'mmp' as value .  "
fi


srv=sqlsrv
sql_server_name=$rs_group$srv
sql_server_admin_user=$my_user 
sql_server_admin_pwd=lnxsas@2020
echo " Resource Group  : $rs_group " 
echo " SQL Pool Server Name  : $sql_server_name" 
echo " SQL Admin User Name  : $sql_server_admin_user" 
echo " SQL Admin User Pwd   : $sql_server_admin_pwd" 


echo " ======================  "    >> resource_info
echo " SQL Poll Server Environment "    >> resource_info
echo " SQL Pool Server Name  : $sql_server_name"  >> resource_info
echo " SQL Pool Database Name  : $sql_pool_db_name"  >> resource_info
echo " SQL Admin User Name  : $sql_server_admin_user"  >> resource_info
echo " SQL Admin User Pwd   : $sql_server_admin_pwd"  >> resource_info
echo " ======================  "    >> resource_info

echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Replace server name in ODBC.ini file. 
sed -i "/#sqls_gelws start/,/#sqls_gelws end/s/HostName=.*/HostName=$sql_server_name.database.windows.net/" spre_odbc.ini
sed -i "/#sqls_gelws start/,/#sqls_gelws end/s/Database=.*/Database=$sql_pool_db_name/" spre_odbc.ini

sed -i "/#sqls_gelws start/,/#sqls_gelws end/s/HostName=.*/HostName=$sql_server_name.database.windows.net/" viya_odbc.ini
sed -i "/#sqls_gelws start/,/#sqls_gelws end/s/Database=.*/Database=$sql_pool_db_name/" viya_odbc.ini


### copy odbc.ini to Viya and SPRE folder and execute MS-SQL plugin script  at Viya01 server .
## Execute plugin_MsSQL_to_viya.sh from  Viya01 VM server
echo " Copying updated ODBC.ini file to Viya server  "
scp -o "StrictHostKeyChecking=no" -i $my_private_key spre_odbc.ini $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key viya_odbc.ini $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key plugin_MsSQL_to_viya.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/plugin_MsSQL_to_viya.sh $env_mode
