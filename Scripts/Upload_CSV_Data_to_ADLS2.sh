# Upload Sample CSV data files to ADLS2 Storage
if [[ $# -lt 2 ]] ; then
    echo "Script requires two parameter"
    echo "     1. Storage Account  Name "
    echo "     2. Container/Filesystem Name "
    exit 1
fi


storage_account_name=$1 
fs_name=$2 

### Load data to fsdata blob container 
az storage blob upload \
--name "flight_data/flight_reporting.csv" \
--file "sample_data/flight_reporting.csv" \
--account-name $storage_account_name \
--container-name $fs_name \
--auth-mode login


### List the data files from fsdata Blob container 
az storage blob list \
--account-name $storage_account_name \
--container-name $fs_name  \
--output table \
--auth-mode login
