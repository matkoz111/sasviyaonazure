#################################################################################
### This script Creats Azure Synapse SQL Pool with existing MS SQL Server at AZ Resource Group 
#################################################################################

if [ $# -lt 3 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User NT ID ] [Resource group name] [Environment Mode] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo "     3. Environment Mode e.g. : smp / mpp  "
    echo
    #echo "This script allows you to create a MS SQL Server in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2
env_mode=$3 

my_user=viyadep
my_location=eastus2
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
vm_name=viya35v01

function error_exit {
    echo "${1}"
    exit 1
}

## Verify private keys file for the WiN NT ID.
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Invalid Win NT ID  "
fi

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

# Test for Valid Environment mode SMP or MMP
echo " Environment mode :"$env_mode
if [[ $env_mode != 'smp'  ]] && [[ $env_mode != 'mpp'  ]]; then
    error_exit "Not a valid Environment mode:$env_mode  ; use 'smp' or 'mmp' as value .  "
fi


srv=sqlsrv
pool=pool
sql_server_name=$rs_group$srv
sql_pool_name=$rs_group$pool
sql_server_admin_user=$my_user 
sql_server_admin_pwd=lnxsas@2020
echo " Resource Group  : $rs_group " 
echo " SQL Pool Server Name  : $sql_server_name" 
echo " SQL Pool Name  : $sql_pool_name" 
echo " SQL Admin User Name  : $sql_server_admin_user" 
echo " SQL Admin User Pwd   : $sql_server_admin_pwd" 

## Check if Azure MS-SQL Server  exists
echo "Checking for Azure MS-SQL Server "
sql_serv_sts=`az sql server show -n $sql_server_name  -g $rs_group --query name -o tsv`
echo "sql_serv_sts=  "$sql_serv_sts
if [ "$sql_serv_sts" != $sql_server_name ]; then
      error_exit "Could not fetch Sql Server or Does not exists   "
fi
#echo "sql_serv_sts=  "$sql_serv_sts



###  Create a SQL Pool ####
echo "Creating a SQL Pool with existing MS-SQL Server  " 
sql_pool_sts=`az sql dw create \
--name $sql_pool_name \
--server $sql_server_name \
--resource-group $rs_group \
--service-objective DW100c \
--tags name=$user_nt_id \
-o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Azure SQL Pool  : $sql_pool_name "
else
    echo "Created Azure SQL Pool   :$sql_pool_name"
fi


echo " ======================  "    >> resource_info
echo " SQL Pool  Environment "    >> resource_info
echo " SQL Pool Server Name  : $sql_server_name"  >> resource_info
echo " SQL Pool Database Name  : $sql_pool_name"  >> resource_info
echo " SQL Admin User Name  : $sql_server_admin_user"  >> resource_info
echo " SQL Admin User Pwd   : $sql_server_admin_pwd"  >> resource_info
echo " ======================  "    >> resource_info


### copy odbc.ini to Viya and SPRE folder and execute MS-SQL plugin script  at Viya01 server .
./To_Distribute_ODBC_ini.sh $user_nt_id $user_rs_group $sql_pool_name $env_mode 
