#################################################################################
### This script Creats HDInsight Hadoop Cluster 
### using  Storage Account and containers in a AZ Resource Group 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [New resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / Win_NT_ID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma_RG / WIn_NT_Id_RG "
    echo
   # echo "This script allows you to create a HDInsight CLuster in AZ resource group. "
   # echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
   # echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
my_location=eastus2
vm_name=viya35v01



function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


stor=hdistrg
storage_acc_name=$rs_group$stor
echo " Resource Group  : $rs_group "
echo " Storage Account : $storage_acc_name "

###  Create an Storgae Account ####
echo "Creating Storage Account "
st_acc_id=`az storage account create --name $storage_acc_name \
--resource-group $rs_group \
--access-tier Hot \
--sku Standard_LRS \
--https-only true \
--kind StorageV2 \
--location $my_location \
--tags name=$user_nt_id \
--query id -o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create  Azure Storage Account: $storage_acc_name "
else
    echo "Created Azure Storage Account :$storage_acc_name  "
fi


###  Collect Storage Key  to create container ####
echo "Collecting Storage Key from Storage Account " 
azure_storage_key=`az storage account keys list \
--account-name $storage_acc_name \
--resource-group $rs_group \
--query [0].value -o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not collect Storage Keys from  Azure Storage Account: $storage_acc_name "
else
    echo "Collected  Storage Key from Azure Storage Account : $storage_acc_name : $azure_storage_key "
fi

## save storage account key in a file to be used in core-site.xml 
echo $azure_storage_key  > hdi_strg_acc_key


### Create fsdata container for data files and external DB for Hive
hd_container_name=fsdata
echo "Creating Container  at Azure Storage account " 
st_acc_cont_id=`az storage container create \
--name $hd_container_name \
--account-name $storage_acc_name \
--account-key $azure_storage_key \
--fail-on-exist `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Container at Azure Storage Account: $hd_container_name "
else
    echo "Created Container at Azure Storage Account : $hd_container_name "
fi

### Create container for Hadoop environment under  Storage Account 
hd_container_name=fs$storage_acc_name
echo "Creating Container  at Azure Storage account " 
st_acc_cont_id=`az storage container create \
--name $hd_container_name \
--account-name $storage_acc_name \
--account-key $azure_storage_key \
--fail-on-exist `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Container at Azure Storage Account: $hd_container_name "
else
    echo "Created Container at Azure Storage Account : $hd_container_name "
fi



### Create HDInsight cluster/Hadoop environment 
echo "Creating HDInsight Cluster using  Azure Storage account " 
hdcluster_name=hdi$rs_group
http_user=$my_user
http_pwd="SASV3i5ya35@2020"
ssh_user=$my_user
cluster_type=hadoop
cluster_version="3.6"
component_version="Hadoop=2.7"
clustersize_nodes=1
ssh_pub_key=`cat $my_public_key `
#echo " Public Key : $ssh_pub_key " 

hdinsight_id=`az hdinsight create \
--name $hdcluster_name \
--resource-group $rs_group \
--location $my_location \
--http-user $http_user \
--http-password $http_pwd \
--ssh-user $ssh_user \
--ssh-public-key "$ssh_pub_key" \
--type $cluster_type \
--version $cluster_version \
--component-version $component_version \
--workernode-count $clustersize_nodes \
--storage-account $storage_acc_name \
--storage-account-key $azure_storage_key \
--storage-container $hd_container_name  \
--tags name=$user_nt_id  ` 

##--ssh-public-key $my_public_key \
##--ssh-password $http_pwd \

if [ "$?" -ne "0" ]; then
    error_exit "Could not Create HDInsight Cluster using Azure Storage Account Container : $hdcluster_name"
else
    echo "Created  HDInsight Cluster using Azure Storage Account Container : $hdcluster_name "
fi

### Get HDI SSH Server name 
hdi_ssh_server_name=`az hdinsight show  -g $rs_group  -n $hdcluster_name --query properties.connectivityEndpoints[0].location -o tsv `
if [ "$?" -ne "0" ]; then
    error_exit "Could not get HDInsight SSH Server Name : $hdcluster_name"
else
    echo "HDInsight SSH Server name : $hdcluster_name "
fi

echo " "  >> resource_info
echo "============= " >> resource_info 
echo " HD Insight Cluster Name : "  $hdcluster_name >> resource_info
echo " Http User       : "  $http_user  >> resource_info
echo " Http user Pwd   : "  $http_pwd   >> resource_info
echo " HDI SSH Server Name : "  $hdi_ssh_server_name   >> resource_info
echo " SSH User        : "  $ssh_user   >> resource_info
echo " SSH Pub Key     : "  $my_public_key   >> resource_info
echo " SSH Private Key : "  $my_private_key   >> resource_info
echo "============= " >> resource_info 
echo "my_private_key=$my_private_key " >> resource_info
echo "my_user=$ssh_user" >> resource_info
echo "hdi_ssh_server_name=$hdi_ssh_server_name "  >> resource_info
echo "  "  >> resource_info
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$hdi_ssh_server_name" >> resource_info
echo "  " >> resource_info
echo "============= " >> resource_info 




### collect HDI Hadoop jar and config files at Viya01 server . 
echo " Collecting Hadoop Jar and Config files from  HDI Hadoop cluster " 
echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Execute plugin_HDInsight_to_viya.sh from  Viya01 VM server 
scp -o "StrictHostKeyChecking=no" -i $my_private_key plugin_HDInsight_to_viya.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/plugin_HDInsight_to_viya.sh $hdi_ssh_server_name

## copy core-site.xml 
scp -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip:~/HDIHadoopData/conf/core-site.xml core-site.xml

