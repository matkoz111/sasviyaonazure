hdi_server=$1
#hdi_server=hdiutkuma5-ssh.azurehdinsight.net

# clean the known host entry at Viya server for ssh access
sed -i '/hdi/d' ~/.ssh/known_hosts

# copy hadooptracer_py file from Viya01 server to HDInsight Master Node and execute it to collect Hadoop Jar files and Config files.
my_user=viyadep
my_pvt_key="~/.ssh/private_key.pem"
echo "My Private Key File : $my_pvt_key " 

scp -o "StrictHostKeyChecking=no" -i $my_pvt_key /opt/sas/spre/home/SASFoundation/lib/hadooptracer/* $my_user@$hdi_server:/tmp/
ssh -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server "sudo chmod 777 /tmp/hadooptracer_py; sudo yum install strace -y ; sudo -su hive python /tmp/hadooptracer_py --filterby=latest --postprocess ; "



# copy Jars and Config files from HDInsight  Master Node to Viya01 server.
mkdir ~/HDIHadoopData
rm -r ~/HDIHadoopData/lib
rm -r ~/HDIHadoopData/conf
scp -r -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server:/tmp/jars ~/HDIHadoopData/lib
scp -r -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server:/tmp/sitexmls ~/HDIHadoopData/conf

# Distribute Hadoop Jars and COnfig files across Viya and CAS servers. 
cd ~
ansible all -m copy -a "src=~/HDIHadoopData/ dest=/opt/sas/viya/config/data/HDIHadoopData owner=sas group=sas " -b

# make a copy of core-site.xml for key property and udpate
cp ~/HDIHadoopData/conf/core-site.xml ~/HDIHadoopData/conf/core-site_org.xml

# copy hdinsight-common folder from HDInsight to Viya server for key decription. 
mkdir ~/HDILib
rm -r ~/HDILib/hdinsight-common
scp -r -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server:/usr/lib/hdinsight-common ~/HDILib/

cd ~
ansible all -m copy -a "src=~/HDILib/ dest=/usr/lib owner=root group=root mode=0755 " -b
