#################################################################################
### This script collect Jar and Config files from  HDInsight Hadoop Cluster 
#################################################################################

if [ $# -lt 3 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource group name] [HDI Cluster name ] "
    echo
    echo "Script requires three parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo "     3. Azure AHDI CLuster Name  e.g. hdiutkuma5 / hdi<WinNTIDn> "
    echo
   # echo "This script allows you to create a HDInsight CLuster in AZ resource group. "
   # echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
   # echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2
#hdcluster_name=hdi$rs_group
hdcluster_name=$3

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
my_location=eastus2
vm_name=$user_rs_group-viya35v01


function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


stor=adls2strg
storage_acc_name=$rs_group$stor
ssh_pub_key=`cat $my_public_key `
echo " Resource Group  : $rs_group "
echo " Storage Account : $storage_acc_name "


### Get HDI SSH Server name 
hdi_ssh_server_name=`az hdinsight show  -g $rs_group  -n $hdcluster_name --query properties.connectivityEndpoints[0].location -o tsv `
if [ "$?" -ne "0" ]; then
    error_exit "Could not get HDInsight SSH Server Name : $hdcluster_name"
else
    echo "HDInsight SSH Server name : $hdi_ssh_server_name"
fi

echo " "  >> resource_info
echo "============= " >> resource_info 
echo " HD Insight Cluster Name : "  $hdcluster_name >> resource_info
echo " HDI SSH Server Name : "  $hdi_ssh_server_name   >> resource_info
echo " SSH User        : "  $my_user   >> resource_info
echo " SSH Private Key : "  $my_private_key   >> resource_info
echo "============= " >> resource_info 
echo "my_private_key=$my_private_key " >> resource_info
echo "my_user=$my_user" >> resource_info
echo "hdi_ssh_server_name=$hdi_ssh_server_name "  >> resource_info
echo "  "  >> resource_info
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$hdi_ssh_server_name" >> resource_info
echo "  " >> resource_info
echo "============= " >> resource_info 


### At VIya01 server collect HDI Hadoop jar and config files from HDI Master node . 
echo " Collecting Hadoop Jar and Config files from  HDI Hadoop cluster " 
echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Execute plugin_HDInsight_to_viya.sh from  Viya01 VM server 
scp -o "StrictHostKeyChecking=no" -i $my_private_key plugin_HDInsight_to_viya.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/plugin_HDInsight_to_viya.sh $hdi_ssh_server_name

## copy core-site.xml 
scp -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip:~/HDIHadoopData/conf/core-site.xml core-site.xml

