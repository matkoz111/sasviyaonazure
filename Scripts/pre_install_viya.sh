## Perform Viya Preinstall 
echo " Perform Viya Pre-install " 

cd /opt/sas/playbook/viya-ark/playbooks/pre-install-playbook/

## Perform Viya Preinstall with --check and --skip-tags options  
ansible-playbook viya_pre_install_playbook.yml -i ~/inventory.ini --check --skip-tags skipstoragefail \
    -e 'min_mem_mb=1000' \
    -e 'min_cores_num=1' \
    -e 'yum_cache_yn=0' \
    -e 'use_pause=no' \
    -e '{"custom_group_list": { "group": "sas" , "gid":"10001" } }' \
    -e '{"custom_user_list": [ { "name": "cas" , "uid":"10002", "group":"sas" , "groups":"sas" } , { "name": "sas" , "uid":"10001", "group":"sas" , "groups":"sas" } ] }'


## Perform pre-installation tasks without --check option to run the task :
ansible-playbook viya_pre_install_playbook.yml -i ~/inventory.ini  --skip-tags skipstoragefail \
    -e 'min_mem_mb=1000' \
    -e 'min_cores_num=1' \
    -e 'yum_cache_yn=0' \
    -e 'use_pause=no' \
    -e '{"custom_group_list": { "group": "sas" , "gid":"10001" } }' \
    -e '{"custom_user_list": [ { "name": "cas" , "uid":"10002", "group":"sas" , "groups":"sas" } , { "name": "sas" , "uid":"10001", "group":"sas" , "groups":"sas" } ] }'



## Create CAS_DISK_CACHE: /opt/sas/cas/cache01  folder
cd ~
#ansible all -m file -a "path=/opt/sas/cas/cache01 state=directory owner=cas group=sas mode=0774 " -b
#ansible all -m file -a "path=/mnt/cascache/cache01 state=directory owner=cas group=sas mode=0774 " -b
./initialise_ephemeral_folder.sh

