###################################################################
### This script lists Status for Viya Servers services 
####################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group name to create VM and related objects  e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to Create an Azure VM from a disk snapshot and install Viya"
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi


user_nt_id=$1
user_rs_group=$2

#user_nt_id=utkuma
#user_rs_group=utkuma3

vm_name=$user_rs_group-viya35v01
my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 
vm_location=eastus2

snapshot_location=eastus2
snpshot_subscription=GELDM
snpshot_rs_group=utkuma
dt_snapshot_name=viya35m_01

function error_exit {
    echo "${1}"
    exit 1
}

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi


## checking for valid  Azure Resource Group 
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


echo " Resource group : "  $rs_group 
echo " Private key    : "  $my_private_key
echo " Public  key    : "  $my_public_key

echo "Getting VM ID"
vm_id=`az vm show -g $rs_group -n $vm_name --query id -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not find VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM ID: $vm_id"

echo "Getting VM Public IP"
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`
echo "VM Public IP : $vm_public_ip"


## lists status from AZ Viya Server Services 
scp -o "StrictHostKeyChecking=no" -i $my_private_key viya_services.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/viya_services.sh status 
