#################################################################################
### This script Terminate MS SQL Pool and Database at AZ Resource Group 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User NT ID ] [Resource group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to drop MS SQL Pool  in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_location=eastus2
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
vm_name=viya35v01

function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

srv=sqlsrv
pool=pool
sql_server_name=$rs_group$srv
sql_pool_name=$rs_group$pool
sql_server_admin_user=$my_user
sql_server_admin_pwd=lnxsas@2020
echo " Resource Group  : $rs_group "
echo " SQL Pool Server Name  : $sql_server_name"
echo " SQL Pool Name  : $sql_pool_name"
echo " SQL Admin User Name  : $sql_server_admin_user"
echo " SQL Admin User Pwd   : $sql_server_admin_pwd"


###  Delete a SQL Pool  ####
echo "Terminating  a SQL Pool " 
sql_serv_sts=`az sql dw delete \
--name $sql_pool_name \
--server $sql_server_name \
--resource-group $rs_group \
--yes -y \
-o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not Terminate Azure SQL Pool : $sql_pool_name "
else
    echo "Terminated Azure SQL Pool  :$sql_pool_name"
fi

