DROP TABLE flight_reporting;
-- Creates an external table over the csv file
CREATE EXTERNAL TABLE flight_reporting (
    YEAR varchar(4) ,
    MONTH varchar(4),
    ORIGIN_AIRPORT_ID varchar(10),
    ORIGIN_AIRPORT_SEQ_ID varchar(10),
    ORIGIN_CITY_MARKET_ID varchar(10),
    ORIGIN_CITY_NAME varchar(20),
    DEST_AIRPORT_ID varchar(10),
    DEST_AIRPORT_SEQ_ID varchar(10),
    DEST_CITY_MARKET_ID varchar(10),
    DEST_CITY_NAME varchar(20)
    )
-- The following lines describe the format and location of the file
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION 'wasbs://fsdata@utkuma8hdistrg.blob.core.windows.net/flight_data';

select * from flight_reporting limit 5 ; 
