## udpate hostname and PasswordAuthentication to YES on all servers.
echo "Updating hostname and PasswordAuthentication  " 
for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo hostnamectl set-hostname $i ; sudo sed -i '/^[^#]*PasswordAuthentication[[:space:]]no/c\PasswordAuthentication yes' /etc/ssh/sshd_config ; sudo service sshd restart ;  "; 
done

#for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;do ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo grep PasswordAuthentication /etc/ssh/sshd_config;  "; done

## Disabling SELinux at all Cloud servers .
echo "Disabling SELinux at Cloud servers " 
for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo setenforce 0 ; sudo sed -i.bak -e 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config; "; 
done


## mount data disk to VMs 
echo "Mounting Data Disk to Host " 
for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo mkdir -p /opt/sas ; sudo umount /opt/sas; sudo sed -i '/sas/d' /etc/fstab; echo "/dev/sdc   /opt/sas   xfs    defaults,nofail        0       2" | sudo tee -a /etc/fstab ; sudo mount /opt/sas ; sudo df -h ; "; 
done

## expand /var and /tmp FS  
echo "Expanding  /var and /tmp folder at Host " 
for i in `grep 'viya' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo lvresize -l +20%FREE /dev/rootvg/varlv ; sudo xfs_growfs  /dev/rootvg/varlv ; sudo df -h ; "; 
done

for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo lvresize -l +20%FREE /dev/rootvg/tmplv ; sudo xfs_growfs  /dev/rootvg/tmplv ; sudo df -h ; "; 
done


## Update Ulimit  at VMs 
echo "Updating Ulimt to Host " 

echo "* soft nofile 48000" > /tmp/uconfig_org
echo "* hard nofile 48000" >> /tmp/uconfig_org
echo "* soft nproc 48000" >> /tmp/uconfig_org
echo "* hard nproc 48000" >> /tmp/uconfig_org
#cat /tmp/uconfig

echo "*          soft    nproc     unlimited " > /tmp/uconfig_1_org
echo "root          soft    nproc     unlimited " >> /tmp/uconfig_1_org
#cat /tmp/uconfig_1

## Copy the /tmp/uconfig and uconfig_1 to all cloud servers
for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
scp -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem /tmp/uconfig_1_org viyadep@$i:/tmp/uconfig_1 ; 
scp -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem /tmp/uconfig_org viyadep@$i:/tmp/uconfig ; 
done

## Update the ulimit at each VMs. 
for i in `grep 'viya\|cas' /etc/hosts | awk -F " " '{print $2;}'`;
do 
ssh -o "StrictHostKeyChecking=no" -i /home/viyadep/.ssh/private_key.pem $i "hostname; sudo sed -i '/48000/d' /etc/security/limits.conf; cat /tmp/uconfig | sudo tee -a /etc/security/limits.conf; cat /etc/security/limits.conf; sudo sed -i '/nproc/d' /etc/security/limits.d/20-nproc.conf; cat /tmp/uconfig_1 | sudo tee -a /etc/security/limits.d/20-nproc.conf; cat /etc/security/limits.d/20-nproc.conf; "; 
done

