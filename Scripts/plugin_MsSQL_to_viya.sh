###################################################################################
## This Script takes one parameter to decided the server to copy SSL lib file. 
###################################################################################
env_mode=$1
my_user=viyadep
my_pvt_key="~/.ssh/private_key.pem"

if [[ $env_mode == 'smp'  ]] ; then
  echo "SMP Environment.  "
  my_cas01=viya01
else
  echo "MPP Environment.  "
  my_cas01=cas01
fi


echo "My Private Key File : $my_pvt_key "

## Copy customised odbc.ini file to spre and viya fodler. 
echo " Copy customised odbc.ini file to spre and CAS fodler " 
#sudo cp ~/spre_odbc.ini /opt/sas/spre/home/lib64/accessclients/odbc.ini
#sudo cp ~/viya_odbc.ini /opt/sas/viya/home/lib64/accessclients/odbc.ini

cd ~
ansible viya01 -m copy -a "src=~/spre_odbc.ini dest=/opt/sas/spre/home/lib64/accessclients/odbc.ini owner=root group=root mode=0755 " -b
ansible all -m copy -a "src=~/viya_odbc.ini dest=/opt/sas/viya/home/lib64/accessclients/odbc.ini owner=root group=root mode=0755 " -b

## copy libS0ssl28.so files from cas01 server to viya server  
mkdir ~/SSL_LIB
rm  ~/SSL_LIB/*
scp -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$my_cas01:/opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so  ~/SSL_LIB/


## Copy libS0ssl28.so file to accessclients/lib folder 
echo " Copy libS0ssl28.so file to accessclients/lib folder" 
#sudo cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so /opt/sas/spre/home/lib64/accessclients/lib/
#sudo cp /opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so /opt/sas/viya/home/lib64/accessclients/lib/

#ansible all -m copy -a "src=/opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so dest=/opt/sas/spre/home/lib64/accessclients/lib/ owner=root group=root mode=0755 " -b
#ansible all -m copy -a "src=/opt/sas/viya/home/SASEventStreamProcessingEngine/6.2/ssl/lib/libS0ssl28.so dest=/opt/sas/viya/home/lib64/accessclients/lib/ owner=root group=root mode=0755 " -b

ansible viya01 -m copy -a "src=~/SSL_LIB/libS0ssl28.so dest=/opt/sas/spre/home/lib64/accessclients/lib/ owner=root group=root mode=0755 " -b
ansible all -m copy -a "src=~/SSL_LIB/libS0ssl28.so dest=/opt/sas/viya/home/lib64/accessclients/lib/ owner=root group=root mode=0755 " -b

