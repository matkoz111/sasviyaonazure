###################################################################
### This script REstarts  AZ VM server in MPP Viya environment
####################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group name to create VM and related objects  e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to Create an Azure VM from a disk snapshot and install Viya"
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi


user_nt_id=$1
user_rs_group=$2

#vm_name=viya35v01
my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 
vm_location=eastus2

snapshot_location=eastus2
snpshot_rs_group=utkuma
dt_snapshot_name=viya35m_01

function error_exit {
    echo "${1}"
    exit 1
}

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi


## checking for valid  Azure Resource Group 
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

## declare Virtual machine name
vm_name=$rs_group-viya35v01

echo "Getting VM ID"
vm_id=`az vm show -g $rs_group -n $vm_name --query id -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not find VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM ID: $vm_id"

## Re-Start an Azure Redhat Linux VM server  
echo "Start The  VM "
vm_status=`az vm start --resource-group $rs_group --name $vm_name`
if [ "$?" -ne "0" ]; then
    error_exit "Could not start VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM Status : $vm_status"

sleep 60

echo "Getting VM Public IP"
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`
echo "VM Public IP : $vm_public_ip"
echo $vm_public_ip "  viya01.onazure.sas.com viya01"  > user_host_file ;

viya_vm_public_ip=$vm_public_ip

echo " ============= " >> resource_info
echo "To Jump to Viya01 cloud server : " >> resource_info
echo " ============= " >> resource_info
echo "my_private_key=$my_private_key " >> resource_info
echo "my_user=$my_user " >> resource_info
echo "vm_public_ip=$vm_public_ip" >> resource_info
echo "  "  >> resource_info
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$vm_public_ip " >> resource_info
echo "  " >> resource_info

## Prepare script to Jump to Viya01 server
echo "my_private_key=$my_private_key " > To_jump_to_viya01_server
echo "my_user=$my_user " >> To_jump_to_viya01_server
echo "vm_public_ip=$vm_public_ip" >> To_jump_to_viya01_server
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$vm_public_ip " >> To_jump_to_viya01_server
chmod 755 To_jump_to_viya01_server


## Steps to Stop CAS Servers 
cas_node=4
cas_ctr=1

#echo " CAS Nodes  :$cas_node"
#echo " CAS CTR    :$cas_ctr"

## While Loop start
while [[ $cas_ctr -le $cas_node ]]
do
#echo " Cas CTR :$cas_ctr"
#vm_name=viya35c0$cas_ctr
vm_name=$rs_group-viya35c0$cas_ctr


echo "Restart The CAS VM "
vm_status=`az vm start --resource-group $rs_group --name $vm_name`
if [ "$?" -ne "0" ]; then
    error_exit "Could not start VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM Status : $vm_status"

sleep 60

echo "Getting VM Public IP"
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`
echo "VM Public IP : $vm_public_ip"
echo $vm_public_ip "  cas0$cas_ctr.onazure.sas.com cas0$cas_ctr"  >> user_host_file ;


cas_ctr=`expr $cas_ctr + 1 `
done
## While Loop end
### End of Steps to re-start 1 CAS Controller and 2 CAS Nodes servers

sleep 30 

vm_public_ip=$viya_vm_public_ip
## Start the Viya Services 
user_action='start'
scp -o "StrictHostKeyChecking=no" -i $my_private_key viya_services.sh $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key initialise_ephemeral_folder.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/initialise_ephemeral_folder.sh
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/viya_services.sh $user_action

