## Verify Ansible version and  access to hosts 
echo "Verify Ansible on VM " 
ansible --version
ansible localhost -m ping
ansible localhost -b -m yum -a "name=git,dstat,tmux state=present"
ansible all -m ping
ansible all -m shell -a "hostname -f"

## distribute the updated /etc/hosts file to all Servers in the cluster.
cd ~
ansible all -m copy -a "src=/etc/hosts dest=/etc/hosts owner=root group=root mode=0644" -b
ansible all -a "ls -l /etc/hosts" ;
ansible all -a "cat /etc/hosts" ;

