###################################################################
### This script creates SSH keys  
### This SSH keys used during Azure VM server creation 
####################################################################
if [ $# -lt 1 ]; then
    progname=${0##*/}
    echo "Usage: $progname [Win NT User id ]"
    echo
    echo "This script allows you to Create a SSH Key which will be used for creating an Azure VM. "
    echo "Script requires one parameter :"
    echo "     1. Win NT User Id for SSH keys generation  e.g. utkuma / Win_NT_ID "
    echo
    exit 1
fi

my_user=$1
#my_user=utkuma9
## In case you are running this for the second time, we don't want to override the old key.
## so there is an if condition in the mix. 
if [ -f ~/.ssh/$my_user-viyadep-key ]; then
    echo "The file '~/.ssh/$my_user-viyadep-key' exists. Aborting"
else
    echo "Generating new keys: '~/.ssh/$my_user-viyadep-key' "
    ssh-keygen -q -t rsa  -N '' -f ~/.ssh/$my_user-viyadep-key  -C viyadep
    chmod 600 ~/.ssh/$my_user-*
fi
