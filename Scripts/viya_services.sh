#######################################################################
# This Script can be used to start all Services in Viya enviornment 
# Can also be used to stop all Services 
# Can also be used to get status of  all Services 
#######################################################################

if [ $# -lt 1 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [ Action  ] "
    echo
    echo "Script requires one parameter"
    echo "     1. Action  e.g. : start / stop / status "
    echo
    exit 1
fi

user_action=$1 

function error_exit {
    echo "${1}"
    exit 1
}

# Test for Valid User actions  start, stop, status
echo " User Action :"$user_action
if [[ $user_action != 'start'  ]] && [[ $user_action != 'stop'  ]] && [[ $user_action != 'status'  ]] ; then
    error_exit "Not a valid User Action :$user_action  ; use 'start' or 'stop' or 'status' as value .  "
fi

cd /opt/sas/playbook/sas_viya_playbook
ansible sas_all -b -m shell -a "/etc/init.d/sas-viya-all-services $user_action " ;

