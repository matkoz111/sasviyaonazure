#############################################################################
### This is the master script will calls subsequest script to 
### creates an AZ VM from disk snapshot :  
###        The Data disk Snapshot contains Viya Repository and Playbook. 
###        On AZ VM it istall Viya software using repository and playbook. 
### creates an Storage account   : 
###       Create an containers 
###       Loads samle data to Blob container 
##############################################################################

if [ $# -lt 3 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [New resource Azure group name] [ Environment mode ]  "
    echo
    echo "Script requires Three parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group name to create VM and related objects  e.g. utkuma5 / WinNTIDn "
    echo "     3. Environemnt Starting mode  e.g. smp / mpp "
    echo
    #echo "This script allows you to Start Azure Environment with VM machine, and Storage Accound "
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi


user_nt_id=$1
user_rs_group=$2
env_mode=$3

#user_nt_id=utkuma
#user_rs_group=utkuma3

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 
my_location=eastus2


function error_exit {
    echo "${1}"
    exit 1
}

## Generate SSH Keys if not present 
./To_Generate_SSH_Key.sh $user_nt_id

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

# Test for Valid Environment mode SMP or MMP 
echo " Environment mode :"$env_mode
if [[ $env_mode != 'smp'  ]] && [[ $env_mode != 'mpp'  ]]; then
    error_exit "Not a valid Environment mode:$env_mode  ; use 'smp' or 'mmp' as value .  " 
fi

## Set Azure default Subscripton to GELDM 
rs_sub_id=`az account set -s GELDM` 


## Create a Resource group  if it does not exists 
echo "Checking for valid Resourec group "
rs_group_id=`az group show -n $user_rs_group --query id -o tsv` 
if [ "$?" -ne "0" ]; then
      echo "Valid new resource group name. creating a new resource group.......  $user_rs_group " 
      rs_group_id=`az group create --name $user_rs_group --location $my_location --query id -o tsv `
      if [ "$?" -ne "0" ]; then
        error_exit "Could not create a new Resorce Group "
      fi
else 
      error_exit "Resource Group: $user_rs_group  already exists, choose a new name" 
      #echo "Resource Group: $user_rs_group  already exists, choose a new name" 
fi

rs_group=`az group show -n $user_rs_group --query name -o tsv` 
echo " Resource group : "  $rs_group 
echo " Private key    : "  $my_private_key
echo " Public  key    : "  $my_public_key

cldlgn_host_name=`uname -a `

echo " Informations about the Environment  "  $rs_group > resource_info
echo " =================================== "  $rs_group >> resource_info
echo " Resource group : "  $rs_group >> resource_info
echo " Private key    : "  $my_private_key >> resource_info
echo " Public  key    : "  $my_public_key >> resource_info
echo " Cloud Login Host Name   : "  $cldlgn_host_name  >> resource_info


## Create a new Network Security Group 
nsg_name=$rs_group-viya35v01-nsg
echo "Creating a new Network Security Group : $nsg_name "

nsg_id=`az network nsg create -n $nsg_name -g $rs_group  --tags name=$user_nt_id --query NewNSG.id -o tsv`
if [ "$?" -ne "0" ]; then
        error_exit "Could not create a new Network Security Group " $nsg_name
fi
echo "Created new Network Security Group : $nsg_id "

## Add new security rule to allow SAS trafic into the NSG   
echo "Adding a new Network Security rule : ssh1 "
nsg_rule_id=`az network nsg rule create -n ssh1 \
--priority 300 -g $rs_group \
--nsg-name $nsg_name \
--source-address-prefixes "149.173.0.0/16" \
--source-port-ranges "*" \
--destination-address-prefixes "*" \
--destination-port-ranges 22 443 1433 \
--access Allow  \
--protocol Tcp  \
--description "Inbound Traffic from SAS Network." \
--query id  \
-o tsv `
if [ "$?" -ne "0" ]; then
        error_exit "Could not create a new Network Security Rule "
fi
echo "Added a new Network Security Rule : $nsg_rule_id "


if [[ $env_mode == 'smp'  ]] ; then
  echo "Starting SMP Environment.  "
  ./To_Start_AZ_ViyaServer_SMP.sh $user_nt_id $rs_group
else 
  echo "Starting MPP Environment.  "
  ./To_Start_AZ_ViyaServer_MPP.sh $user_nt_id $rs_group
fi


###### Call Scripts to start related objects 
./To_Create_ADLS2_Storage_Account.sh $user_nt_id $rs_group
./To_Create_SQLServer_Database.sh $user_nt_id $rs_group $env_mode
