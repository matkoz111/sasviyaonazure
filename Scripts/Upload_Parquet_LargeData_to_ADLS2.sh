# Prepare data files from given seed data file. 
if [[ $# -lt 3 ]] ; then
    echo "Script requires Three parameter"
    echo "     1. Storage Account Name "
    echo "     2. Container/File Syatem Name  "
    echo "     3. Number of files  "
    exit 1
fi

storage_account_name=$1
fs_name=$2 
number_of_file=$3 

echo "Number of Files to copy :"$number_of_file

for ((i=1; i<=$number_of_file; i++)) ;
do  

#echo "Number:"$i ; 
### Load data to fsdata blob container
az storage blob upload \
--name "baseball/baseball_prqt_$i" \
--file "sample_data/baseball_prqt_0" \
--account-name $storage_account_name \
--container-name $fs_name \
--auth-mode login

done ; 
### List the data files from fsdata Blob container
az storage blob list \
--account-name $storage_account_name \
--container-name $fs_name   \
--output table \
--auth-mode login

