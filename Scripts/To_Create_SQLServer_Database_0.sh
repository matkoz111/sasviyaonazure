#################################################################################
### This script Creats MS SQL Server and Database at AZ Resource Group 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User NT ID ] [New resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / Win_NT_ID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma_RG / WIn_NT_Id_RG "
    echo
    #echo "This script allows you to create a MS SQL Server in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_location=eastus2
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
vm_name=viya35v01

function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


srv=sqlsrv
sql_server_name=$rs_group$srv
sql_server_admin_user=$my_user 
sql_server_admin_pwd=lnxsas@2020
echo " Resource Group  : $rs_group " 
echo " SQL Server Name  : $sql_server_name" 
echo " SQL Admin User Name  : $sql_server_admin_user" 
echo " SQL Admin User Pwd   : $sql_server_admin_pwd" 


###  Create a SQL Server  ####
echo "Creating a SQL Server  " 
sql_serv_sts=`az sql server create \
--name $sql_server_name \
--admin-user $sql_server_admin_user \
--admin-password $sql_server_admin_pwd \
--resource-group $rs_group \
--location $my_location \
--enable-public-network true \
-o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Azure SQL Server : $sql_server_name "
else
    echo "Created Azure SQL Server  :$sql_server_name"
fi



### Create a user Database in SQL Server  
sqldb_name=geldb
echo "Creating a database at Azure SQL Server " 
sql_serv_db_sts=`az sql db create \
--name $sqldb_name \
--server $sql_server_name \
--resource-group $rs_group \
--service-objective Basic \
--edition Basic \
--tags name=$user_nt_id \
--zone-redundant false `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Azure SQL Server Database  : $sqldb_name"
else
    echo "Created Azure SQL Server Database : $sqldb_name"
fi

### Add Vnet and Sub net to SQL server to access the database 
v_netId=`az network vnet list -g $rs_group --query [0].name -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not get Vnet Id at rs_group  : $rs_group"
else
    echo "Vnet Id for SQL Server Database : $v_netId"
fi

v_netName=`az network vnet list -g $rs_group --query [0].name -o tsv`

v_subnetId=`az network vnet list -g $rs_group --query [0].subnets[0].id -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not get sub_net Id at rs_group  : $rs_group"
else
    echo "sub_net Id for SQL Server Database : $v_subnetId"
fi

v_subnetName=`az network vnet list -g $rs_group --query [0].subnets[0].name -o tsv`

### Enable service poitn for Microsoft.SQL 
az network vnet subnet create -g $rs_group -n $v_subnetName --vnet-name $v_netName --address-prefix 10.0.1.0/24 --service-endpoints Microsoft.SQL


### Create a Vnet RUle at MS-SQL Server  
vnet_rule_sts=`az sql server vnet-rule create \
--name MyVRule \
--server $sql_server_name \
--resource-group $rs_group \
--subnet $v_subnetId `

if [ "$?" -ne "0" ]; then
    echo  "Could not Create Vnet Rule On SQL Server.  "
else
    echo "Created Vnet Rule on SQL Server  : $vnet_rule_sts"
fi

echo " ======================  "    >> resource_info
echo " SQL Server Environment "    >> resource_info
echo " SQL Server Name  : $sql_server_name"  >> resource_info
echo " SQL Database Name  : db$sql_server_name"  >> resource_info
echo " SQL Admin User Name  : $sql_server_admin_user"  >> resource_info
echo " SQL Admin User Pwd   : $sql_server_admin_pwd"  >> resource_info
echo " ======================  "    >> resource_info


### copy odbc.ini to Viya and SPRE folder  to Viya01 server .
echo " Copying updated ODBC.ini file to Viya server  "
echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Replace server name in ODBC.ini file. 
sed -i "/#sqls_gel start/,/#sqls_gel end/s/HostName=.*/HostName=$sql_server_name.database.windows.net/" spre_odbc.ini
sed -i "/#sqls_gel start/,/#sqls_gel end/s/HostName=.*/HostName=$sql_server_name.database.windows.net/" viya_odbc.ini

## Execute plugin_MsSQL_to_viya.sh from  Viya01 VM server
scp -o "StrictHostKeyChecking=no" -i $my_private_key spre_odbc.ini $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key viya_odbc.ini $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key plugin_MsSQL_to_viya.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/plugin_MsSQL_to_viya.sh 
