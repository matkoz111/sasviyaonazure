#################################################################################
### This script Creats Storage Account and containers in a AZ Resource Group 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to create a Storage Account in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_location=eastus2
my_subscription="GELDM"

function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


stor=adls2strg
storage_acc_name=$rs_group$stor
container_name=fs$rs_group$stor
data_container_name=fsdata
echo " Resource Group  : $rs_group " 
echo " Storage Account : $storage_acc_name " 
echo " Container  Name : $container_name " 
echo " Data Container  Name : $data_container_name " 

###  Create an Storgae Account ####
echo "Creating Storage Account " 
st_acc_id=`az storage account create --name $storage_acc_name \
--resource-group $rs_group \
--access-tier Hot \
--enable-hierarchical-namespace true \
--sku Standard_LRS \
--https-only true \
--kind StorageV2 \
--location $my_location \
--tags name=$user_nt_id \
--query id -o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create  Azure Storage Account: $storage_acc_name "
else
    echo "Created Azure Storage Account :$storage_acc_name  "
fi

echo " Storage Account id : $st_acc_id " 

### Create container under  Storage Account 
echo "Creating Container  at Azure Storage account " 
st_acc_cont_id=`az storage container create \
--name $container_name  \
--account-name $storage_acc_name \
--auth-mode login \
--fail-on-exist `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Container at Azure Storage Account: $container_name  "
else
    echo "Created Container at Azure Storage Account : $container_name  "
fi


### Create fsdata container for data files and external DB for Hive
echo "Creating Container  at Azure Storage account "
st_acc_cont_id=`az storage container create \
--name $data_container_name \
--account-name $storage_acc_name \
--auth-mode login \
--fail-on-exist `

if [ "$?" -ne "0" ]; then
    error_exit "Could not create Container at Azure Storage Account: $data_container_name "
else
    echo "Created Container at Azure Storage Account : $data_container_name "
fi


### upload a blob/file to storage account contianer/fodler
echo "uploading data objects into storage account container/folder  " 
data_folder=sample_data
dt_upload_sts=`az storage blob upload \
--name "$data_folder/cars.csv" \
--file $data_folder/cars_source.csv  \
--account-name $storage_acc_name \
--container-name $container_name  \
--auth-mode login `

if [ "$?" -ne "0" ]; then
    error_exit "Could not Upload data object to Azure Storage Account Container : fs$storage_acc_name "
else
    echo "Uploaded data object to Azure Storage Account Container : fs$storage_acc_name  "
fi



### list  blob/file from  storage account contianer/fodler
echo " listing objects from storage account container " 
az storage blob list \
--account-name $storage_acc_name \
--container-name $container_name  \
--output table \
--auth-mode login 

if [ "$?" -ne "0" ]; then
    error_exit "Could not list data object from Azure Storage Account Container : fs$storage_acc_name "
else
    echo "Listed data object from Azure Storage Account Container : fs$storage_acc_name  "
fi
