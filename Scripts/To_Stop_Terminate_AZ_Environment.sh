################################################################################################
### This script either Stop all VM machine or Terminate  AZ Resource Group with all objects 
################################################################################################

if [ $# -lt 4 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource group name] [Environment Mode ] [User Action ] "
    echo
    echo "Script requires four parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo "     3. Environment Mode :   smp    or    mpp  "
    echo "     4. User Action  :   stop     or    terminate   "
    echo
    #echo "This script allows you to drop the AZ resource group with all objects in it. "
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2
env_mode=$3
user_action=$4

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 
vm_location=eastus2

function error_exit {
    echo "${1}"
    exit 1
}

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

# Test for Valid Environment mode SMP or MMP
echo " Environment mode :"$env_mode
if [[ $env_mode != 'smp'  ]] && [[ $env_mode != 'mpp'  ]]; then
    error_exit "Not a valid Environment mode:$env_mode  ; use 'smp' or 'mmp' as value .  "
fi

# Test for Valid Environment mode SMP or MMP
echo " User Action  :"$user_action
if [[ $user_action != 'stop'  ]] && [[ $user_action != 'terminate'  ]]; then
    error_exit "Not a valid User Acton :$user_action  ; use 'stop' or 'terminate' as value .  "
fi


## Set Azure default Subscripton to GELDM
rs_sub_id=`az account set -s GELDM`


## Check if Azure Resource Group exists 
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists 
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists " 
   rs_group=`az group show -n $user_rs_group --query name -o tsv` 
else 
      error_exit "Azure Resource Group: $user_rs_group does not exists." 
fi


if [[ $user_action == 'stop'  ]] ; then
  echo "Stopping Azure VM Servers/Environment.  "
  if [[ $env_mode == 'smp'  ]] ; then 
     ./To_Stop_AZ_ViyaServer_SMP.sh $user_nt_id $rs_group
  else 
    ./To_Stop_AZ_ViyaServer_MPP.sh $user_nt_id $rs_group
  fi 
  ./To_Terminate_SQLServer_Database.sh $user_nt_id $rs_group
  ./To_Terminate_ADLS2_HDIcluster.sh $user_nt_id $rs_group
else
  echo "Terminate/Kill Azure Environment.  "
  ## Drop the Azure Resource Group 
  echo "Dropping/killing  Azure Resource Group : $rs_group"
  #rs_group_id=`az group delete --name $rs_group --query id --yes -y --no-wait -o tsv `
  rs_group_id=`az group delete --name $rs_group --yes -y --no-wait -o tsv `
  if [ "$?" -ne "0" ]; then
    error_exit "Could not drop Resource Group  $rs_group " 
  else 
    echo "Dropped Azure Resource Group: $rs_group " 
  fi
fi
