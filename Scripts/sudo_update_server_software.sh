# udpate OS  packages on VM
echo "Updating OS packages on VM " 
#yum update -y

# install EPEL package to install Ansible and related software.
echo "Installing EPEL package on VM " 
majversion=7
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-$majversion.noarch.rpm

## install pip
echo "Installing Python and Pip on VM " 
yum install -y python-pip gcc python-devel
pip install --upgrade pip setuptools

## use pip to install specific a specific ansible version
echo "Installing Ansible on VM " 
pip install 'ansible==2.8.10'
#ansible --version
#ansible localhost -b -m yum -a "name=git,dstat,tmux state=present"

## use pip to install Complex HTTP Server 
echo "Installing ComplexHTTPServer  on VM " 
pip install ComplexHTTPServer

chown -R viyadep:viyadep /opt/sas/mirror
chown -R viyadep:viyadep /opt/sas/playbook

chmod 755 /opt/sas/playbook/sas_viya_playbook/scripts/validate_java.sh
