my_user=viyadep
my_pvt_key="~/.ssh/private_key.pem"
hdi_server=hdputkuma8-ssh.azurehdinsight.net

# Copy SAS EP RPM files to HDI  Master Node and extract it.
ssh -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server "mkdir /tmp/RPMTempDir ; sudo apt install rpm -y ; sudo apt install alien -y; "
scp -o "StrictHostKeyChecking=no" -i $my_pvt_key /opt/sas/mirror/yummirror/repos/shipped/indbhive/125/indbhive-125-x64_redhat_linux_6-yum/Packages/s/* $my_user@$hdi_server:/tmp/RPMTempDir/

ssh -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server "cd /tmp/RPMTempDir ; ls -l;  sudo rpm -ivh sas-* ; "
ssh -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server "cd /tmp/RPMTempDir ; ls -l;  sudo alien -d -i sas-* ; "

# Deploy SAS EP from  HDI Master Node.
ssh -o "StrictHostKeyChecking=no" -i $my_pvt_key $my_user@$hdi_server "sudo chmod 777 -R /opt/sas/ep ; cd /opt/sas/ep/home/bin/ ; ./sasep-admin.sh -add -nohostcheck ; ./sasep-admin.sh -check -nohostcheck ; "


