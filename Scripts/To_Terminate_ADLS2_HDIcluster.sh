#################################################################################
### This script Terminate Azure HDI Cluster at AZ Resource Group 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User NT ID ] [Resource group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to terminate a HDI CLuster  in AZ resource group. "
    #echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_location=eastus2
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
vm_name=$user_rs_group-viya35v01

function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

stor=adls2strg
storage_acc_name=$rs_group$stor

## Find the Azure HDI CLuster starting with hdi in your resource group. 
hdi_cluster_name=`az hdinsight list \
-g $rs_group \
--query "[?contains(name,'hdi')].name" \
-o tsv ` 

if [ "$?" -ne "0" ]; then
    error_exit "Could not find Azure HDI CLuster  in Resource Group : $rs_group "
else
    echo "Found HDI CLuster  :$hdi_cluster_name"
fi


## Find Storage Account Container for HDI Cluster 
hdi_container_name=`az hdinsight show \
-g $rs_group \
-n $hdi_cluster_name \
--query "properties.storageProfile.storageaccounts[].fileSystem" \
-o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not find ADLS2 Container for Azure HDI CLuster  : $hdi_cluster_name"
else
    echo "Found ADLS2 Container for HDI CLuster  :$hdi_container_name"
fi

echo " Resource Group       : $rs_group " 
echo " HDI_Cluster Name     : $hdi_cluster_name " 
echo " Storage Account Name : $storage_acc_name " 
echo " HDI Container Name   : $hdi_container_name" 


###  Terminate a HDI Cluster  ####
echo "Terminating  a HDI Cluster Server  " 
hdi_sts=`az hdinsight delete \
--name $hdi_cluster_name \
--resource-group $rs_group \
--yes -y \
-o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not Terminate Azure HDI CLuster  : $hdi_cluster_name "
else
    echo "Terminated Azure HDI CLuster :$hdi_cluster_name"
fi

###  Delete a storage account HDI Container   ####
echo "Deleting  storage account HDI container " 
hdi_container_sts=`az storage container delete \
--name $hdi_container_name \
--account-name $storage_acc_name \
--auth-mode login \
-o tsv `

if [ "$?" -ne "0" ]; then
    error_exit "Could not delete Storage Account Container : $hdi_container_name"
else
    echo "Deleted Storage Account Container :$hdi_container_name "
fi
