#################################################################################
### This script distribute JDBC driver from Script folder to Viya environment 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource group name] [HDI Cluster name ] "
    echo
    echo "Script requires three parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma5 / WinNTIDn "
    echo
   # echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
   # echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
my_location=eastus2
vm_name=$user_rs_group-viya35v01


function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


echo " Resource Group  : $rs_group "

### At Viya01 server create a folder and copy JDBC jar file.  
echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Execute plugin_JDBCDriver_to_viya.sh from  Viya01 VM server 
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip " rm -rf JDBCDriver ; mkdir JDBCDriver;" 
scp -o "StrictHostKeyChecking=no" -i $my_private_key plugin_JDBCDriver_to_viya.sh $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key ./JDBCDriver/* $my_user@$vm_public_ip:~/JDBCDriver/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/plugin_JDBCDriver_to_viya.sh
