###################################################################
### This script creates AZ VM from disk snapshot  
### The Data disk Snapshot contains Viya Repository and Playbook. 
### On AZ VM it istall MPP Viya software using repository and playbook. 
####################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [Resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / WinNTID "
    echo "     2. Azure Resource group name to create VM and related objects  e.g. utkuma5 / WinNTIDn "
    echo
    #echo "This script allows you to Create an Azure VM from a disk snapshot and install Viya"
    #echo "THis script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
    #echo "(after install run 'az login')"
    echo
    exit 1
fi


user_nt_id=$1
user_rs_group=$2

#user_nt_id=utkuma
#user_rs_group=utkuma3

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub 

snapshot_location=eastus2
snpshot_subscription=GELDM
snpshot_rs_group=utkuma
dt_snapshot_name=viya35m_01
dt_cas_snapshot_name=viya35m_c01

function error_exit {
    echo "${1}"
    exit 1
}

## Verify public keys file for the WiN NT ID. 
if [ ! -f "$my_public_key" ]
then
    error_exit "$0: File '${my_public_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi

## Verify private keys file for the WiN NT ID. 
if [ ! -f "$my_private_key" ]
then
    error_exit "$0: File '${my_private_key}' not found. Generate your ssh keys using script 'To_Generate_SSH_Keys.sh'. "
fi


## checking for valid  Azure Resource Group 
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi


echo " Resource group : "  $rs_group 
echo " Private key    : "  $my_private_key
echo " Public  key    : "  $my_public_key

#echo " Resource group : "  $rs_group > resource_info
#echo " Private key    : "  $my_private_key >> resource_info
#echo " Public  key    : "  $my_public_key >> resource_info



## These steps are for Viya01 server 
###########
###################

vm_size=Standard_D14_v2
#vm_size=Standard_DS2_v2
vm_image=RHEL
vm_name=$rs_group-viya35v01
vm_location=eastus2
nsg_name=$rs_group-viya35v01-nsg


## Get the ID of the data disk snapshot to attach with new VM
echo "Fetching snapshot ID"
snapshot_id=`az snapshot show -n $dt_snapshot_name --subscription $snpshot_subscription -g $snpshot_rs_group --query id -o tsv`
if [ "$?" -ne "0" ]; then
      error_exit "Could not fetch snapshot ID"
fi
echo "Snapshot ID: $snapshot_id"

## Create a name for the new disk
new_disk_name="${vm_name}_${dt_snapshot_name}_`date +%Y-%m-%d_%s`"

## Create new disk from snapshot
echo "Creating a new data disk with name $new_disk_name from snapshot"
new_disk_id=`az disk create -n $new_disk_name -g $rs_group \
--location $vm_location \
--sku Standard_LRS \
--source $snapshot_id \
--tags name=$user_nt_id \
--query id -o tsv `
if [ "$?" -ne "0" ]; then
	error_exit "Could not create a new disk from snapshot"
fi
echo "Created new disk with ID $new_disk_id"


## Start an Azure Redhat Linux VM server  
echo "Starting a VM with data disk "
az vm create \
--resource-group $rs_group \
--name $vm_name \
--image $vm_image \
--admin-username $my_user \
--ssh-key-values $my_public_key \
--location $vm_location \
--nsg $nsg_name \
--size $vm_size \
--storage-sku Standard_LRS \
--os-disk-size-gb 64 \
--attach-data-disks $new_disk_id \
--tags name=$user_nt_id \
-o tsv \


echo "Getting VM ID"
vm_id=`az vm show -g $rs_group -n $vm_name --query id -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not find VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM ID: $vm_id"

echo "Getting VM Private IP"
viya_vm_private_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.privateIpAddresses -o tsv`
echo "VM Private IP : $vm_private_ip "
echo $viya_vm_private_ip "  viya01.onazure.sas.com viya01"  > hosts_file ;

echo "Getting VM Public IP"
viya_vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`
echo "VM Public IP : $viya_vm_public_ip"
echo $viya_vm_public_ip "  viya01.onazure.sas.com viya01"  > user_host_file ;

echo " ============= " >> resource_info
echo "Machine Viya01 "  >> resource_info
echo "VM Private IP : $viya_vm_private_ip " >> resource_info 
echo "VM Public IP : $viya_vm_public_ip" >> resource_info

#### 
##### End of Viya01 server  Stpeps 
#####

### Steps to create 1 CAS Controller and 2 CAS Nodes servers
## Get the Snapshot ID of the CAS data disk snapshot to attach with new VM
echo "Fetching snapshot ID"
snapshot_id=`az snapshot show -n $dt_cas_snapshot_name -g $snpshot_rs_group --query id -o tsv`
if [ "$?" -ne "0" ]; then
      error_exit "Could not fetch snapshot ID"
fi
echo "Snapshot ID: $snapshot_id"

vm_size=Standard_DS3_v2
#vm_size=Standard_DS2_v2
vm_image=RHEL
nsg_name=$rs_group-viya35v01-nsg
cas_node=4
cas_ctr=1

#echo " CAS Nodes  :$cas_node" 
#echo " CAS CTR    :$cas_ctr" 

## While Loop start
while [[ $cas_ctr -le $cas_node ]]
do

#echo " Cas CTR :$cas_ctr" 
vm_name=$rs_group-viya35c0$cas_ctr

## Create a name for the new disk
new_disk_name="${vm_name}_${dt_cas_snapshot_name}_`date +%Y-%m-%d_%s`"
#echo "New Desk Name : $new_disk_name " 

## Create new disk from snapshot
echo "Creating a new data disk with name $new_disk_name from snapshot"
new_disk_id=`az disk create -n $new_disk_name -g $rs_group \
--location $vm_location \
--sku Standard_LRS \
--source $snapshot_id \
--tags name=$user_nt_id \
--query id -o tsv `
if [ "$?" -ne "0" ]; then
        error_exit "Could not create a new disk from snapshot"
fi
echo "Created new disk with ID $new_disk_id"


## Start an Azure Redhat Linux VM server
echo "Starting a VM with data disk "
az vm create \
--resource-group $rs_group \
--name $vm_name \
--image $vm_image \
--admin-username $my_user \
--ssh-key-values $my_public_key \
--location $vm_location \
--nsg $nsg_name \
--size $vm_size \
--storage-sku Standard_LRS \
--os-disk-size-gb 64 \
--attach-data-disks $new_disk_id \
--tags name=$user_nt_id \
-o tsv \


echo "Getting VM ID"
vm_id=`az vm show -g $rs_group -n $vm_name --query id -o tsv`
if [ "$?" -ne "0" ]; then
    error_exit "Could not find VM '$vm_name' in resource group '$rs_group'"
fi
echo "VM ID: $vm_id"

echo "Getting VM Private IP"
vm_private_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.privateIpAddresses -o tsv`
echo "VM Private IP : $vm_private_ip "
echo $vm_private_ip "  cas0$cas_ctr.onazure.sas.com cas0$cas_ctr"  >> hosts_file ;

echo "Getting VM Public IP"
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`
echo "VM Public IP : $vm_public_ip"
echo $vm_public_ip "  cas0$cas_ctr.onazure.sas.com cas0$cas_ctr"  >> user_host_file ;


echo " ============= " >> resource_info
echo "Machine cas0$cas_ctr "  >> resource_info
echo "VM Private IP : $vm_private_ip " >> resource_info
echo "VM Public IP : $vm_public_ip" >> resource_info


cas_ctr=`expr $cas_ctr + 1 `

done
## While Loop end 

### End of Steps to create 1 CAS Controller and 2 CAS Nodes servers

vm_public_ip=$viya_vm_public_ip
#### Environment Variables ####

echo " ============= " >> resource_info
echo " To Jump to Viya01 cloud server : " >> resource_info
echo " ============= " >> resource_info
echo "my_private_key=$my_private_key " >> resource_info
echo "my_user=$my_user " >> resource_info
echo "vm_public_ip=$vm_public_ip" >> resource_info
echo "  "  >> resource_info
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$vm_public_ip " >> resource_info
echo "  " >> resource_info

## Prepare script to Jump to Viya01 server
echo "my_private_key=$my_private_key " > To_jump_to_viya01_server
echo "my_user=$my_user " >> To_jump_to_viya01_server
echo "vm_public_ip=$vm_public_ip" >> To_jump_to_viya01_server
echo "ssh -o "StrictHostKeyChecking=no" -i \$my_private_key \$my_user@\$vm_public_ip " >> To_jump_to_viya01_server
chmod 755 To_jump_to_viya01_server


# Copy host file to AZ VM server 
## copy host and private key  file to host  
scp -o "StrictHostKeyChecking=no" -i $my_private_key hosts_file $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key $my_private_key $my_user@$vm_public_ip:~/.ssh/private_key.pem

## Update /etc/hosts file and SELinx 
scp -o "StrictHostKeyChecking=no" -i $my_private_key sudo_update_hosts.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip sudo /home/viyadep/sudo_update_hosts.sh 

## Update host configuratuons  hostanme on all server 
scp -o "StrictHostKeyChecking=no" -i $my_private_key update_hosts_config.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/update_hosts_config.sh

## Install ansible and related software to VM  
scp -o "StrictHostKeyChecking=no" -i $my_private_key sudo_update_server_software.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip sudo /home/viyadep/sudo_update_server_software.sh 

## Verify ansible version at VM  
scp -o "StrictHostKeyChecking=no" -i $my_private_key verify_ansible_software.sh $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key ansible_work.cfg $my_user@$vm_public_ip:~/ansible.cfg
scp -o "StrictHostKeyChecking=no" -i $my_private_key inventory_work_mpp.ini $my_user@$vm_public_ip:~/inventory.ini
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/verify_ansible_software.sh

## Perfom Viya Pre-INstall  at VM  
scp -o "StrictHostKeyChecking=no" -i $my_private_key initialise_ephemeral_folder.sh $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key validate_java_n.sh $my_user@$vm_public_ip:/opt/sas/playbook/sas_viya_playbook/scripts/validate_java.sh
scp -o "StrictHostKeyChecking=no" -i $my_private_key pre_install_viya.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/pre_install_viya.sh

## Install Open LDAP at VM  
scp -o "StrictHostKeyChecking=no" -i $my_private_key install_ldap.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/install_ldap.sh

## Deploy Viya software at VM  
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip "rm -f /opt/sas/playbook/sas_viya_playbook/ansible.cfg"
scp -o "StrictHostKeyChecking=no" -i $my_private_key ansible_playbook.cfg $my_user@$vm_public_ip:/opt/sas/playbook/sas_viya_playbook/ansible.cfg
scp -o "StrictHostKeyChecking=no" -i $my_private_key vars_playbook_mpp.yml  $my_user@$vm_public_ip:/opt/sas/playbook/sas_viya_playbook/vars.yml
scp -o "StrictHostKeyChecking=no" -i $my_private_key inventory_playbook_mpp.ini $my_user@$vm_public_ip:/opt/sas/playbook/sas_viya_playbook/inventory.ini
#scp -o "StrictHostKeyChecking=no" -i $my_private_key viya.services*.yml $my_user@$vm_public_ip:/opt/sas/playbook/sas_viya_playbook/
scp -o "StrictHostKeyChecking=no" -i $my_private_key install_viya.sh $my_user@$vm_public_ip:~/
scp -o "StrictHostKeyChecking=no" -i $my_private_key viya_services.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/install_viya.sh

