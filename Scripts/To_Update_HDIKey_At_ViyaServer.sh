#################################################################################
### This script copy updated core-site.xml to Viya server and distributes it.
### The core-site.xml updated with decrepted keys from  Storage Account 
#################################################################################

if [ $# -lt 2 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [Win NT User id ] [New resource Azure group name] "
    echo
    echo "Script requires two parameters"
    echo "     1. Win NT User Id used for SSH keys generation  e.g. utkuma / Win_NT_ID "
    echo "     2. Azure Resource group containing all related objects e.g. utkuma_RG / WIn_NT_Id_RG "
    echo
   # echo "This script allows you to create a HDInsight CLuster in AZ resource group. "
   # echo "This script requires the 'az' command. This can be installed by running E.g. 'pip install azure-cli'"
   # echo "(after install run 'az login')"
    echo
    exit 1
fi

user_nt_id=$1
user_rs_group=$2

my_user=viyadep
my_private_key=~/.ssh/$user_nt_id-viyadep-key
my_public_key=~/.ssh/$user_nt_id-viyadep-key.pub
my_location=eastus2
vm_name=viya35v01


function error_exit {
    echo "${1}"
    exit 1
}

## Check if Azure Resource Group exists
echo "Checking for Azure Resource Group "
rs_group_exists=`az group exists --name $user_rs_group `
#echo "rs_group_exists= "$rs_group_exists
if [ "$rs_group_exists" == "true" ]; then
   echo "Azure Resource Group Exists : $user_rs_group : $rs_group_exists "
   rs_group=`az group show -n $user_rs_group --query name -o tsv`
else
      error_exit "Azure Resource Group: $user_rs_group does not exists."
fi

# generate core-site.xml for HDI hadoop cluster with access Key
#access_key=`cat hdi_strg_acc_key`
#echo $access_key


### copy and distribute the core-site.xml at Viya01 server .
echo " Getting Viya01 VM Public IP "
vm_public_ip=`az vm list-ip-addresses -g $rs_group -n $vm_name --query [].virtualMachine.network.publicIpAddresses[].ipAddress -o tsv`

if [ "$?" -ne "0" ]; then
    error_exit "Could not get public IP of Viya01 VM server  : $vm_name"
else
    echo "Public Ip of Viya01 VM : $vm_public_ip "
fi

## Distribute updated core-site.xml to Viya and CAS Servers 
scp -o "StrictHostKeyChecking=no" -i $my_private_key core-site.xml $my_user@$vm_public_ip:~/HDIHadoopData/conf/
scp -o "StrictHostKeyChecking=no" -i $my_private_key distribute_coresite.sh $my_user@$vm_public_ip:~/
ssh -o "StrictHostKeyChecking=no" -i $my_private_key $my_user@$vm_public_ip /home/viyadep/distribute_coresite.sh 
