![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# SAS access to Synapse SQL Pool database table
- [Login to Viya application](#login-to-viya-application)
- [SAS access to Synapse SQL Pool database table](#sas-access-to-synapse-sql-pool-database-table)

Purpose of this hands-on is to demonstrate the data access from SAS to Synapse SQL Pool database table. Data read to SAS using SAS/ACCESS to MS-SQL interface (ODBC driver).

To connect Azure Synapse SQL Pool database, it requires ODBC driver and customized odbc.ini with a DSN entry. The ODBC driver is installed as part of environment startup process and customized odbc.ini is deployed later by a script.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## SAS access to Synapse SQL Pool database table
On SAS Studio execute the following code to save and read SQL Pool database table to SAS Compute Server.


**Before executing the code change/update the value for Schema= parameter.**

Notice: The URL with user name 'token' and PWD=< Databricks WS token >

Code:

```sas

libname azsqlws sqlsvr
noprompt="uid=viyadep;pwd=lnxsas@2020;dsn=sqls_gelws;schema=utkuma8pool"
stringdates=yes;

data azsqlws.fish_sas ;
set sashelp.fish ;
run;

PROC SQL outobs=20;
select * from azsqlws.fish_sas;
run;
quit;

```

Results:

```log
1    %studio_hide_wrapper;
82   libname azsqlws sqlsvr
83   noprompt=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
84   stringdates=yes;
NOTE: Libref AZSQLWS was successfully assigned as follows:
      Engine:        SQLSVR
      Physical Name: sqls_gelws
85
86   data azsqlws.fish_sas ;
87   set sashelp.fish ;
88   run;
NOTE: There were 159 observations read from the data set SASHELP.FISH.
NOTE: The data set AZSQLWS.fish_sas has 159 observations and 7 variables.
NOTE: DATA statement used (Total process time):
      real time           12.14 seconds
      cpu time            0.18 seconds

89
90   Proc SQL outobs=20;
91   select * from azsqlws.fish_sas ;
WARNING: Statement terminated early due to OUTOBS=20 option.
92   run;
NOTE: PROC SQL statements are executed immediately; The RUN statement has no effect.
93   quit;
NOTE: The PROCEDURE SQL printed page 1.
NOTE: PROCEDURE SQL used (Total process time):
      real time           0.42 seconds
      cpu time            0.14 seconds


```

Results:

Results from PROC SQL statement.

![1](img/utkuma_2020-07-02-15-28-30.png)


<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)**<-- you are here**
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
