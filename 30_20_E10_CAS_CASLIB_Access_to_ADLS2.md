![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Data file Access from CAS to Azure ADLS2
- [Login to Viya Application](#login-to-viya-application)
- [Data save from CAS to ADLS2 (CSV and ORC)](#data-save-from-cas-to-adls2-csv-and-orc)
  - [Authenticate device for Azure ADLS2 access](#authenticate-device-for-azure-adls2-access)
    - [Steps to login to Viya/CAS Unix servers](#steps-to-login-to-viyacas-unix-servers)
      - [You are on Viya01/CAS01/CAS02 server](#you-are-on-viya01cas01cas02-server)
  - [Return to SAS studio and re-run code](#return-to-sas-studio-and-re-run-code)
  - [Verify saved data file at ADLS2](#verify-saved-data-file-at-adls2)
- [CAS load from ADLS2 data files (CSV and ORC)](#cas-load-from-adls2-data-files-csv-and-orc)
  - [Authenticate each CAS Node for CSV load (ADLS2 CSV file)](#authenticate-each-cas-node-for-csv-load-adls2-csv-file)
    - [Steps to login to CAS Unix servers](#steps-to-login-to-cas-unix-servers)
  - [Return to SAS studio and re-run CAS (csv) load code](#return-to-sas-studio-and-re-run-cas-csv-load-code)
  - [CAS Load  from multiple .orc file into a single table](#cas-load-from-multiple-orc-file-into-a-single-table)

Purpose of this hands-on is to demonstrate the data file access from CAS to Azure Data Lake Storage 2 (ADLS2). CAS supports CSV and ORC data file with read and write to ADLS2 storage.

To connect ADLS2 from SAS Viya/CAS, you need Storage account information with tenant it and application id. Assuming you have information from previous section.

## Login to Viya Application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)

## Data save from CAS to ADLS2 (CSV and ORC)

On SAS Studio editor execute the following code to save a CAS table to ADLS2 as .csv and .orc file.

**Before running the code replace the value for Storage account name, and  filesystem name with information collected in previous section.**

Very first time access to ADLS2 CASLIB (e.g. list files statement) generate an error msg in sas log, follow next steps to authenticate.

Code:

```sas
/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;

/*
Example:
%let MYSTRGACC="utkuma8adls2strg";
%let MYSTRGFS="fsutkuma8adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-a430-xxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-9eda-xxxxxxxxxxxx";
*/


CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib ADLS2 datasource=(
      srctype="adls"
      accountname=&MYSTRGACC
      filesystem=&MYSTRGFS
      dnsSuffix=dfs.core.windows.net
      timeout=50000
      tenantid=&MYTNTID
      applicationId=&MYAPPID
   )
   path="/sample_data"
   subdirs;

proc casutil incaslib="ADLS2";
   list files ;
run;
quit;

/* Load a sample table to CAS under public CASLIB */
PROC CASUTIL incaslib="public" outcaslib="public";
    droptable casdata="sas_orsales"  quiet;
    LOAD DATA=sashelp.orsales CASOUT="sas_orsales" promote;
RUN;

/* Save CAS table to ADLS2 storage */
proc casutil  incaslib="public"  outcaslib="ADLS2";
   save casdata="sas_orsales" casout="sas_orsales.orc" replace ;
   save casdata="sas_orsales" casout="sas_orsales.csv" replace ;
run;
quit;

cas mysession terminate;
```

### Authenticate device for Azure ADLS2 access

Note: Very first time access to ADLS2 CASLIB (e.g. list files statement) generate following error msg in sas log. User need to login to 'https://microsoft.com/devicelogin' site to register and authenticate the device code for CAS server/s.

Results:

```log
NOTE: Executing action 'table.fileInfo'.
ERROR: To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code AMUPDJ8N7 to
       authenticate.
ERROR: Pending end-user authorization.
ERROR: The action stopped due to errors.
NOTE: Action 'table.fileInfo' used (Total process time):
```

On a web browser open the site //microsoft.com/devicelogin and use the code listed in the log to authenticate the device.

Note: If user takes too long to authenticate, the code might expire and require to regenerate it. To re-generate the device code remove the /home/cas/.sasadls....json file and rerun the sas code. Use “sudo rm  /home/cas/.sasadls_10002.json “ statement to remove file at viya01/cas01 server.

#### Steps to login to Viya/CAS Unix servers

**In case you need to view/delete the .json file at viya and cas servers**

Login to cldlgn.fyi.sas.com server and cd to ..../Scripts folder and concatenate the "user_host_file" and "resource_info" file. These file contains the ip address and information about how to jump onto Viya01 server.

In MPP environment, to jump to CAS server assign the vm_public_ip= with respective CAS server public IP and it will take you there.

```bash
cd ~/PSGEL265-sas-viya-3.5....../Scripts

more user_host_file

more resource_info
```

Look for “To Jump to Viya01 cloud server:” section from executed statement. Don't execute the following log example, it's just for reference purpose. You need to run statements what you see at your X-term window.

```log
………………………..
……
=============
To Jump to Viya01 cloud server :
=============
my_private_key=/r/ge.unx.sas.com/vol/vol310/u31/utkuma/.ssh/utkuma-viyadep-key
my_user=viyadep
vm_public_ip=52.254.10.86

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
```

Execute listed 4 lines from your X-Term display at cldlgn.fyi.sas.com server and it will take you to Viya01 server.

Code:

```bash

example:
my_private_key=/r/ge.unx.sas.com/vol/vol310/u31/utkuma/.ssh/utkuma-viyadep-key
my_user=viyadep
vm_public_ip=52.254.10.86

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip
```

##### You are on Viya01/CAS01/CAS02 server

On Unix server execute the following statement to get the device registration instructions. The .json file may have generated with different random number, before running 'more' statement, double check the file name.

Code:

```bash
sudo ls -la /home/cas/
sudo more /home/cas/.sasadls_100001.json

exit

```

Results:

```log
[viyadep@viya01 ~]$ sudo ls -la /home/cas/
total 32
…..
….
-rw-------.  1 cas cas  408 Apr 22 16:10 .sasadls_100001.json
....
...
[viyadep@viya01 ~]$


[viyadep@viya01 ~]$ sudo more /home/cas/.sasadls_100001.json
{"refresh_token":"","device_code":"AAQABAAEAAAAm-06blBE1TpVMil8KPQ41GgI-YkVkIf2nhpSkRd9jM9j2N5GfLEPotyL7Bibs0gFHjjvfATdJKe22oKxRjVIIv-djHs_-0JAdgTTMEu70qS
cODRuYDpQigEI1Mx2RJf-fQj_vgpL5p_nD2T4PWrxhxZx7J6e4BXDdJmslfece7O5r_diYESDIV-V5WuX14CsgAA","message":"To sign in, use a web browser to open the page https:
//microsoft.com/devicelogin and enter the code AAM8LARQT to authenticate.","oauth":"","resource":""}
[viyadep@viya01 ~]$
```

### Return to SAS studio and re-run code

After device authentication process, re-run “list files” statement and rest of code at SAS Studio.

Results:

```log
…………….
….
82   proc casutil incaslib="ADLS2";
NOTE: The UUID 'e43b3980-e1af-b646-8b75-fa42986fa0bb' is connected using session MYSESSION.
83      list files ;
NOTE: Executing action 'table.caslibInfo'.
…..
………………..


NOTE: Action 'table.caslibInfo' used (Total process time):
NOTE: Executing action 'table.fileInfo'.
                                                        CAS File Information
                                                                                                                Last Modified
 Name            Permission    Owner                  Group                  ETag                  File Size    (UTC)
 cars.csv        -rw-r-----    Uttam.Kumar@SAS.COM    Uttam.Kumar@SAS.COM    0x8D7E613CF1257AC        34.6KB     21APR2020:16:48:24
 fish_n          drwxr-x---    $superuser             Uttam.Kumar@SAS.COM    0x8D7E6DE879EA1CA         0.0KB     22APR2020:16:59:32
 fish_new.orc    -rw-r-----    Uttam.Kumar@SAS.COM    Uttam.Kumar@SAS.COM    0x8D7E6E1D00DE246         5.1KB     22APR2020:17:23:02
 fish_orc.orc    -rw-r-----    Uttam.Kumar@SAS.COM    Uttam.Kumar@SAS.COM    0x8D7E6DBFDBDE44B         4.7KB     22APR2020:16:41:22
NOTE: Action 'table.fileInfo' used (Total process time):
NOTE:       real time               0.986084 seconds
 ……………
…………………..


82   /* Save CAS table to ADLS2 storage */
83   proc casutil  incaslib="public"  outcaslib="ADLS2";
NOTE: The UUID 'e43b3980-e1af-b646-8b75-fa42986fa0bb' is connected using session MYSESSION.
84      save casdata="sas_orsales" casout="sas_orsales.orc" replace ;
NOTE: Executing action 'table.save'.
NOTE: Cloud Analytic Services saved the file sas_orsales.orc in caslib ADLS2.
85      save casdata="sas_orsales" casout="sas_orsales.csv" replace ;
NOTE: Executing action 'table.save'.
NOTE: Cloud Analytic Services saved the file sas_orsales.csv in caslib ADLS2.
86   run;
87   quit;
```

### Verify saved data file at ADLS2

Verify CAS data file saved at ADLS2 storage account under sub-folder 'sample_data'. At Azure Storage account navigate to Storage Explorer and refresh the sample_data sub-folder to see the new files.

![1](img/utkuma_2020-05-26-15-26-07.png)

## CAS load from ADLS2 data files (CSV and ORC)

On SAS Studio editor execute the following code to load CAS from ADLS2 storage data file.

**Before running the code replace the value for accountname, filesystem, and applicationID with information collected in previous section.**

CAS01 server may already have the Azure token key from previous statement , where you saved a CAS table to ADLS2.

In case of Multi-Node CAS environment, very first time CAS load from a CSV file (located at ADLS2) generates an error msg.

user need to Azure authenticate each CAS Node for CSV data file load. The CAS load from CSV data file (ADLS2) is in parallel. The .json files with access token key reside on each CAS Node under user 'cas'.

Code:

```sas

/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;

/*
Example:
%let MYSTRGACC="utkuma5adls2strg";
%let MYSTRGFS="fsutkuma5adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-a430-xxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-9eda-xxxxxxxxxxxx";
*/


CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib ADLS2 datasource=(
      srctype="adls"
      accountname=&MYSTRGACC
      filesystem=&MYSTRGFS
      dnsSuffix=dfs.core.windows.net
      timeout=50000
      tenantid=&MYTNTID
      applicationId=&MYAPPID
   )
   path="/sample_data"
   subdirs;

proc casutil incaslib="ADLS2";
   list files ;
run;
quit;


/* CAS load from ADLS2 storage data file */
proc casutil  incaslib="ADLS2"  outcaslib="ADLS2";
  load casdata="sas_orsales.orc" casout="sas_orsales_ORC" replace ;
  load casdata="sas_orsales.csv" casout="sas_orsales_CSV" replace ;
  load casdata="cars.csv" casout="sas_cars_CSV" replace ;
  list tables ;
run;
quit;

cas mysession terminate;
```

### Authenticate each CAS Node for CSV load (ADLS2 CSV file)

Very first time you load a CSV file from ADLS2 CASLIB, you will see following error msg in sas log. You need to jump to each CAS node and view the /home/cas/.sasadls_XXXXXX.json file for authentication process. Each node has separate code to validate and get access key. You need to login to 'https://microsoft.com/devicelogin' site for each cas node and authenticate the device code for CASLIB.

```log
NOTE:       bytes moved             114.62K
NOTE: The Cloud Analytic Services server processed the request in 0.479026 seconds.
85     load casdata="sas_orsales.csv" casout="sas_orsales_CSV" replace ;
NOTE: Executing action 'table.loadTable'.
ERROR: Pending end-user authorization.
ERROR: The action stopped due to errors.
NOTE: Action 'table.loadTable' used (Total process time):
```

#### Steps to login to CAS Unix servers

Steps are provided in-detail in previous section.

View the "user_host_file" and "resource_info" file from script folder to jump to cas01....04 server.

```bash

more  user_host_file

more resource_info

```

Look for “To Jump to Viya01 cloud server:” section from executed statement. Don't execute the following log example, it's just for reference purpose. You need to run statements what you see at your X-term window.

Execute the code with different ip address for vm_public_ip= to jump to different CAS node. Skip the CAS01 server if there is already a Azure access token under /user/cas/ folder.

```log

Example:
my_private_key=/r/ge.unx.sas.com/vol/vol310/u31/utkuma/.ssh/utkuma-viyadep-key
my_user=viyadep
vm_public_ip=52.252.1.88

ssh -o StrictHostKeyChecking=no -i $my_private_key $my_user@$vm_public_ip

```

Once you are on CAS node server you can view the json file using following statement.

```bash
sudo ls -al /home/cas/

sudo more /home/cas/.sasadls_10002.json

exit

```

### Return to SAS studio and re-run CAS (csv) load code

Results:

```log
……….
……………………….
84   proc casutil  incaslib="ADLS2"  outcaslib="ADLS2";
NOTE: The UUID '4fa74088-73fb-6540-a681-344cdf160e17' is connected using session MYSESSION.
85     load casdata="sas_orsales.orc" casout="sas_orsales_ORC" replace ;
NOTE: Executing action 'table.loadTable'.
NOTE: Cloud Analytic Services made the external data from sas_orsales.orc available as table SAS_ORSALES_ORC in caslib ADLS2.
NOTE: The Cloud Analytic Services server processed the request in 0.24411 seconds.
86     load casdata="sas_orsales.csv" casout="sas_orsales_CSV" replace ;
NOTE: Executing action 'table.loadTable'.
NOTE: Cloud Analytic Services made the external data from sas_orsales.csv available as table SAS_ORSALES_CSV in caslib ADLS2.
NOTE: The Cloud Analytic Services server processed the request in 0.163074 seconds.
…………
……………………….
```

Result Output:

![1](img/utkuma_2020-05-26-15-26-59.png)

### CAS Load  from multiple .orc file into a single table

CAS support loading list of .orc file under a sub-folder(ADLS2) as one CAS table. Execute the following code on SAS Studio application.

**Note: CASLIB ADLS2 has path to /sample_data with subdir option.**

**Before running the code set the parameter value for Storage account name, and file system name with information collected in previous section.**

Code:

```sas

/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;

/*
Example:
%let MYSTRGACC="utkuma5adls2strg";
%let MYSTRGFS="fsutkuma5adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-a430-xxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-9eda-xxxxxxxxxxxx";
*/

CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib ADLS2 datasource=(
      srctype="adls"
      accountname=&MYSTRGACC
      filesystem=&MYSTRGFS
      dnsSuffix=dfs.core.windows.net
      timeout=50000
      tenantid=&MYTNTID
      applicationId=&MYAPPID
   )
   path="/sample_data"
   subdirs;

proc casutil  incaslib="ADLS2"  outcaslib="ADLS2";
  load casdata="fish_n" casout="fish_New" replace  importoptions=(filetype="orc") ;
  list tables ;
run;
quit;

cas mysession terminate;
```

Results:

```log
………
…………….
83   proc casutil  incaslib="ADLS2"  outcaslib="ADLS2";
NOTE: The UUID '3c194e64-9fc0-6d4d-8c49-ea2b18757c04' is connected using session MYSESSION.
84     load casdata="fish_n" casout="fish_New" replace  importoptions=(filetype="orc") ;
NOTE: Executing action 'table.loadTable'.
NOTE: Cloud Analytic Services made the external data from fish_n available as table FISH_NEW in caslib ADLS2.
………
…………
```

Notice the Number of rows in FISH_NEW CAS table.

![1](img/utkuma_2020-06-22-13-26-27.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)**<-- you are here**
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
