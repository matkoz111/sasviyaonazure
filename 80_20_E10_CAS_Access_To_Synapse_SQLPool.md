![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# CAS access to Synapse SQL Pool database
- [Login to Viya application](#login-to-viya-application)
- [Serial data save/load from Synapse SQL Pool database to CAS](#serial-data-saveload-from-synapse-sql-pool-database-to-cas)

Purpose of this hands-on is to demonstrate the data access from CAS to Azure Synapse SQL Pool database. The serial and multi-node method can be used to load CAS from Azure Synapse SQL Pool database  using MS-SQL Data connector.

To connect Azure Synapse SQL Pool database, it requires ODBC driver and customized odbc.ini with an DSN entry. The ODBC driver is installed as part of environment startup process and customized odbc.ini is deployed later by a script.

## Login to Viya application

Login to SAS Viya application using following information.

```text
link    : https://viya01/SASDrive
User id : viyademo01
Password: lnxsas
```

Assume the SAS Administrators role when prompted.

Note: you must have updated local desktop C:\windows\System32\drivers\etc\hosts file and included the Viya server IP and alias information to access the above URL.

![1](img/utkuma_2020-05-26-15-56-49.png)

Open SAS Studio application using Develop SAS Code menu item.

![1](img/utkuma_2020-05-26-15-57-10.png)

![1](img/utkuma_2020-05-26-15-57-43.png)


## Serial data save/load from Synapse SQL Pool database to CAS

On SAS Studio execute the following code to load/save Synapse SQL Pool database table to CAS.

Code:

```sas

CAS mySession  SESSOPTS=(CASLIB=casuser TIMEOUT=99 LOCALE="en_US" metrics=true);

caslib azsqlws desc='Microsoft SQL Server Caslib'
dataSource=(srctype='sqlserver'
username='viyadep'
password='lnxsas@2020'
sqlserver_dsn="sqls_gelws" );

proc casutil incaslib="azsqlws" outcaslib="azsqlws";
load data=sashelp.cars casout="cars" replace;
save casdata="cars" casout="cars"   replace;
quit;


proc casutil incaslib="azsqlws" outcaslib="azsqlws";
load casdata="cars" casout="cars_new" replace;
load casdata="fish_sas" casout="fish_sas" replace;
list tables;
quit;

CAS mySession  TERMINATE;

```

Results:

```log
.........
.....
NOTE: The CAS statement request to update one or more session options for session MYSESSION completed.
83
84   caslib azsqlws desc='Microsoft SQL Server Caslib'
85   dataSource=(srctype='sqlserver'
86   username='viyadep'
87   password=XXXXXXXXXXXXX
88   sqlserver_dsn="sqls_gelws" );
NOTE: Executing action 'table.addCaslib'.
NOTE: 'AZSQLWS' is now the active caslib.
NOTE: Cloud Analytic Services added the caslib 'AZSQLWS'.
NOTE: Action 'table.addCaslib' used (Total process time):

....
...........
84   save casdata="cars" casout="cars"   replace;
NOTE: Executing action 'table.save'.
NOTE: Performing serial SaveTable action using SAS Data Connector to SQL Server.
WARNING: WARNING: [SAS][ODBC SQL Server Wire Protocol driver]Setting QuotedIdentifier to 'OFF' is not supported with this server.
         The value of EnabledQuotedIdentifiers has been changed to 1
WARNING: WARNING: [SAS][ODBC SQL Server Wire Protocol driver]Setting QuotedIdentifier to 'OFF' is not supported with this server.
         The value of EnabledQuotedIdentifiers has been changed to 1
NOTE: Cloud Analytic Services saved the file cars in caslib AZSQLWS.
NOTE: Action 'table.save' used (Total process time):
NOTE: The Cloud Analytic Services server processed the request in 41.347043 seconds.
85   quit;
....
..............

82
83   proc casutil incaslib="azsqlws" outcaslib="azsqlws";
NOTE: The UUID '6d1aa51f-2364-3e4f-8602-24daa3c5aee8' is connected using session MYSESSION.
84   load casdata="cars" casout="cars_new" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to SQL Server.
WARNING: WARNING: [SAS][ODBC SQL Server Wire Protocol driver]Setting QuotedIdentifier to 'OFF' is not supported with this server.
         The value of EnabledQuotedIdentifiers has been changed to 1
NOTE: Cloud Analytic Services made the external data from cars available as table CARS_NEW in caslib azsqlws.

85   load casdata="fish_sas" casout="fish_sas" replace;
NOTE: Executing action 'table.loadTable'.
NOTE: Performing serial LoadTable action using SAS Data Connector to SQL Server.
WARNING: WARNING: [SAS][ODBC SQL Server Wire Protocol driver]Setting QuotedIdentifier to 'OFF' is not supported with this server.
         The value of EnabledQuotedIdentifiers has been changed to 1
NOTE: Cloud Analytic Services made the external data from fish_sas available as table FISH_SAS in caslib azsqlws.
....
..............

```

Results:

CAS Table loaded from Synapse SQL Pool table.

![1](img/utkuma_2020-07-02-15-48-53.png)




<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)**<-- you are here**
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
