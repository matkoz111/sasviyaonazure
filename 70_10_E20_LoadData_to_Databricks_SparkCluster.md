![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Load sample data to Databricks Spark Cluster
- [Launch Databricks Workspace](#launch-databricks-workspace)
- [Create a new Notebook at Databricks Workspace](#create-a-new-notebook-at-databricks-workspace)
- [Read a Sample databricks data file](#read-a-sample-databricks-data-file)
- [Read parquet files from ADLS2 to Databricks SPARK](#read-parquet-files-from-adls2-to-databricks-spark)
- [Write Data from Databricks SPARK to ADLS2](#write-data-from-databricks-spark-to-adls2)

Purpose of this hands-on is to load sample data to Databricks Spark cluster by executed Python code at Notebook. The code will read .Json and .parquet file to load into Spark CLuster.


## Launch Databricks Workspace

Navigate to your Azure Resource Group and look for Azure Databricks service from the list of objects.

![1](img/utkuma_2020-06-24-16-13-47.png)

Click on hyperlink of Databricks service and  will take you to Databricks Service dashboard.
Click on **Launch Workspace** to launch the databricks workspace.

![1](img/utkuma_2020-06-24-16-18-06.png)

The **Launch Workspace** action will open a new web browser tab.

![1](img/utkuma_2020-06-24-16-21-34.png)


## Create a new Notebook at Databricks Workspace

On Azure Databricks dashboard, select **New Notebook** from common tasks.

![1](img/utkuma_2020-06-26-14-17-04.png)

It will pop-up to create a new Notebook. Pick a notebook name and keep default language to "Python". Keep the default cluster name.

**Note : make sure your SPARK CLuster is up and running. If it's ideal for 120 Min. it will shout down itself.**

![1](img/utkuma_2020-06-26-14-25-49.png)

Once you hit **Create**, it will take you to editor for code writing and execution.

![1](img/utkuma_2020-06-26-14-42-33.png)

## Read a Sample databricks data file

Read a sample databricks data file and save into Spark/Hive catalog to share with other users and applications.

In the first command block write following statement to read a sample .json data file into SPARK data frame. Use **Run Cell** option to execute it.

Code:

```bash

#Read a sample data file (iot_devices.json) from Databricks DBFS location.

df = spark.read.json("dbfs:/databricks-datasets/iot/iot_devices.json")

```

Note: To view the list of sample file from Databricks File System (DBFS), you can use **dbutils.fs.ls("dbfs:/databricks-datasets/iot/")** statement in one of the command block.

![1](img/utkuma_2020-06-26-15-02-54.png)

Insert a new cell and use following code to create a temp view and query first 10 rows. Use **Run Cell** option to execute it.

```bash
#Create temporary view on Spark Data Frame "DF"

df.createOrReplaceTempView('source')

#Display top 10 ros from the source file.

print('Displaying top 10 rows: ')
display(spark.sql('SELECT * FROM source LIMIT 10'))

```

![1](img/utkuma_2020-06-26-15-09-03.png)


Insert a new cell and use following code to save the temp view into a permanent table to share the data with other users and applications. Use **Run Cell** option to execute it.

```bash
#Write a parmanent table to share with other users and application.

permanent_table_name = "iot_device"

df.write.format("parquet").saveAsTable(permanent_table_name)

```

![1](img/utkuma_2020-06-26-15-14-04.png)

View the saved table under default database.On left pane of Azure Databricks dashboard, click  **Data** and it will display the list of tables under default database.

![1](img/utkuma_2020-06-26-15-18-15.png)


## Read parquet files from ADLS2 to Databricks SPARK

Read parquet data files from ADLS2 to Databricks SPARK cluster and save into Spark/Hive catalog to share with other users and applications.

collect Storage Account key from ADLS2 storage account . Navigate to your Storage account and under **Setting -> Access Keys** , copy the **key1** value to a text pad for usage in next step.

![1](img/utkuma_2020-06-26-15-30-16.png)

**Back to Databricks Workspace Editor.**

In the next command block write following statement to configure ADLS2 access from Databricks SPARK cluster.

**Before running the code update the Storage Account name and Storage account key with your storage account name and key collected from previous step.**

Code:

```bash


# Configure SPARK with Storage Account access key for ADLS2 read and write

storage_account_name = "utkuma8adls2strg"
storage_account_access_key = "u+KFB7Q78OeyCd5G0YZ0QfIq24DroMz3fgNw/Rp4iGvVTpJc9+3F0THt/38ZE6BMfbA2IZQgDus2zzw=="
storage_account_fs="fsdata"

spark.conf.set("fs.azure.account.key."+storage_account_name+".dfs.core.windows.net", storage_account_access_key)
spark.conf.set("fs.azure.createRemoteFileSystemDuringInitialization", "true")
dbutils.fs.ls("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/")
spark.conf.set("fs.azure.createRemoteFileSystemDuringInitialization", "false")


# Reference Code:
#spark.conf.set("fs.azure.account.key.<storage-account-name>.dfs.core.windows.net", <storage-account-key>)
#spark.conf.set("fs.azure.createRemoteFileSystemDuringInitialization", "true")
#dbutils.fs.ls("abfss://<file_system>@<storage-account-name>.dfs.core.windows.net/")
#spark.conf.set("fs.azure.createRemoteFileSystemDuringInitialization", "false")

```

![1](img/utkuma_2020-07-01-13-55-34.png)

In the next command block use following statement to list files from ADLS2 file system from Databricks SPARK.

Code:

```bash

# List filesystem to view the files and fodler

dbutils.fs.ls("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/")

```
![1](img/utkuma_2020-07-01-14-03-04.png)

In the next command block use following statement to read baseball parquet files from ADLS2 file system to Databricks SPARK.

Code:

```bash

# Read a parquet file (saved from another application) in a new Spark Data Frame "df3" and display the data.

df3 = spark.read.parquet("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/baseball")
display(df3)

```

![1](img/utkuma_2020-07-01-14-03-31.png)

The previous statement at Editor just read the following files from ADLS2 location.

![1](img/utkuma_2020-06-26-15-55-18.png)

In the next command block, use following statement to save baseball data into a permanent table to share with other users and applications.

Code:

```bash
# Save the Parquet DF3 into a table to share with other users and applications.

permanent_table_name = "baseball_prqt"

df3.write.format("parquet").saveAsTable(permanent_table_name)

```

![1](img/utkuma_2020-06-26-16-01-34.png)

View the saved table under default database.On left pane of Azure Databricks dashboard, click  **Data** and it will display the list of tables under default database.

![1](img/utkuma_2020-06-26-16-03-09.png)


## Write Data from Databricks SPARK to ADLS2

**This section is  optional**

Write Data from Databricks SPARK Data Frame to ADLS2 location. While writing data you can specify the file type as well. The data files can be consumed by other application as well.

**Back to Databricks Workspace Editor with SPARK configured to access ADLS2 location. Use the same editor from previous steps.**

**Assuming all the SPARK Data Frames are still active on Editor**


In the next command block, use following statement to save iot_device and baseball data to ADLS2 under a new folder.

Code:

```bash

# Write 'df' data frame to ADLS2 blob storage as json file/folder.
df.write.json("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/iot_devices.json")

# Write 'df' data frame to ADLS2 blob storage as Parquet file/folder.
df.write.parquet("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/iot_devices.parquet")

# Write 'df3' data frame to ADLS2 blob storage as Parquet file/folder.
df3.write.parquet("abfss://"+storage_account_fs+"@"+storage_account_name+".dfs.core.windows.net/baseball_new.parquet")

```

![1](img/utkuma_2020-07-01-14-08-43.png)

The ADLS2 location with new data files written by Databricks SPARK.

![1](img/utkuma_2020-06-26-16-54-01.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)**<-- you are here**
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
