![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Add an user to GELDM Azure subscription
- [Login/connect to Azure Dashboard](#loginconnect-to-azure-dashboard)
- [Navigate to GELDM subscription](#navigate-to-geldm-subscription)
- [Add a new user to GELDM subscription](#add-a-new-user-to-geldm-subscription)
  - [Assigning "contributor" role](#assigning-contributor-role)
  - [Assigning "Storage Blob Data Contributor" role](#assigning-storage-blob-data-contributor-role)


Purpose of this document is to add an user to **GELDM** Azure subscription.

## Login/connect to Azure Dashboard

Login to Azure dashboard / home by using following link:

[SAS Managed Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

Use your SAS email account to continue to Microsoft Azure.

![1](img/utkuma_2020-07-09-17-22-54.png)

The successful login will take you to the Azure Dashboard.

![1](img/utkuma_2020-07-09-17-25-01.png)

## Navigate to GELDM subscription

By using three bar-line menu, located at top left corner, choose **All services**.

![1](img/utkuma_2020-07-09-17-27-49.png)

At All service search box, type **subs** and it will filter and shows you **subscriptions**. Select the subscriptions hyperlink to move on to next page.

![1](img/utkuma_2020-07-09-17-30-48.png)

On subscriptions page, look for **GELDM** subscription and select the GELDM hyperlink to move onto next page.

![1](img/utkuma_2020-07-09-17-33-43.png)

On GELDM subscription page, navigate to **Access Control (IAM)** properties. Under Access control detail, select **Role assignments** tab.

![1](img/utkuma_2020-07-09-17-43-04.png)

## Add a new user to GELDM subscription

On GELDM subscription page, use **+Add** menu item and **Add role assignment** to add a role to new user. This action will open a popup tab.

![1](img/utkuma_2020-07-09-17-47-31.png)

![1](img/utkuma_2020-07-09-17-49-14.png)

### Assigning "contributor" role

On Pop-up tab, under **Role** columns, use drop-down to select a role. You can type the role name in search box to narrow down the search list. Select **Contributor** role.

Keep the default value for **Assign access to** column .

Under **Select** column, type a user name and it will show the user with SAS email id. Select the user to include in for the role.

![1](img/utkuma_2020-07-09-18-00-14.png)

Use **Save** button to add the role.

![1](img/utkuma_2020-07-09-18-02-29.png)

### Assigning "Storage Blob Data Contributor" role

On GELDM subscription page, use **+Add** menu item and **Add role assignment** to add a role for new user.

On Pop-up tab, under **Role** columns, use drop-down to select a role. You can type the role name "blob" in search box to narrow down the search list. Select **Storage Blob Data Contributor** role.

![1](img/utkuma_2020-07-09-18-06-03.png)

Keep the default value for **Assign access to** column .

Under **Select** column, type a user name and it will show the user with SAS email id. Select the user to include in for the role.
Use **Save** button to add the role.

![1](img/utkuma_2020-07-09-18-10-51.png)


Once an user is added with both roles, you can scroll the **Role Assignments** tab and double check the roles.

![1](img/utkuma_2020-07-09-18-14-30.png)

![1](img/utkuma_2020-07-09-18-16-24.png)

This concludes the addition of a new user to GELDM Azure subscription.

