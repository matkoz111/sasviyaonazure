#############################################################################
### This script add users from GELDM subscriptions
##############################################################################

if [ $# -lt 1 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User_list_file_name]"
    echo
    echo "Script requires one parameter"
    echo "     1. User list file name which contains user id with @sas.com e.g. user_list_07102020 "
    echo "     User list example (one user per line in the file)  " 
    echo "uttam.kumar@sas.com" 
    echo "Doug.Reeder@sas.com"
    echo
    echo
    exit 1
fi

user_list_file_nm=$1

function error_exit {
    echo "${1}"
    exit 1
}

## Set Azure default Subscripton to GELDM 
rs_sub_id=`az account set -s GELDM` 


#### Loop through the list of users from user_list_file 
for user_email_id in `cat $user_list_file_nm` ; 
do

## Add users to GEMDM subscription with "Contributor" and "Storage Blob Data Contributor" role 
echo "Adding User :  $user_email_id "

## "Contributor" Role 
user_role_id=`az role assignment create --role "Contributor" --assignee "$user_email_id" --query name -o tsv`
if [ "$?" -ne "0" ]; then
        error_exit "Could not add user to GELDM Subscription with 'Contributor' role."
fi
echo "Added user to GELDM Subscription with 'Contributor' role: $user_role_id "

## "Storage Blob Data Contributor" role
user_role_id=`az role assignment create --role "Storage Blob Data Contributor" --assignee "$user_email_id"  --query name -o tsv`
if [ "$?" -ne "0" ]; then
        error_exit "Could not add user to GELDM Subscription with 'Storage Blob Data Contributor' role."
fi
echo "Added user to GELDM Subscription with 'Storage Blob Data Contributor' role: $user_role_id"


done 
## End of loop 
