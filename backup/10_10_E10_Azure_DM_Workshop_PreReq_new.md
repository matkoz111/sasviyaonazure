![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Pre-requisites to access and start Viya environment at Azure
- [Ask us for being added to the **GELDM** Azure Subscription](#ask-us-for-being-added-to-the-geldm-azure-subscription)
- [Download and install MobaXterm application](#download-and-install-mobaxterm-application)
- [Login to cldlgn.fyi.sas.com Unix server](#login-to-cldlgnfyisascom-unix-server)
  - [Verify Azure CLI](#verify-azure-cli)
  - [Login to Azure using CLI](#login-to-azure-using-cli)
- [Login to Azure Dashboard](#login-to-azure-dashboard)

Purpose of this hands-on is to get an access to Azure Subscription **GELDM** and prepare your local machine for Hands-on exercises. The **GELDM** Azure Subscription is a SAS managed subscription and can only be granted access to SAS employees.

## Ask us for being added to the **GELDM** Azure Subscription

Your account will be added to the **GELDM** subscription for 2 week. Please plan to do the hands-on accordingly.

Send this **[email](mailto:gelreg@sas.com?subject=GEL%20Azure%20GELDM%20add)** without any change.

If the link above does not open your default email program, see this or simply email gelreg@sas.com with subject "GEL Azure GELDM add".

Once you get a feedback from us, please proceed with the next steps.

## Download and install MobaXterm application

Use following link to download and install “Home Edition” (Free version) of MobaXterm software to local machine.

[MobaXterm Download](https://mobaxterm.mobatek.net/download.html)

![2](img/utkuma_2020-05-26-13-36-47.png)

## Login to cldlgn.fyi.sas.com Unix server

The cldlgn.fyi.sas.com server is available for all SAS employee to access a Unix server using window network password. At this server you can generate access keys for SAS managed cloud environment.

Login to cldlgn.fyi.sas.com server using MobaXterm application with following information. If you cannot login to this server, please open a request with SAS IT support to get the access.

```bash
Remote host : cldlgn.fyi.sas.com
User Name   : < SAS Windows NT user id >
Password    : < SAS Windows NT password >
```

![1](img/utkuma_2020-05-28-16-39-36.png)

![1](img/utkuma_2020-05-28-16-41-08.png)

### Verify Azure CLI

At cldlgn.fyi.sas.com server, verify you can find Azure CLI at Unix prompt. If you cannot find, include the “/usr/bin/” path in PATH variable under .profile file.

Code:

```bash
which az
```

Results:

```log
cldlgn03.unx.sas.com> which az
/usr/bin/az
cldlgn03.unx.sas.com>
```

If previous step cannot find Azure (‘az’) CLI, update the .profile file under your home directory to include /usr/bin/ in PATH variable.

Code:

```bash
cd ~
echo "export PATH=$PATH:/usr/bin/"  >> .profile
```

Log out from cldlgn.fyi.sas.com server and login back to verify Azure CLI available at Unix prompt.

Code:

```bash
which az
```

Results:

```log
cldlgn03.unx.sas.com> which az
/usr/bin/az
cldlgn03.unx.sas.com>
```

### Login to Azure using CLI

From cldlgn.fyi.sas.com server use azure login CLI to generate access key token at Unix server.

Code:

```bash
az login
```

**Do not use Ctrl-C at Unix prompt to copy the microsoft link or device code.**

If you do so, it will terminate the "az login" command and will not generate Azure Access token. You can simply highlight the text at Unix session to copy into MS-Windows copy buffer.

This is highlighted text without Ctrl-C :
![1](img/utkuma_2020-06-05-11-36-25.png)

![](img/utkuma_2020-06-05-11-52-55.png)

The statement will ask you to use a web browser to open the page https://microsoft.com/devicelogin and enter the code XXXXXXXX to authenticate.

![1](img/utkuma_2020-06-05-11-42-13.png)

Follow the steps at microsoft site and use <NT_UserName>@sas.com to authenticate.

![1](img/utkuma_2020-06-05-11-42-34.png)

![1](img/utkuma_2020-06-05-11-43-30.png)

**Back to Unix session at cloud login server**

When you login to Azure, it generates an access key token and store under ~/.azure folder.

**If you don't see following similar result output on cloud login unix session, login to Azure did not go well and you don't have azure access key to proceed further.**

Results:

```log
cldlgn02.unx.sas.com> az login
To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code AKEVHR8XQ to authenticate.
[
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "b1c14d5c-3625-45b3-a430-9552373a0c2f",
    "id": "41baf198-cc93-4165-9882-6804e5fb95c0",
    "isDefault": true,
    "managedByTenants": [
      {
        "tenantId": "2f4a9838-26b7-47ee-be60-ccc1fdec5953"
      }
    ],
    "name": "GELDM",
    "state": "Enabled",
    "tenantId": "b1c14d5c-3625-45b3-a430-9552373a0c2f",
    "user": {
      "name": "Uttam.Kumar@SAS.COM",
      "type": "user"
    }
  }
]
cldlgn02.unx.sas.com>
```

Execute a azure CLI statement to test valid access token.

Code:
```bash

az account show -s GELDM

```

Results:

```log
cldlgn01.unx.sas.com> az account show -s GELDM
{
  "environmentName": "AzureCloud",
  "homeTenantId": "b1c14d5c-3625-45b3-a430-9552373a0c2f",
  "id": "41baf198-cc93-4165-9882-6804e5fb95c0",
  "isDefault": true,
  "managedByTenants": [
    {
      "tenantId": "2f4a9838-26b7-47ee-be60-ccc1fdec5953"
    }
  ],
  "name": "GELDM",
  "state": "Enabled",
  "tenantId": "b1c14d5c-3625-45b3-a430-9552373a0c2f",
  "user": {
    "name": "Uttam.Kumar@SAS.COM",
    "type": "user"
  }
}
cldlgn01.unx.sas.com>
```

## Login to Azure Dashboard

Once you have access to SAS managed Azure subscription/account, login to Azure Dashboard using following link. Login is validated using SAS Windows NT authentication.

[SAS Managed Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

The successful login will take you to the Azure Dashboard.

![1](img/utkuma_2020-05-26-14-17-47.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)**<-- you are here**
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
