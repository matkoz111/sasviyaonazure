#############################################################################
### This script add users from GELDM subscriptions
##############################################################################

if [ $# -lt 1 ]; then
    progname=${0##*/}
    echo
    echo "Usage: $progname [User_list_file_name]"
    echo
    echo "Script requires one parameter"
    echo "     1. User list file name which contains user id with @sas.com e.g. user_list_07102020 "
    echo "     User list example (one user per line in the file)  " 
    echo "uttam.kumar@sas.com" 
    echo "Doug.Reeder@sas.com"
    echo
    echo
    exit 1
fi

user_list_file_nm=$1

function error_exit {
    echo "${1}"
    exit 1
}

## Set Azure default Subscripton to GELDM 
rs_sub_id=`az account set -s GELDM` 


#### Loop through the list of users from user_list_file 
for user_email_id in `cat $user_list_file_nm` ; 
do

## Delete users from GEMDM subscription with all assigned role.
echo "Deleting User :  $user_email_id "

user_role_id=`az role assignment delete --assignee "$user_email_id" --yes -y --query name -o tsv`
if [ "$?" -ne "0" ]; then
        echo "Could not delete user from GELDM Subscription."
else
  echo "Deleted user from GELDM Subscription : $user_email_id"
fi


done 
## End of loop 
