![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# Create an Azure Databricks workspace service with Apache Spark cluster
- [Create Databricks Workspace Service](#create-databricks-workspace-service)
  - [Login to Azure Dashboard](#login-to-azure-dashboard)
  - [Navigate to Resource group](#navigate-to-resource-group)
  - [Add Databricks Workspace Service](#add-databricks-workspace-service)
    - [Basics Tab](#basics-tab)
    - [Networking Tab](#networking-tab)
    - [Tags Tab](#tags-tab)
    - [Review and Create Tab](#review-and-create-tab)
- [Launch Databricks Workspace](#launch-databricks-workspace)
- [Generate an Access Token at Databricks Workspace](#generate-an-access-token-at-databricks-workspace)
- [Create An Spark Cluster under Databricks Workspace](#create-an-spark-cluster-under-databricks-workspace)
- [Collect Spark Connection information](#collect-spark-connection-information)
- [Distribute Databricks JDBC driver to Viya and CAS Servers](#distribute-databricks-jdbc-driver-to-viya-and-cas-servers)

Purpose of this hands-on is to start an Azure Databricks Workspace Service with Apache Spark CLuster. Create an Databricks access key to connect workspace. Collect connection information to connect Spark environment.

## Create Databricks Workspace Service

### Login to Azure Dashboard

Login to Azure Dashboard and navigate to Azure Menu.
[Azure Dashboard](https://portal.azure.com/#@sasoffice365.onmicrosoft.com/dashboard/)

![1](img/utkuma_2020-05-26-14-40-49.png)

### Navigate to Resource group

Navigate to list of Azure Resource Groups from Azure Menu.

![1](img/utkuma_2020-05-26-14-41-29.png)

Navigate to Azure Resource Group from the list of resource group.

![1](img/utkuma_2020-05-26-16-11-10.png)

On Resource group click on +Add menu option to add a new object.

![1](img/utkuma_2020-05-26-16-09-45.png)

### Add Databricks Workspace Service

On New object window, select Analytics from Azure marketplace list and then select Azure Databricks to start a new Databricks Workspace.

![1](img/utkuma_2020-06-24-15-30-20.png)

#### Basics Tab

On Basics tab provide required values as follows:


```text
Subscription   : GELDM

Resources group :  e.g. utkuma8

Workspace name   : ws<resource_group_name> e.g. wsutkuma8

Location   : (US) EAST US 2

Pricing Tier : Standard (Apache Spark, Secured with Azure AD)

```

![1](img/utkuma_2020-06-24-15-39-54.png)

Press Next to move on to **Networking** tab.

#### Networking Tab

Keep the default value "No" for your own virtual network for deploying Databricks workspace. select next to move on to **Tags**  tab.

![1](img/utkuma_2020-06-24-15-44-47.png)

#### Tags Tab

On Tags tab use 'name' as tag and your NTid as value.  Select **Review and Create** to review the parameters details for Databricks Workspace.

![1](img/utkuma_2020-06-24-15-51-00.png)

#### Review and Create Tab

On Review tab, if the review process is ok and have green check. Select Create option to launch the Databricks Workspace service.  Wait for 5-10 min for Databricks workspace service  to be available.

![1](img/utkuma_2020-06-24-15-58-08.png)

While Databricks deployment is running you will see the following screen.

![1](img/utkuma_2020-06-24-15-59-33.png)

Once deployment completed you will see following screen.

![1](img/utkuma_2020-06-24-16-04-30.png)

## Launch Databricks Workspace

Navigate to your Azure Resource Group and look for Azure Databricks service from the list of objects.

![1](img/utkuma_2020-06-24-16-13-47.png)

Click on hyperlink of Databricks service and  will take you to Databricks Service dashboard.
Click on **Launch Workspace** to launch the databricks workspace.

![1](img/utkuma_2020-06-24-16-18-06.png)

The **Launch Workspace** action will open a new web browser tab.

![1](img/utkuma_2020-06-24-16-21-34.png)

## Generate an Access Token at Databricks Workspace

On Databricks Workspace dashboard, under **Account**, select **User Settings** to generate a new Token.

![1](img/utkuma_2020-06-24-17-12-04.png)

On User setting, under **Access Tokens** tab select **Generate New Token**

 ![1](img/utkuma_2020-06-24-16-31-13.png)

 On **Generate New Token** popup, you can type "mytoken" as comment and select generate.

![1](img/utkuma_2020-06-24-16-35-57.png)

Copy the token and save it in a text file for later use. Highlight the token by double clicking on token text. Right click on highlighted token text and it will give you option to copy.

![1](img/utkuma_2020-06-24-16-40-15.png)

Once you have copied of token and saved in a text file press **Done**. You will see a new token under **Access Tokens** tab. If you miss a step, you can always generate a new token. Keep in mind, every time you generate a new token, it will have different value.

![1](img/utkuma_2020-06-24-16-44-45.png)

## Create An Spark Cluster under Databricks Workspace

On Azure Databricks dashboard, select **Clusters** and it will take you to cluster creation page.

![1](img/utkuma_2020-06-24-16-53-05.png)

![1](img/utkuma_2020-06-24-16-55-40.png)

On Create Cluster page, provide required information as follows and press **Create Cluster**:

```text
Cluster Name  : <NAME> e.g. utkuma8cluster

Cluster Mode  :  Standard

Pool   : None

Databricks Runtime Version :  "Runtime: 6.5 9Scala 2.11, SPark 2.4.5)

AutoPilot Options:
Enable AutoScaling - Yes
Terminate after 120 Minutes of inactivity - Yes

Worker Type :
Standard_DS3_V2
Min Workers - 2
Max Worker - 3

Driver Type: Same as Worker

```

![1](img/utkuma_2020-06-26-10-56-23.png)

The Spark cluster creation takes ~2 Min, while it's created you wills ee following screen.

![1](img/utkuma_2020-06-26-10-59-24.png)

Upon successful creation of Spark Cluster.

![1](img/utkuma_2020-06-26-11-03-26.png)


## Collect Spark Connection information

On Azure Databricks Dashboard navigate to list of cluster by selecting **Cluster** from left pan. CLick on cluster hyperlink to navigate to cluster detail page.

![1](img/utkuma_2020-06-26-11-10-55.png)

On Cluster detail page, under **Configuration** tab, expand **Advanced Options**.

![1](img/utkuma_2020-06-26-11-17-27.png)

Scroll down the screen to see more details. Under Advanced tab go to **JDBC/ODBC** tab and copy the JDBC URL. Save this information in a text file, later it will be used in a SAS LIBNAME and CASLIB statement.

![1](img/utkuma_2020-06-26-11-56-27.png)

## Distribute Databricks JDBC driver to Viya and CAS Servers

The third-party application access to Databricks is supported by using JDBC driver. The JDBC Driver can be downloaded from

https://pages.databricks.com/ODBC-Driver-Download.html


For user convenience, the JDBC driver is staged under ..../Scripts/JDBCDriver subfolder.

The following steps will copy the JDBC driver from Script folder to Viya and CAS servers.

Login to cldlgn.fyi.sas.com Unix server and navigate to ..../Scripts folder, from where you started Azure environment. Execute the "To_Distribute_JDBC_Driver.sh" script to distribute Databricks JDBC Jar file to Viya environment.

Code:

```bash

cd ~/PSGEL265-sas-viya-3.5....../Scripts

./To_Distribute_JDBC_Driver.sh <NT ID> <Resource Group Name>

Example:
./To_Distribute_JDBC_Driver.sh utkuma utkuma8

```

Results:

```log

cldlgn04.unx.sas.com> ./To_Distribute_JDBC_Driver.sh utkuma utkuma8
Checking for Azure Resource Group
Azure Resource Group Exists : utkuma8 : true
 Resource Group  : utkuma8
 Getting Viya01 VM Public IP
Public Ip of Viya01 VM : 52.252.108.109
plugin_JDBCDriver_to_viya.sh                                                                                            100%  172    12.8KB/s   00:00
SparkJDBC42.jar                                                                                                         100% 9275KB  54.8MB/s   00:00
localhost | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": true,
    "checksum": "8260144a62356aa7d937b66135f195ce920334d1",
    "dest": "/opt/sas/viya/config/data/JDBCDriver/SparkJDBC42.jar",
    "gid": 10001,
    "group": "sas",
....
.................
......................

cldlgn02.unx.sas.com>
```




<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)**<-- you are here**
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
