![Global Enablement & Learning](https://gelgitlab.race.sas.com/GEL/utilities/writing-content-in-markdown/-/raw/master/img/gel_banner_logo_tech-partners.jpg)

# SAS FILENAME access to ADLS2 data file
- [SAS Filename statement to read/write data files to and from ADLS2](#sas-filename-statement-to-readwrite-data-files-to-and-from-adls2)

Purpose of this hands-on is to demonstrate the data file access from SAS to Azure Data Lake Storage 2 (ADLS2) using SAS FILENAME Statement.

Assuming you have ADLS2 Connection information.

## SAS Filename statement to read/write data files to and from ADLS2

On SAS Studio editor execute the following code to save a text file from SAS 9.4 to ADLS2.

**Before running the code set the parameter for account name, and filesystem with information collected in previous section.**

Note: Very first time you run the filename statement you will see error msg “token not valid”. If you have executed ORC LIBNAME statement, you have a valid token and should not see error msg. Otherwise, authenticate the viya01 server device by viewing the /home/viyademo01/*.json file. The .json file has the instruction how to register and authenticate the device. The detail instruction is in SAS_ORC_LIBNAME_Access_to_ADLS2 section.

Code:

```sas

/* Set parameters */
%let MYSTRGACC=<Your Storage Account name> ;
%let MYSTRGFS=<Your Storage File system Name> ;
%let MYTNTID=<Your Tenant Id> ;
%let MYAPPID=<Your Application(gelsas) Id>;

/*
Example:
%let MYSTRGACC="utkuma8adls2strg";
%let MYSTRGFS="fsutkuma8adls2strg";
%let MYTNTID="b1c14d5c-3625-45b3-a430-xxxxxxxxxxxxxx" ;
%let MYAPPID="a2e7cfdc-93f8-4f12-9eda-xxxxxxxxxxxxxx";
*/

options azuretenantid= &MYTNTID;

filename out adls "sasfile/example.txt"
applicationid=&MYAPPID
accountname=&MYSTRGACC
filesystem=&MYSTRGFS
;

data _null_;
   file out;
   put 'line 1';
   put 'line 2';
run;
```

Results:

```log
…………….
….
83   options azuretenantid=&MYTNTID;   /* 1*/
84
85   /** File Name Statement to ADLS2 **/
86   filename out adls "sasfile/example.txt"
87      applicationid=&MYAPPID
88      accountname=&MYSTRGACC
89      filesystem=&MYSTRGFS
90   ;
91
92   data _null_;
93      file out;
94      put 'line 1';
95      put 'line 2';
96   run;
NOTE: The file OUT is:
      Filename=sasfile/example.txt,
      Account Name=utkuma8adls2strg,
      File system=fsutkuma8adls2strg
NOTE: 2 records were written to the file OUT.
      The minimum record length was 6.
      The maximum record length was 6.
NOTE: DATA statement used (Total process time):
      real time           0.14 seconds
      cpu time            0.01 seconds

97
……………
…………………..
```

Verify the data files at ADLS2 sub-folder.

![1](img/utkuma_2020-05-26-15-21-00.png)

<!-- startnav -->
* [10 10 E10 Azure DM Workshop PreReq](/10_10_E10_Azure_DM_Workshop_PreReq.md)
* [10 10 E20 Start Environment](/10_10_E20_Start_Environment.md)
* [10 10 E30 Restart Environment](/10_10_E30_Restart_Environment.md)
* [30 10 E10 Collect ADLS2 Connection Info](/30_10_E10_Collect_ADLS2_Connection_Info.md)
* [30 10 E20 SAS ORC LIBNAME Access to ADLS2](/30_10_E20_SAS_ORC_LIBNAME_Access_to_ADLS2.md)
* [30 10 E30 SAS FILENAME Access to ADLS2](/30_10_E30_SAS_FILENAME_Access_to_ADLS2.md)**<-- you are here**
* [30 20 E10 CAS CASLIB Access to ADLS2](/30_20_E10_CAS_CASLIB_Access_to_ADLS2.md)
* [40 10 E10 Collect MSSQL Connection Info](/40_10_E10_Collect_MSSQL_Connection_Info.md)
* [40 10 E20 SAS Access to Azure MSSQL](/40_10_E20_SAS_Access_to_Azure_MSSQL.md)
* [40 20 E10 CAS Access to Azure MSSQL](/40_20_E10_CAS_Access_to_Azure_MSSQL.md)
* [50 20 E10 Create HDI using ADLS2](/50_20_E10_Create_HDI_using_ADLS2.md)
* [50 20 E20 Collect HDI Connection Info](/50_20_E20_Collect_HDI_Connection_Info.md)
* [60 10 E10 Upload sample data to ADLS2](/60_10_E10_Upload_sample_data_to_ADLS2.md)
* [60 10 E20 SAS Read ADLS2 Via HDI](/60_10_E20_SAS_Read_ADLS2_Via_HDI.md)
* [60 10 E30 Create HiveDB at ADLS2](/60_10_E30_Create_HiveDB_at_ADLS2.md)
* [60 10 E40 SAS Save to ADLS2 Via HDI](/60_10_E40_SAS_Save_to_ADLS2_Via_HDI.md)
* [60 20 E10 CAS LoadSave to ADLS2 via HDI](/60_20_E10_CAS_LoadSave_to_ADLS2_via_HDI.md)
* [70 10 E10 Create DataBricksWS SparkCLuster](/70_10_E10_Create_DataBricksWS_SparkCLuster.md)
* [70 10 E20 LoadData to Databricks SparkCluster](/70_10_E20_LoadData_to_Databricks_SparkCluster.md)
* [70 10 E30 SAS Access To DataBricks Spark](/70_10_E30_SAS_Access_To_DataBricks_Spark.md)
* [70 20 E10 CAS Access To DataBricks Spark](/70_20_E10_CAS_Access_To_DataBricks_Spark.md)
* [70 20 E20 Delete DataBricksWS SparkCLuster](/70_20_E20_Delete_DataBricksWS_SparkCLuster.md)
* [80 10 E10 Create Synapse SQLPool](/80_10_E10_Create_Synapse_SQLPool.md)
* [80 10 E20 SAS Access To Synapse SQLPool](/80_10_E20_SAS_Access_To_Synapse_SQLPool.md)
* [80 20 E10 CAS Access To Synapse SQLPool](/80_20_E10_CAS_Access_To_Synapse_SQLPool.md)
* [80 20 E20 Delete Synapse SQLPool](/80_20_E20_Delete_Synapse_SQLPool.md)
* [90 10 E10 Stop Terminate Environment](/90_10_E10_Stop_Terminate_Environment.md)
<!-- endnav -->
